/**********************************************************
Fichier : jsEvenement.js
Auteur  : Massinisa Talah
Date    : 25 avril 2019
Fonction: Gestion d'evenement
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Anthony Cote    TODO : Mettre plus de documentation
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/

/**************************
pour les tooltips
****************************/
$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();
  loadInfoBulle();
});

function loadInfoBulle() {
  $('#msgAide').load('../HTML/info/infoEvenement.html');
}

/**
 * pour le typing caroussel
 */
var TxtRotate = function(el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = '';
  this.tick();
  this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

  var that = this;
  var delta = 300 - Math.random() * 100;

  if (this.isDeleting) { delta /= 2; }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function() {
    that.tick();
  }, delta);
};

window.onload = function() {
  changerMenu();
  var elements = document.getElementsByClassName('txt-rotate');
  for (var i=0; i<elements.length; i++) {
    var toRotate = elements[i].getAttribute('data-rotate');
    var period = elements[i].getAttribute('data-period');
    if (toRotate) {
      new TxtRotate(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #666 }";
  document.body.appendChild(css);
};

/***********************
pour afficher les groupes
************************/
$(document).ready(function(){
  $('.formComposerGroupe').submit(function(){
    var event=$('#evenements').val();

    //afficher les groupes dans la divAffichage
    $.post('../PHP/controller/CtrlGroupeEvaluation.php',{evenements:event},function(donnees){
      $('.divAffichage').html(donnees);
    });

    return false;
  });
});

/**
 * ajouter une evenement
 */
$(document).ready(function(){
  $('.formAjoutEvent').submit(function(){

  if (validerForm())
  {
      $(".alert").show();
        var adresse=$('#adresse').val();
        var jour=$('#jour').val();
        var mois=$('#mois').val();
        var annee=$('#annee').val();
        var heure=$('#heure').val();
        var min=$('#min').val();

        //afficher un alert success
        $.post('../PHP/controller/CtrlEvenementEvaluation.php',{adresse:adresse,jour:jour,mois:mois,annee:annee,heure:heure,min:min},function(donnees){
          $('.returnMessage').html(donnees).slideUp(2000);

          $('#adresse').val('');
          $('#jour').val('01');
          $('#mois').val('01');
          $('#annee').val('');
          $('#heure').val('08');
          $('#min').val('00');

          //Nous envoyons un message de confirmation à l'utilisateur.
             swal({
                 title: "Confirmation d'ajout!",
                 text: "Le bénévole a été ajouté à la base de donnée.",
                 icon: "success"
             })
             //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
           .then((isConform) => {
               if (isConform) {
                   setTimeout(window.location.href = '../GUIEvenement.php', 1000);
               }
           });
        });
    }
    return false;

  });
});

/**
 * valider le form avant le submit
 */
function validerForm() {
  var valide = true;
  if (!validerAnnee('annee')) {
      valide = false;
  }
  if (!validerAdresse('adresse')) {
      valide = false;
  }
  return valide;
}
