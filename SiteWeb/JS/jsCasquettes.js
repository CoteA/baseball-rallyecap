/****************************************
Fichier : jsCasquettes.js
Auteur  : Christophe Leclerc
Fonction: Permet d'intégrer du javascript à l'interface permettant
          de gérer les casquettes.
Date    : 2019-04-17
=========================================================
Vérification :
2019-05-05  Maïka Forestal      Ok
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
=========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-27  Christophe Leclerc  Ajout d'une partie ajax permettant d'aller chercher les épreuves de casquettes.
2019-04-29  Christophe Leclerc  Ajout d'une partie ajax permetant de tranférer des épreuves d'un côté à l'autre.
2019-04-30  Christophe Leclerc  Ajout de quelque pop-up.
****************************************/

/**
 * Permet d'ajouter tous les listeners dans notre html
 */
function setOnChangeAction() {
  $(document.getElementById('casquettesList')).change(function () { buttonState(); });
  $(document.getElementById('btnAjouter')).on('click', function () { addItem(); });
  $('#casquettesList').change(function () { changeList(); });
  $('#addEpreuve').on('click', function () { addEpreuveToCasquette(); });
  $('#removeEpreuve').on('click', function () { removeEpreuveFromCasquette(); });
  $('#ajoutBD').on('click', function () { addCasquette(); });
  $('#modifier').on('click', function () { updateCasquette(); });
  $('#supprimer').on('click', function () { deleteCasquette(); });
  $('#enterAjout').blur(function () { canUpdate(); });
  $(document).ready(function () { $('[data-toggle="tooltip"]').tooltip(); loadInfoBulle();});
};


function loadInfoBulle() {
  $('#msgAide').load('../HTML/info/infoCasquette.html');
}

/**
 * Permet d'activer et désactiver les boutons modifier et supprimer
 */
function buttonState() {
  if ($(document.getElementById('casquettesList')).disable) {
    $(document.getElementById('modifier')).attr('disabled', 'disabled');
    $(document.getElementById('supprimer')).attr('disabled', 'disabled');
  } else {
    $(document.getElementById('supprimer')).removeAttr('disabled');
  }
};

function canUpdate() {
  if ($('#enterAjout').val() != null) {
    $(document.getElementById('modifier')).removeAttr('disabled');
  }
}

/**
 * Permet de faire apparaitre un entrer pour un nom et un bouton pour ajouter une casquette
 */
function addItem() {
  document.getElementById('enterAjout').type = 'text';
  document.getElementById('ajoutBD').type = 'button';
};

function loadComboBoxCasquette() {
  var rootDir = $('#rootDir').val();
  $.post('../PHP/controller/CtrlCasquettes.php', {
    loadComboBoxCasquette: 'loadComboBoxCasquette',
    rootDir: rootDir,
  }, function (data) {
    var select = document.getElementById("casquettesList");
    var length = select.options.length;
    for (i = length; i > 0; i--) {
      select.options[i] = null;
    }
    opts = JSON.parse(data);
    opts.forEach(function (opt) {
      $('#casquettesList').append(new Option(opt.nom, opt.id));
    });
  });
  document.getElementById('casquettesList').getElementsByTagName('option')[0].selected = 'selected';
  $(document.getElementById('modifier')).attr('disabled', 'disabled');
  $(document.getElementById('supprimer')).attr('disabled', 'disabled');
  $('#enterAjout').val('');
  document.getElementById('enterAjout').type = 'hidden';
  document.getElementById('ajoutBD').type = 'hidden';
};

/**
 * Permet de mettre à jour les list que contient la page casquette
 */
function changeList() {
  var casquettesList = $(document.getElementById('casquettesList')).val(); //Va chercher le id dans le combo box des casquettes
  var rootDir = $('#rootDir').val();
  $("#epreuveCasquette tr").remove();

  //Va chercher les épreuves pour la casquette dans la bd
  $.post('../PHP/controller/CtrlCasquettes.php', {
    casquette: casquettesList,
    rootDir: rootDir,
    loadEpreuveForCasquette: 'loadEpreuveForCasquette'
  }, function (data) {
    $(document.getElementById('epreuveCasquette')).append(data)
  });
  $("#epreuveDispo tr").remove();

  //Va chercher les épreuves an'ayant pas de casquette attribué
  $.post('../PHP/controller/CtrlCasquettes.php', {
    rootDir: rootDir,
    loadEpreuveNull: 'loadEpreuveNull'
  }, function (data) {
    $(document.getElementById('epreuveDispo')).append(data)
  });

  $('#addEpreuve').attr('disabled', 'disabled');
  $('#removeEpreuve').attr('disabled', 'disabled');
};

/**
 * Permet de sélectionner des éléments dans un tableau
 * @param idLigne est le id de la ligne à sélectionner
 */
function selectedLigne(idLigne, idTable) {
  removeSelection();
  var row = document.getElementById(idLigne);
  row.classList.add("ligneSelectionner");
  if (idTable == 'epreuveDispos')
    $('#addEpreuve').removeAttr('disabled');
  else
    $('#removeEpreuve').removeAttr('disabled');
}

/**
 * Permet d'enlever la classe sélection sur tous les tables de la page
 */
function removeSelection() {
  var tables = Array.from(document.getElementsByTagName('table'));

  tables.forEach(function (table) {
    for (var i = 0; row = table.rows[i]; i++) {
      row.classList.remove("ligneSelectionner");
    }
  });
}

/**
 * Permet d'ajouter une épreuve à une casquette
 */
function addEpreuveToCasquette() {
  var casquette = $(document.getElementById('casquettesList')).val();
  var all = document.getElementsByClassName('ligneSelectionner');
  var id = all[0].id;

  var rootDir = $('#rootDir').val();

  //Permet d'ajouter une épreuve à une casquette
  $.post('../PHP/controller/CtrlCasquettes.php', {
    addEpreuveToCasquette: 'addEpreuveToCasquette',
    id: id,
    casquette: casquette,
    rootDir: rootDir
  }, function () { });

  //reload les lists d'épreuves
  setTimeout(function () {
    changeList();
  }, 300);;
  $('#addEpreuve').attr('disabled', 'disabled');
  $('#removeEpreuve').attr('disabled', 'disabled');

  swal({
    title: "Confirmation!",
    text: "Vous avez ajouté l'épreuve à la casquette.",
    icon: "success"
  });
};

/**
 * Permet d'enlever une épreuve d'une casquette
 */
function removeEpreuveFromCasquette() {
  var all = document.getElementsByClassName('ligneSelectionner');
  var id = all[0].id;

  var rootDir = $('#rootDir').val();

  //Permet d'enlever une épreuve à une casquette
  $.post('../PHP/controller/CtrlCasquettes.php', {
    removeEpreuveFromCasquette: 'removeEpreuveFromCasquette',
    id: id,
    rootDir: rootDir
  }, function () { });

  //Reload les lists d'épreuves
  setTimeout(function () {
    changeList();
  }, 300);;

  //disable les boutons de sélection
  $('#addEpreuve').attr('disabled', 'disabled');
  $('#removeEpreuve').attr('disabled', 'disabled');

  swal({
    title: "Confirmation!",
    text: "Vous avez retiré l'épreuve à la casquette.",
    icon: "success"
  });
}

/**
 * Fenêtre modale pour l'ajout d'une casquette
 */
function addCasquette() {

  //aller chercher nos valeurs dans notre html.
  var casquette = $('#enterAjout').val();
  var ajoutBD = $('#ajoutBD').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer une suppression.
  $.post('../PHP/controller/CtrlCasquettes.php', { ajoutBD: ajoutBD, casquette: casquette, rootDir: rootDir }, function () { });

  //Nous envoyons un message de confirmation de suppression à l'utilisateur.
  swal({
    title: "Confirmation d'ajout!",
    text: "L'ajout' s'est effectué avec succès.",
    icon: "success"
  })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        //setTimeout(window.location.href = '../GUICasquettes.php', 1000);
        loadComboBoxCasquette();
      }
    });
};

/**
 * Fenêtre modale pour modifier une casquette
 */
function updateCasquette() {

  //aller chercher nos valeurs dans notre html.
  var casquette = $('#casquettesList').val();
  var modifier = $('#modifier').val();
  var newValue = $('#enterAjout').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer une mise a jour.
  $.post('../PHP/controller/CtrlCasquettes.php', {
    modifier: modifier,
    casquette: casquette,
    newValue: newValue,
    rootDir: rootDir
  },
    function () { });

  //Nous envoyons un message de confirmation de suppression à l'utilisateur.
  swal({
    title: "Confirmation de la modification!",
    text: "La modifiction s'est effectué avec succès.",
    icon: "success"
  })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        loadComboBoxCasquette();
      }
    });
};

/**
 * Fenêtre modale pour la suppression d'une casquette
 */
function deleteCasquette() {

  //La première fenêtre qui demande une confirmation
  swal({
    title: "Êtes-vous certain de vouloir supprimer?",
    text: "Une fois supprimé, il vous sera impossible de le récupérer.",
    icon: "warning",
    buttons: ["Annuler", "Confirmer"],
    dangerMode: true,
  })

    .then((willDelete) => {
      //Se qui se passe si nous avons confirmer la suppression.
      if (willDelete) {

        //aller chercher nos valeurs dans notre html.
        var casquette = $('#casquettesList').val();
        var supprimer = $('#supprimer').val();
        var rootDir = $('#rootDir').val();

        //Nous effectuons un post pour effectuer une suppression.
        $.post('../PHP/controller/CtrlCasquettes.php', {
          supprimer: supprimer,
          casquette: casquette,
          rootDir: rootDir
        }, function () { });

        //Nous envoyons un message de confirmation de suppression à l'utilisateur.
        swal({
          title: "Confirmation de suppression!",
          text: "La suppression s'est effectué avec succès.",
          icon: "success"
        })

          //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
          .then((isConform) => {
            if (isConform) {
              loadComboBoxCasquette();
            }
          });

        //Dans le cas où nous avons annuler la suppression (dans le premier pop-up) nous envoyons un message.
      } else {
        swal("Annulation!", "Vous n'avez rien supprimé.", "error");
      }
    });
};