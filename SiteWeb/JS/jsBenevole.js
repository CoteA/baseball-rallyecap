/**********************************************************
Fichier :           jsBenevole.js
Auteur  :           Maïka Forestal
Date    :           15 avril 2019
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom             Approuvé
2019-04-15  Anthony Cote    TODO : Manque de JSDoc, Extraire les swal
2019-05-05  Maïka Forestal  Ok
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
===========================================================
Historique de modifications :
Date            Nom                 Description
2019-05-05      Maïka Forestal      Ajout de commentaire, et remise de la fonction ajouter() 
                                    qui avait ete enlever
**********************************************************/

function loadInfoBulle() {
    $('#msgAide').load('../HTML/info/infoBenevole.html');
  }

/**
 * Variable class, permettant de gérer les champs du formulaire
 * de bénévole
 */
var benevole = {
    nom: null,
    prenom: null,
    nomComplet: null,
    telephone: null,
    courriel: null,
    type: null,
    switchActif: null,
    utilisateur: null,
    motDePasse: null,

    /**
     * Initialise un bénévole
     */
    init: function (ligne, actif, username, motDePasse) {

        this.setNom(ligne);
        this.setPrenom(ligne);
        this.setType(ligne);
        this.setTelephone(ligne);
        this.setCourriel(ligne);
        this.setActif(actif);
        this.setUserName(username);
        this.setMotDePasse(motDePasse);
        this.setNomComplet();
    },

    /**
     * Modifie le nom du champs Nom lorsque l'on sélectionne une ligne,
     * récupère le nom de famille, lorsque le nom complet y est
     * @param {Ligne sélectionner} ligne 
     */
    setNom: function (ligne) {
        var reNom = (/^([A-ZÉÈÀÇÙ][a-zéèùàç]+)([-][A-ZÉÈÀÇÙ][a-zéèùàç]+)*/);

        this.nom = document.getElementById('nomEvaluateur');
        reNom.test(ligne.cells[0].innerHTML);

        this.nom.value = RegExp.$1;
    },

    /**
     * Modifie le prénom dans le champs prenom lorsque 
     * lon sélectionne une ligne dans le tableau
     * @param {Ligne sélectionné} ligne 
     */
    setPrenom: function (ligne) {
        this.prenom = document.getElementById('prenomEvaluateur');
        this.prenom.value = ligne.cells[1].innerHTML;
    },

    /**
     * Modifie le nom complet du bénévole, sans nécessairement modifier le
     * champs nom de famille
     */
    setNomComplet: function () {
        this.nomComplet = this.nom.value + " " + this.prenom.value;
    },

    /**
     * Modifie le champs type de bénévole selon la ligne du tableau
     * sélectionné.
     * @param {Ligne sélectionné} ligne 
     */
    setType: function (ligne) {
        this.type = document.getElementById('typeBenevole');
        this.type.selectedIndex = benevoleType(ligne.cells[2].innerHTML, type.options);
    },

    /**
     * Modifie le champ numéro de téléphone lorsque l'on sélectionne une ligne 
     * dans le tableau
     * @param {Ligne sélectionné} ligne 
     */
    setTelephone: function (ligne) {
        this.telephone = document.getElementById('telEvaluateur');
        this.telephone.value = ligne.cells[3].innerHTML;
    },

    /**
     * Modifie le champs courriel lorsque selon la ligne sélectionné
     * dans le tableau
     * @param {Ligne sélectionnée} ligne 
     */
    setCourriel: function (ligne) {
        this.courriel = document.getElementById('courrielEvaluateur');
        this.courriel.value = ligne.cells[4].innerHTML;
    },

    /**
     * Modifie le champs actif/Désactif lorsque selon la ligne sélectionné
     * dans le tableau
     * @param {Ligne sélectionnée} ligne 
     */
    setActif: function (actif) {
        this.switchActif = document.getElementById('switchActif');
        this.switchActif.checked = estActif(actif);
    },

    /**
     * Modifie le champs nom d'utilisateur lorsque selon la ligne sélectionné
     * dans le tableau
     * @param {Ligne sélectionnée} ligne 
     */
    setUserName: function (username) {
        this.utilisateur = document.getElementById('utilisateur');
        this.utilisateur.value = username;
    },

    /**
     * Modifie le champs mot de passe lorsque selon la ligne sélectionné
     * dans le tableau
     * @param {Ligne sélectionnée} ligne 
     */
    setMotDePasse: function (motDePasse) {
        this.motDePasse = document.getElementById('mdp');
        this.motDePasse.value = motDePasse;
    },

    /**
     * Renvoie le nom de famille
     */
    getNom: function () {
        return this.nom.value;
    },

    /**
     * Renvoie le prénom
     */
    getPrenom: function () {
        return this.prenom.value;
    },

    /**
     * Renvoie le nom complet
     */
    getNomComplet: function () {
        return this.nomComplet;
    }


}

/**
 * Permet de faire une recherche d'un bénévole 
 */
function rechercherBenevole() {

    if (document.getElementById("zoneRecherche").hidden == true) {
        afficherRecherche();
    }
    else {
        afficherForm();
    }
}

/**
 * Affiche qu'un bénévole a été ajouter
 */
function ajoutBenevole() {
    if (validerForm()) {
        ajouter();
    }
}

/**
 * Affiche que le bénévole sélectionner a
 * été modifier.
 */
function modifierBenevole() {
    modifier();
}

/**
 * Sctive un bénévole
 */
function activeBenevole() {
    var type = document.getElementById('typeBenevole');
    var switchActif = document.getElementById('switchActif');
    var val = "Évaluateur";
    var options = type.options;
    opt = options[options.selectedIndex];
    if (opt.text == val) {
        switchActif.checked = true;
    }
    else
        switchActif.checked = false;
}

/**
 * Permet de supprimer un bénévole 
 */
function supprimerBenevole() {
    supprimer();

}


/**
 * Affiche le mot de passe à l'utilisateur
 */
function afficherMotDePasse() {
    var saisieMdp = document.getElementById("mdp");
    if (saisieMdp.type === "password") {
        saisieMdp.type = "text";
    } else {
        saisieMdp.type = "password";
    }
}

/**
 * Valider que tout les champs du formulaire son valide
 */
function validerForm() {
    var valide = true;

    if (!validerInfoPersonnel()) {
        valide = false;
    }
    if (!validerInfoCompte()) {
        valide = false;
    }
    return valide;
}

/**
 * Désactive les boutons ajouter/modifier/Supprimer
 */
function desactiverButtons() {
    var btnAjout = document.getElementById("ajoutEvaluateur");
    var btnModif = document.getElementById("modifBenevole");
    var btnSupp = document.getElementById("suppBenevole");

    btnAjout.hidden = true;
    btnModif.disabled = true;
    btnSupp.disabled = true;
}

/**
 * Affiche la zone de recherche d'un bénévole
 * et cache le formulaire
 */
function afficherRecherche() {
    document.getElementById("zoneRecherche").hidden = false;
    document.getElementById("infoEvaluateur").hidden = true;
    document.getElementById("infoCompte").hidden = true;
    document.getElementById("groupedeEvaluateur").hidden = true;
    document.getElementById("rechEvaluateur").textContent = "Annuler";
    desactiverButtons();
}

/**
 * Affiche le formulaire et cache la 
 * zonne de recherche
 */
function afficherForm() {
    document.getElementById("zoneRecherche").hidden = true;
    document.getElementById("infoEvaluateur").hidden = false;
    document.getElementById("infoCompte").hidden = false;
    document.getElementById("groupedeEvaluateur").hidden = false;
    document.getElementById("rechEvaluateur").textContent = "Rechercher";

    var btnAjout = document.getElementById("ajoutEvaluateur");
    btnAjout.hidden = false;
}

/**
 * Valide que les champs dans la zones
 * information personnels sont valide
 */
function validerInfoPersonnel() {
    var valide = true;
    if (!validerNom('nomEvaluateur')) { valide = false; }
    if (!validerPrenom('prenomEvaluateur')) { valide = false; }
    if (!validerTelephone('telEvaluateur')) { valide = false; }
    if (!validerCourriel('courrielEvaluateur')) { valide = false; }
    return valide;
}

/**
 * Valide que les champs dans la section info compte sont valide
 */
function validerInfoCompte() {
    return validerUtilisateur('utilisateur');
}

/**
 * Ajoute un bénévole dans la base de donnée
 * et affiche à l'utilisateur que l'action c'est bien effectué
 */
function ajouter() {
    //aller chercher nos valeurs dans notre html.
    $('#ajoutEvaluateur').val("ajouter"); //Nous devons défénir la valeur de notre input pour l'envoyer.
    var action = $('#ajoutEvaluateur').val();
    var nom = $('#nomEvaluateur').val();
    var prenom = $('#prenomEvaluateur').val();
    var telephone = $('#telEvaluateur').val();
    var type = $('#typeBenevole').val();
    var courriel = $('#courrielEvaluateur').val();

    var user = $('#utilisateur').val();
    var motDePasse = $('#mdp').val();
    var actif = $('#switchActif').val();

    //Nous effectuons un post pour effectuer une suppression.
    $.post('../PHP/controller/CtrlBenevole.php', {
        action: action,
        nomEvaluateur: nom,
        prenomEvaluateur: prenom,
        typeBenevole: type,
        telEvaluateur: telephone,
        courrielEvaluateur: courriel,
        utilisateur: user,
        mdp: motDePasse,
        Actif: actif
    }, function () { });

    //Nous envoyons un message de confirmation de suppression à l'utilisateur.
    swal({
        title: "Confirmation d'ajout!",
        text: "Le bénévole a été ajouté à la base de donnée.",
        icon: "success"
    })

        //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
        .then((isConform) => {
            if (isConform) {
                setTimeout(window.location.href = '../GUIBenevoles.php', 1000);
            }
        });
}


/**
 * Modifie un bénévole et affiche une fenêtre modal, 
 * indiquant à l'utilisateur que l'action c'est bien déroulé
 */
function modifier() {
    //aller chercher nos valeurs dans notre html.
    $('#modifBenevole').val("modifier"); //Nous devons défénir la valeur de notre input pour l'envoyer.
    var action = $('#modifBenevole').val();
    var nom = $('#nomEvaluateur').val();
    var prenom = $('#prenomEvaluateur').val();
    var telephone = $('#telEvaluateur').val();
    var type = $('#typeBenevole').val();
    var courriel = $('#courrielEvaluateur').val();

    var user = $('#utilisateur').val();
    var motDePasse = $('#mdp').val();
    var actif = $('#switchActif').val();

    //Nous effectuons un post pour effectuer une modification.
    $.post('../PHP/controller/CtrlBenevole.php', {
        action: action,
        nomEvaluateur: nom,
        prenomEvaluateur: prenom,
        typeBenevole: type,
        telEvaluateur: telephone,
        courrielEvaluateur: courriel,
        utilisateur: user,
        mdp: motDePasse,
        Actif: actif
    }, function () { });

    //Nous envoyons un message de confirmation de modification à l'utilisateur.
    swal({
        title: "Confirmation de modification!",
        text: "Le bénévole a bien été modifier.",
        icon: "success"
    })

        //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
        .then((isConform) => {
            if (isConform) {
                setTimeout(window.location.href = '../GUIBenevoles.php', 1000);
            }
        });
}

/**
 * Supprime un bénévole et affiche à l'utilisateur que
 * l'action c'est bien dérouler
 */
function supprimer() {
    swal({
        title: "Êtes-vous certain de vouloir supprimer?",
        text: "Une fois supprimé, il vous sera impossible de récupérer le bénévole.",
        icon: "warning",
        buttons: ["Annuler", "Confirmer"],
        dangerMode: true,
    })
        .then((willDelete) => {
            //Se qui se passe si nous avons confirmer la suppression.
            if (willDelete) {
                //aller chercher nos valeurs dans notre html.
                $('#suppBenevole').val("supprimer"); //Nous devons défénir la valeur de notre input pour l'envoyer.
                var action = $('#suppBenevole').val();
                var nom = $('#nomEvaluateur').val();
                var prenom = $('#prenomEvaluateur').val();
                var telephone = $('#telEvaluateur').val();
                var type = $('#typeBenevole').val();
                var courriel = $('#courrielEvaluateur').val();

                var user = $('#utilisateur').val();
                var motDePasse = $('#mdp').val();
                var actif = $('#switchActif').val();

                //Nous effectuons un post pour effectuer une suppression.
                $.post('../PHP/controller/CtrlBenevole.php', {
                    action: action,
                    nomEvaluateur: nom,
                    prenomEvaluateur: prenom,
                    typeBenevole: type,
                    telEvaluateur: telephone,
                    courrielEvaluateur: courriel,
                    utilisateur: user,
                    mdp: motDePasse,
                    Actif: actif
                }, function () { });

                //Nous envoyons un message de confirmation de suppression à l'utilisateur.
                //Nous envoyons un message de confirmation de suppression à l'utilisateur.
                swal({
                    title: "Confirmation de suppression!",
                    text: "Le bénévole a bien été supprimer.",
                    icon: "success"
                })
                    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
                    .then((isConform) => {
                        if (isConform) {
                            setTimeout(window.location.href = '../GUIBenevoles.php', 1000);
                        }
                    });
            } else {
                swal("Annulation!", "Vous n'avez rien supprimé.", "error");
            }
        });
}


/**
 * Affiche les informations du bénévole
 * et active ses boutons
 */
function selectBenevole(ligne, actif, username, motDePasse) {
    benevole.init(ligne, actif, username, motDePasse);

    if (document.getElementById("listBenevole").width <= 908) {
        ligne.cells[0].innerHTML = benevole.getNom();
        ligne.cells[1].hidden = false;
        ligne.cells[2].hidden = false;
        ligne.cells[3].hidden = false;
        ligne.cells[4].hidden = false;
    }


    // Activer les boutons supprimer et modifier
    document.getElementById("modifBenevole").removeAttribute('disabled');
    document.getElementById("suppBenevole").removeAttribute('disabled');
}

/**
 * Renvoie l'index a sélectionner lors de la sélection du bénévole
 * dans le tableaux des bénévoles.
 * @param {* Type de bénévole, du bénévole sélectionnée} val 
 * @param {* Les options du comboBox de type de bénévoles} options 
 */
function benevoleType(val, options) {
    for (var opt, j = 0; opt = options[j]; j++) {
        if (opt.text == val) {
            return j;
        }
    }
}

/**
 * Renvoie vrai si le bénévole est actif
 * @param {* État du bénévole (Actif ou pas)} actif 
 */
function estActif(actif) {
    if (actif == 1) {
        return true;
    } else
        return false;
}

function detecterMaj() {
    var divErreur = document.getElementById('motDePasseInvalide');
    divErreur.innerText = "Attention ! La touche Verr. maj. est activé!";

    if (event.getModifierState("CapsLock")) {
        divErreur.hidden = false;
    } else {
        divErreur.hidden = true;
    }
}
