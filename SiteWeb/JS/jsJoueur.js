/**********************************************************
Fichier : jsJoueur.js
Auteur  : Massinisah Talah
Date    : 15 avril 2019
Fonction: Gestion des joueurs
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Anthony Cote    TODO : Rajouter des commentaires
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/

/**************************
pour les tooltips
****************************/
$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip();
  loadInfoBulle();
});

function loadInfoBulle() {
  $('#msgAide').load('../HTML/info/infoJoueur.html');
}

/**************************
la selection d'une ligne d'un tableau
et remplir les champs du form avec les informations
**************************/
function selectLigne(id) {
  changerSelection();
  var row = document.getElementById(id);
  row.classList.add("ligneSelectionner");
  activerButton();

  remplirInput(row);
}

function setRows() {
  var table = document.getElementById("listJoueur");
  for (var i = 0, row; row = table.rows[i]; i++) {
    table.rows[i].id = ("row-" + (i + 1));
    if ((i + 1) != 1)
      table.rows[i].classList.add("tabRow");

    if ((i + 1) % 2 == 0) {
      table.rows[i].classList.add("altRow");
    }
  }
}

function changerSelection() {
  var table = document.getElementById("listJoueur");
  for (var i = 0, row; row = table.rows[i]; i++) {
    table.rows[i].classList.remove("ligneSelectionner");
  }
}

function activerButton() {
  document.getElementById("modifJoueur").removeAttribute('disabled');
  document.getElementById("suppJoueur").removeAttribute('disabled');
}

function remplirInput(row) {
  document.getElementById('prenom').value = row.cells[0].innerHTML;
  document.getElementById('nom').value = row.cells[1].innerHTML;
  document.getElementById('dateNaissance').value = row.cells[2].innerHTML;

  document.getElementById('prenomHidden').value = row.cells[0].innerHTML;
  document.getElementById('nomHidden').value = row.cells[1].innerHTML;
  document.getElementById('dateNaissanceHidden').value = row.cells[2].innerHTML;

  document.getElementById('casquettesList').value = getCouleurCasquette(row.cells[3].innerHTML);

  if (row.cells[4].innerHTML == '1')
    document.getElementById('actif').checked = true;
  else
    document.getElementById('desactive').checked = true;

  document.getElementById('prenomTuteur').value = row.cells[5].innerHTML;
  document.getElementById('nomTuteur').value = row.cells[6].innerHTML;
  document.getElementById('telTuteur').value = row.cells[7].innerHTML;

  document.getElementById('telTuteurHidden').value = row.cells[7].innerHTML;

  document.getElementById('courrielTuteur').value = row.cells[8].innerHTML;
}

function getCouleurCasquette(id_casquette) {
  var couleur;
  switch (id_casquette) {
    case "Blanc":      couleur = "1";      break;
    case "Gris":      couleur = "2";      break;
    case "Noir":      couleur = "3";      break;
    case "Vert":      couleur = "4";      break;
    case "Bleu":      couleur = "5";      break;
    case "Rouge":      couleur = "6";
      break;
  }
  return couleur;
}

/********************
valider l'envoie du format
*********************/
function ajouter() {
  if (validerInfo()) {
    ajouterJoueur();
  }
}

function validerInfo() {
  var valide = true;
  if (!validerNom('nom'))                 {    valide = false;  }
  if (!validerPrenom('prenom'))           {    valide = false;  }
  if (!validerDate('dateNaissance'))      {    valide = false;  }
  if (!validerNom('nomTuteur'))           {    valide = false;  }
  if (!validerPrenom('prenomTuteur'))     {    valide = false;  }
  if (!validerTelephone('telTuteur'))     {    valide = false;  }
  if (!validerCourriel('courrielTuteur')) {    valide = false;  }

  return valide;
}

/**
 * Permet de supprimer un joueur
 */
function supprimerJoueur() {
    suppJoueur();

}
/**
 * Permet de modifer un joueur
 */
function modifierJoueur() {
  if (validerInfo()) {
    modifierJoueur();
  }
}
/**
 * lancer la suppression d'un joueur
 */
function suppJoueur() {
  swal({
      title: "Êtes-vous certain de vouloir supprimer?",
      text: "Une fois supprimé, il vous sera impossible de le récupérer.",
      icon: "warning",
      buttons: ["Annuler", "Confirmer"],
      dangerMode: true,
    })
    .then((willDelete) => {
      //Se qui se passe si nous avons confirmer la suppression.
      if (willDelete) {

        //aller chercher nos valeurs dans notre html.
        var nom = $('#nomHidden').val();
        var prenom = $('#prenomHidden').val();
        var dateNaissance = $('#dateNaissanceHidden').val();

        $('#suppJoueur').val("suppJoueur"); //Nous devons défénir la valeur de notre input pour l'envoyer.
        var supprimer = $('#suppJoueur').val();
        var rootDir = $('#rootDir').val();

        //Nous effectuons un post pour effectuer une suppression.
        $.post('../PHP/controller/CtrlJoueur.php', {
          suppJoueur: supprimer,
          nomHidden: nom,
          prenomHidden: prenom,
          dateNaissanceHidden: dateNaissance,
          rootDir: rootDir
        }, function () {});

        //Nous envoyons un message de confirmation de suppression à l'utilisateur.
        swal({
            title: "Confirmation de suppression!",
            text: "La suppression s'est effectuée avec succès.",
            icon: "success"
          })

          //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
          .then((isConform) => {
            if (isConform) {
              setTimeout(window.location.href = '../GUIJoueurs.php', 1000);
            }
          });

        //Dans le cas où nous avons annuler la suppression (dans le premier pop-up) nous envoyons un message.
      } else {
        swal("Annulation!", "Vous n'avez rien supprimé.", "error");
      }
    });
}

/**
 * lancer l'ajout d'un joueur
 */
function ajouterJoueur() {
  //aller chercher nos valeurs dans notre html.
  var nom = $('#nom').val();
  var prenom = $('#prenom').val();
  var dateNaissance = $('#dateNaissance').val();
  var casquettesList = $('#casquettesList').val();
  var etat = $('.etat').val();
  var nomTuteur = $('#nomTuteur').val();
  var prenomTuteur = $('#prenomTuteur').val();
  var telTuteur = $('#telTuteur').val();
  var courrielTuteur = $('#courrielTuteur').val();

  $('#ajoutJoueur').val("ajoutJoueur"); //Nous devons défénir la valeur de notre input pour l'envoyer.
  var ajouter = $('#ajoutJoueur').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer un ajout.
  $.post('../PHP/controller/CtrlJoueur.php', {
      ajoutJoueur: ajouter,
      nom: nom,
      prenom: prenom,
      dateNaissance: dateNaissance,
      casquettes: casquettesList,
      etat: etat,
      nomTuteur: nomTuteur,
      prenomTuteur: prenomTuteur,
      telTuteur: telTuteur,
      courrielTuteur: courrielTuteur,
      rootDir: rootDir
    },
    function () {});

  //Nous envoyons un message de confirmation de suppression à l'utilisateur.
  swal({
      title: "Confirmation d'ajout!",
      text: "L'ajout' s'est effectué avec succès.",
      icon: "success"
    })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        setTimeout(window.location.href = '../GUIJoueurs.php', 1000);
      }
    });
}

/**
 * modifier un joueur
 */
function modifierJoueur() {
  //aller chercher nos valeurs dans notre html.
  var nom = $('#nom').val();
  var prenom = $('#prenom').val();
  var dateNaissance = $('#dateNaissance').val();
  var casquettesList = $('#casquettesList').val();
  var etat = $('.etat').val();
  var nomTuteur = $('#nomTuteur').val();
  var prenomTuteur = $('#prenomTuteur').val();
  var telTuteur = $('#telTuteur').val();
  var courrielTuteur = $('#courrielTuteur').val();
  //----Pour trouver le bon joueur----//
  var nomHidden = $('#nomHidden').val();
  var prenomHidden = $('#prenomHidden').val();
  var dateNaissanceHidden = $('#dateNaissanceHidden').val();
  //----pour trouver le tuteur----//
  var telTuteurHidden = $('#telTuteurHidden').val();

  $('#modifJoueur').val("modifJoueur"); //Nous devons défénir la valeur de notre input pour l'envoyer.
  var modifier = $('#modifJoueur').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer un ajout.
  $.post('../PHP/controller/CtrlJoueur.php', {
      modifJoueur: modifier,
      nomHidden: nomHidden,
      prenomHidden: prenomHidden,
      dateNaissanceHidden: dateNaissanceHidden,
      telTuteurHidden: telTuteurHidden,
      rootDir: rootDir,
      nom: nom,
      prenom: prenom,
      dateNaissance: dateNaissance,
      casquettes: casquettesList,
      etat: etat,
      nomTuteur: nomTuteur,
      prenomTuteur: prenomTuteur,
      telTuteur: telTuteur,
      courrielTuteur: courrielTuteur
    },
    function () {});

  //Nous envoyons un message de confirmation de suppression à l'utilisateur.
  swal({
      title: "Confirmation d'ajout!",
      text: "L'ajout' s'est effectué avec succès.",
      icon: "success"
    })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        setTimeout(window.location.href = '../GUIJoueurs.php', 2000);
      }
    });

}
