/****************************************
Fichier : jsEpreuve.js
Auteur  : Christophe Leclerc
Fonction: Permet d'intégrer du javascript à l'interface permettant
          de gérer les épreuves.
Date    : 2019-05-02
=========================================================
Vérification :
2019-05-05  Anthony Cote    TODO : Extraction de swal
2019-05-05  Maïka Forestal  TODO: Enlever les code en commentaire pas importantss
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
=========================================================
Historique de modifications :
Date        Nom                 Description

****************************************/
function setOnChangeAction() {
  $('#btnPlus').on('click', function () { addItem(); });
  $('#btnAdd').on('click', function () { addTypeEpreuve(); });
  $('#ajoutEpreuve').on('click', function () { addEpreuve(); });
  $('#modifEpreuve').on('click', function () { updateEpreuve(); });
  $('#suppEpreuve').on('click', function () { deleteEpreuve(); });
  $(document).ready(function () { $('[data-toggle="tooltip"]').tooltip(); loadInfoBulle(); });
};

function loadInfoBulle() {
  $('#msgAide').load('../HTML/info/infoEpreuve.html');
}

/**
 * Permet d'afficher les input pour l'ajout de type d'épreuve.
 */
function addItem() {
  document.getElementById('ajoutTypeEpreuve').type = 'text';
  document.getElementById('btnAdd').type = 'button';
};

/**
 * Permet de sélectionner des éléments dans un tableau
 * @param idLigne est le id de la ligne à sélectionner
 */
function selectedLigne(idLigne) {
  removeSelection();
  var row = document.getElementById(idLigne);
  row.classList.add("ligneSelectionner");
  loadSelectedEpreuve();
  $('#modifEpreuve').removeAttr('disabled');
  $('#suppEpreuve').removeAttr('disabled');
}

/**
 * Permet d'enlever la classe sélection sur tous les tables de la page
 */
function removeSelection() {
  var tables = Array.from(document.getElementsByTagName('table'));

  tables.forEach(function (table) {
    for (var i = 0; row = table.rows[i]; i++) {
      row.classList.remove("ligneSelectionner");
    }
  });
}

/**
 * Fenêtre modale pour l'ajout d'un type d'épreuve
 */
function addTypeEpreuve() {

  //aller chercher nos valeurs dans notre html.
  var valueToAdd = $('#ajoutTypeEpreuve').val();
  var ajoutTypeEpreuve = $('#btnAdd').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer un ajout.
  $.post('../PHP/controller/CtrlEpreuve.php', { ajoutTypeEpreuve: ajoutTypeEpreuve, valueToAdd: valueToAdd, rootDir: rootDir }, function () { });

  //Nous envoyons un message de confirmation de l'ajout à l'utilisateur.
  swal({
    title: "Confirmation d'ajout!",
    text: "L'ajout' s'est effectué avec succès.",
    icon: "success"
  })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        setTimeout(window.location.href = '../GUIEpreuves.php', 1000);
      }
    });
};

/**
 * Fenêtre modale pour l'ajout d'une épreuve
 */
function addEpreuve() {

  //aller chercher nos valeurs dans notre html.
  var nom = $('#nom').val();
  var description = $('#description').val();
  var nbEssaiMin = $('#nbrEssaiMin').val();
  var nbEssaiTotal = $('#nbrEssaiTotal').val();
  var idTypeEpreuve = $('#TypeEpreuve').val();
  $('#ajoutEpreuve').val("ajoutEpreuve"); //Nous devons défénir la valeur de notre input pour l'envoyer.
  var ajoutEpreuve = $('#ajoutEpreuve').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer un ajout.
  $.post('../PHP/controller/CtrlEpreuve.php', {
    nom: nom,
    description: description,
    nbEssaiMin: nbEssaiMin,
    nbEssaiTotal: nbEssaiTotal,
    idTypeEpreuve: idTypeEpreuve,
    ajoutEpreuve: ajoutEpreuve,
    rootDir: rootDir
  },
    function () {
    });

  //Nous envoyons un message de confirmation de l'ajout à l'utilisateur.
  swal({
    title: "Confirmation d'ajout!",
    text: "L'ajout' s'est effectué avec succès.",
    icon: "success"
  })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        setTimeout(window.location.href = '../GUIEpreuves.php', 1000);
      }
    });
};

/**
 * Fenêtre modale pour modifier une épreuve
 */
function updateEpreuve() {

  //aller chercher nos valeurs dans notre html.
  var all = document.getElementsByClassName('ligneSelectionner');
  var id = all[0].id;
  var nom = $('#nom').val();
  var description = $('#description').val();
  var nbEssaiMin = $('#nbrEssaiMin').val();
  var nbEssaiTotal = $('#nbrEssaiTotal').val();
  var idTypeEpreuve = $('#TypeEpreuve').val();
  $('#modifEpreuve').val("modifEpreuve"); //Nous devons défénir la valeur de notre input pour l'envoyer.
  var modifEpreuve = $('#modifEpreuve').val();
  var rootDir = $('#rootDir').val();

  //Nous effectuons un post pour effectuer une modification.
  $.post('../PHP/controller/CtrlEpreuve.php', {
    id: id,
    nom: nom,
    description: description,
    nbEssaiMin: nbEssaiMin,
    nbEssaiTotal: nbEssaiTotal,
    idTypeEpreuve: idTypeEpreuve,
    modifEpreuve: modifEpreuve,
    rootDir: rootDir
  },
    function () {
    });

  //Nous envoyons un message de confirmation de modification à l'utilisateur.
  swal({
    title: "Confirmation de la modification!",
    text: "La modifiction s'est effectué avec succès.",
    icon: "success"
  })

    //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
    .then((isConform) => {
      if (isConform) {
        setTimeout(window.location.href = '../GUIEpreuves.php', 1000);
      }
    });
};

/**
 * Fenêtre modale pour la suppression d'une épreuve
 */
function deleteEpreuve() {

  //La première fenêtre qui demande une confirmation
  swal({
    title: "Êtes-vous certain de vouloir supprimer?",
    text: "Une fois supprimé, il vous sera impossible de le récupérer.",
    icon: "warning",
    buttons: ["Annuler", "Confirmer"],
    dangerMode: true,
  })

    .then((willDelete) => {
      //Se qui se passe si nous avons confirmer la suppression.
      if (willDelete) {

        //aller chercher nos valeurs dans notre html.
        $('#suppEpreuve').val('suppEpreuve');
        var suppEpreuve = $('#suppEpreuve').val();
        var all = document.getElementsByClassName('ligneSelectionner');
        var id = all[0].id;

        var rootDir = $('#rootDir').val();

        //Nous effectuons un post pour effectuer une suppression.
        $.post('../PHP/controller/CtrlEpreuve.php', {
          suppEpreuve: suppEpreuve,
          id: id,
          rootDir: rootDir
        }, function () { });

        //Nous envoyons un message de confirmation de suppression à l'utilisateur.
        swal({
          title: "Confirmation de suppression!",
          text: "La suppression s'est effectué avec succès.",
          icon: "success"
        })

          //Une fois que nous avons cliquer sur ok, la page se reload afin de mettre à jour les valeurs de la pages.
          .then((isConform) => {
            if (isConform) {
              setTimeout(window.location.href = '../GUIEpreuves.php', 1000);
            }
          });

        //Dans le cas où nous avons annuler la suppression (dans le premier pop-up) nous envoyons un message.
      } else {
        swal("Annulation!", "Vous n'avez rien supprimé.", "error");
      }
    });
};

/** TODO */
function loadSelectedEpreuve() {

  var epreuve;
  var getById = 1;
  var all = document.getElementsByClassName('ligneSelectionner');
  var id = all[0].id;
  var rootDir = $('#rootDir').val();

  $.post('../PHP/controller/CtrlEpreuve.php', {
    getById: getById,
    id: id,
    rootDir: rootDir
  },
    function (data) {
      epreuve = JSON.parse(data);
      $('#nom').val(epreuve[0].nom);
      $('#description').val(epreuve[0].description);
      $('#nbrEssaiMin').val(epreuve[0].nbMin);
      $('#nbrEssaiTotal').val(epreuve[0].nbMax);
      //$("#TypeEpreuve option[value=4]").attr('selected', 'selected');
      document.getElementById('TypeEpreuve').selectedIndex = epreuve[0].idType;
    });
}