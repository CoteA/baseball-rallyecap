/**********************************************************
Fichier :           jsTableaux.js
Auteur  :           Maïka Forestal
Date    :           15 avril 2019
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Anthony Cote    Ok
2019-05-05  Maïka Forestal  Ok
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
/**
 * Ajoute des id à chaque ligne
 * Ajoute des classse pour changer la couleur des ligne
 * @param {* Id du tableau} idTab 
 */
function setRows(idTab) {
    var table = document.getElementById(idTab);
    for (var i = 0, row; row = table.rows[i]; i++) {
        table.rows[i].id = ("row-" + i);

        if (i > 0) {
            table.rows[i].classList.add("tabRow");
        }
    }
}

/**
 * Ajoute une classe à la ligne du tableau,
 * pour afficher qu'elle est sélectionner.
 * @param {* Id de la ligne sélectionné} idLigne 
 * @param {* Id du tableau} idTab 
 */
function selectLigne(idLigne, idTab, actif, username, motDePasse) {
    changerSelection(idTab);
    var row = document.getElementById(idLigne);
    row.classList.add("ligneSelectionner");
    activerButton(idTab);

    if (idTab == "listBenevole") {
        selectBenevole(row, actif, username, motDePasse);
    }
}

/**
 * Si une ligne porte déjà cette classe, on lui enleve
 */
function changerSelection(idTab) {
    var table = document.getElementById(idTab);
    
    for (var i = 1; row = table.rows[i]; i++) {
        table.rows[i].classList.remove("ligneSelectionner");

    
        if (window.outerWidth <= 908) {     
            benevole.setNom(table.rows[i]);
            benevole.setPrenom(table.rows[i]);
            benevole.setNomComplet();   
            table.rows[i].cells[0].innerText = benevole.getNomComplet();
            table.rows[i].cells[1].hidden = true;
            table.rows[i].cells[2].hidden = true;
            table.rows[i].cells[3].hidden = true;
            table.rows[i].cells[4].hidden = true;
        }
    }
}

/**
 * Active les bouttons du formulaire
 * selon le tableau...
 */
function activerButton(idTab) {
    if (idTab == 'listJoueur') {
        document.getElementById("modifJoueur").removeAttribute('disabled');
        document.getElementById("suppJoueur").removeAttribute('disabled');
    }
}