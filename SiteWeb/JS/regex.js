/**********************************************************
Fichier : regex.js
Auteur  : Anthony Cote
Date    : TODO
Fonction: Validation de donnees
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Anthony Cote    TODO : Mettre de la JS Doc
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/

// Regex
var value;

var reNom = (/^[A-ZÉÈÀÇÙ][a-zéèùàç]+([-][A-ZÉÈÀÇÙ][a-zéèùàç]+)*/);
var reTelephone = /^((([0-9]{1})*[- .(]*([0-9]{3})[- .)]*[0-9]{3}[- .]*[0-9]{4})+)*$/;
//var reDate = /^((19|20)?[0-9]{2})[-](0?[1-9]|1[012])[-](0?[1-9]|[12][0-9]|3[01])*$/;
var reDate =/^[0-9]{4}[-](0[1-9]|1[0-2])[-](0[1-9]|[1-2][0-9]|3[0-1])$/;
var reEmail = /^(([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-\.]+)@{[a-zA-Z0-9_\-\.]+0\.([a-zA-Z]{2,5}){1,25})+)*$/;
var reAnnee = /^(^[0-9]{4}$)/;
var reAdresse= /(^[a-zA-Z0-9]+([, ]*[a-zA-Z0-9]+)*)/;

var GOOD = "white";
var BAD = "#F1D8D7";


// ******************************************

/**
 * Vérifie si les valeurs entrée par l'utilisateur
 * respecte les régex que nous avons défini
 */
function checkRegexProperty(pattern, $elem){

    if(pattern.test($elem.value) && $elem.value != ""){
        changeBackgroundColor($elem, GOOD);             // Indique visuelement que le champ est correct
        return true
    }else{
        changeBackgroundColor($elem, BAD);              // Indique visuelement que le champ est incorrect
        return false
    }
}

/**
 * Change la couleur de fond du champs
 * @param {Champs du formulaire vérifié} $elem 
 * @param {Couleur du champs à appliquer} color 
 */
function changeBackgroundColor($elem, color) {
    $elem.style.backgroundColor = color;
    $elem.style.border = "1px solid darkgray";
}

// ******************************************

function isNom( $elem) {
    return checkRegexProperty(reNom,$elem);
}

function isTelephone( $elem) {
    return checkRegexProperty(reTelephone,$elem);
}

function isDate($elem) {
    return checkRegexProperty(reDate, $elem);
}

function isEmail($elem) {
    return checkRegexProperty( reEmail,$elem);
}

function isAnnee($elem) {
    return checkRegexProperty( reAnnee,$elem);
}

function isAdresse($elem)
{
  return checkRegexProperty( reAdresse,$elem);
}

/**
 * Les fonctions suivante permettent de valider les champs 
 * et d'afficher un messsage d'erreur si le régex n'est pas respecté
 */

function validerNom(id) {

    if (!isNom(document.getElementById(id))) {
        document.getElementById('nomInvalide').innerText = "Veuillez entrer un nom valide!";
        document.getElementById('nomInvalide').hidden = false;
        return false;
    }
    document.getElementById('nomInvalide').hidden = true;
    return true;
}

function validerPrenom(id) {
    if (!isNom(document.getElementById(id))) {
        document.getElementById('prenomInvalide').innerText = "Veuillez entrer un prénom valide!";
        document.getElementById('prenomInvalide').hidden = false;
        return false;
    }
    document.getElementById('prenomInvalide').hidden = true;
    return true;
}

function validerTelephone(id) {
    if (!isTelephone(document.getElementById(id))) {
        document.getElementById('telInvalide').innerText = "Le format de numéro de téléphone que vous avez entrer est invalide. Veuillez réessayer!";
        document.getElementById('telInvalide').hidden = false;
        return false;
    }
    document.getElementById('telInvalide').hidden = true;
    return true;
}

function validerCourriel(id) {
    if (!isEmail(document.getElementById(id))) {
        document.getElementById('courrielInvalide').innerText = "L'addresse courriel est invalide!";
        document.getElementById('courrielInvalide').hidden = false;
        return false;
    }
    document.getElementById('courrielInvalide').hidden = true;
    return true;
}

function validerUtilisateur(id) {
    if (document.getElementById(id).value.length > 255) {
        document.getElementById('utilisateurInvalide').innerText = "Le nom d'utilisateur est trop long. Il doit posséder moins de 255 caractères!";
        document.getElementById('utilisateurInvalide').hidden = false;
        return false;
    }
    document.getElementById('utilisateurInvalide').hidden = true;
    return true;
}



function validerDate(id) {
    if (!isDate(document.getElementById(id))) {
        document.getElementById('dateInvalide').innerText = "Le format de la date n'est pas compatible";
        document.getElementById('dateInvalide').hidden = false;
        return false;
    }
    document.getElementById('dateInvalide').hidden = true;
    return true;
}

function validerAnnee(id) {

    if (!isAnnee(document.getElementById(id))) {
        document.getElementById('anneeInvalide').innerText = "Veuillez entrer une année valide!";
        document.getElementById('anneeInvalide').hidden = false;
        return false;
    }
    document.getElementById('anneeInvalide').hidden = true;
    return true;
}

function validerAdresse(id) {

    if (!isAdresse(document.getElementById(id))) {
        document.getElementById('adresseInvalide').innerText = "Veuillez entrer une adresse!";
        document.getElementById('adresseInvalide').hidden = false;
        return false;
    }
    document.getElementById('adresseInvalide').hidden = true;
    return true;
}
