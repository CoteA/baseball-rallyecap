/**********************************************************
Fichier : menu.js
Auteur  : Maïka Forestal
Date    : 2019-04-21    (J'ai oublié la date...)
Fonction: Permet de changer la façon don le menu s'affiche
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Anthony Cote    TODO : Mettre des commentaires
2019-05-05  Maîka Forestal  Ok
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-05-05  Maïka Forestal      Ajout de commentaire
**********************************************************/
/**
 * Change l'apparence du menu selon la taille de l'écran
 */
function changerMenu() {
    var w = window.outerWidth;

    if (w < 1216) {
        document.getElementById("verticalMenu").hidden = true;
        document.getElementById("menuIcon").removeAttribute("hidden");
    }
    else {               
        document.getElementById("verticalMenu").hidden = false;
        document.getElementById("menuIcon").hidden = true;

    }
}

//Affiche le menu vertical lorsque l'on appuie sur l'icon
function afficherMenu() {
    if (document.getElementById("verticalMenu").hidden)
        document.getElementById("verticalMenu").hidden = false;
    else
        document.getElementById("verticalMenu").hidden = true;
}