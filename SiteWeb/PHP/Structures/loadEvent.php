<?php
/**********************************************************
Fichier :           loadEvent.php
Auteur  :           Massinissa Talah
Date    :           2019-04-13
Fonctionnalité :    Gestion groupe evaluation
===========================================================
Vérification :
Date        Nom
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
**********************************************************/

require_once("PHP/utils/DatabaseManager.php");
$query = "SELECT * FROM EvenementEvaluation";
$result = DatabaseManager::getResultSet($query);
?>

<select name="evenements" id="evenements" class="form-control">
<option disabled selected value>Sélectionner</option>
    <?php while($row = mysqli_fetch_array($result)):; ?>
    <option value="<?php echo $row[0]; ?>"><?php echo $row[1]; ?></option>
    <?php endwhile;?>
</select>
