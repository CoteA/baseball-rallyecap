<?php
/**********************************************************
Fichier :           loadTypeBenevole.php
Auteur  :           Maïka Forestal
Date    :           2019-04-13
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom
2019-04-23  Christophe Leclerc  APPROUVÉ
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
**********************************************************/

require_once("PHP/utils/DatabaseManager.php");
$query = "SELECT * FROM typeBenevole";
$result = DatabaseManager::getResultSet($query);
?>


<option disabled selected value>Sélectionner</option>
<?php while($row = mysqli_fetch_array($result)):; ?>
    <option value="<?php echo $row[0]; ?>">
        <?php echo $row[1]; ?>
    </option>
<?php endwhile;?>