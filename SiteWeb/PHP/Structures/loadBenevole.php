<?php
/**********************************************************
Fichier :           loadBenevole.php
Auteur  :           Maïka Forestal 
Date    :           2019-04-19
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom
2019-05-05  Maïka Forestal      Ok
===========================================================
**********************************************************/

/**
 * Affiche tout les bénévole dans le tableaux
 */
function loadAllBenevole(){
    $benevole = new gestBenevole();
    $listBenevoles = $benevole->getAll();
    foreach ($listBenevoles as $ben) {
        if (@pasSupprimer($ben) && @existe($ben)) {
            echo "<tr role=\"cell\"onclick=\"selectLigne(this.id, 'listBenevole', '" . $ben->getActif() . "','" . $ben->getUsername() . "', '" . $ben->getMotDePasse() . "')\">
            <td role=\"cell\">" . $ben->getNom() ."</td>
            <td role=\"cell\">" . $ben->getPrenom() ."</td>
            <td role=\"cell\">" . $benevole->typeBenevole($ben->getId()) ."</td>
            <td role=\"cell\">" . $ben->getTelephone() ."</td>
            <td role=\"cell\">" . $ben->getCourriel() ."</td>           
        </tr>";
        }
    }
}

/**¸
 * Liste tout les bénévole qui sont des entraineurs
 */
function loadEntraineur(){
    $benevole = new gestBenevole();
    $listBenevoles = $benevole->getAll();
    foreach ($listBenevoles as $ben) {
        if (@pasSupprimer($ben) && @existe($ben) && $benevole->typeBenevole($ben->getId()) == "Coach") {
            echo "<tr onclick=\"selectLigne(this.id, 'listBenevole', '" . $ben->getActif() . "','" . $ben->getUsername() . "', '" . $ben->getMotDePasse() . "')\">
            <td>" . $ben->getPrenom() . " ". $ben->getNom() ."</td>        
        </tr>";
        }
    }
}

