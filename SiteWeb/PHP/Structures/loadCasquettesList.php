<?php
/**********************************************************
Fichier :           loadCasquetteList.php
Auteur  :           Massinisa Talah
Date    :           2019-04-13
Fonctionnalité :    Gestion joueurs
===========================================================
Vérification :
Date        Nom
2019-05-05  Maïka Forestal      Ok
===========================================================
*/
require_once("PHP/utils/DatabaseManager.php");
$query = "SELECT * FROM casquette";
$result = DatabaseManager::getResultSet($query);
?>


<select name="casquettes" id="casquettesList" class="form-control selectpicker">
    <?php while($row = mysqli_fetch_array($result)):; ?>
    <option data-icon="glyphicon glyphicon-eye-open" data-subtext="petrification" value="<?php echo $row[0]; ?>"><?php echo $row[1]; ?></option>
    <?php endwhile;?>
</select>
