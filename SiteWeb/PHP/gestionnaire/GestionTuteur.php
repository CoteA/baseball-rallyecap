<?php
/**********************************************************
Fichier : GestionTuteur.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des Tuteurs
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/


if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
    require_once $root . "PHP/CLASS/Tuteur.php";
} else {
    require_once ROOT_DIR . "PHP/utils/DatabaseManager.php";
    require_once ROOT_DIR . "PHP/CLASS/Tuteur.php";
}

/** la classe permet de manipuler un tuteur d'un joueur, donc de l'ajouter à la bd, de le modifierJoueur
* de le supprimer ou de le trouver aussi dans la bd */
class GestionTuteur
{
    function __construct()
    {
    }

    /**     * ajouter le tuteur du joueur dans la BD
     */
    function ajouterTuteur($map)
    {
    $bdd=ouvrirConnection();

    $dernierIDtuteur=0;

    //le tuteur du joueur
    $tuteur=new Tuteur($dernierIDtuteur,$map['prenomTuteur'],$map['nomTuteur'],$map['telTuteur'],$map['courrielTuteur']);

    //insérer le tuteur dans la BD
    $stmt=$bdd->prepare(
        "INSERT INTO tuteur (id_tuteur, prenom, nom, telephone, courriel)
        VALUES (?, ?, ?, ?, ?)");

        $id=$tuteur->getId();
        $prenom=$tuteur->getPrenom();
        $nom=$tuteur->getNom();
        $telephone=$tuteur->getTelephone();
        $courriel=$tuteur->getCourriel();

        $stmt->bindParam(1, $id);
        $stmt->bindParam(2, $prenom);
        $stmt->bindParam(3, $nom);
        $stmt->bindParam(4, $telephone);
        $stmt->bindParam(5, $courriel);

        $stmt->execute();
    }

    /**     * modifier le tuteur du joueur selectionné
     */
    function modifierTuteur($map)
    {
        $bdd=ouvrirConnection();

        $tuteur=$this->trouverTuteur($map['telTuteurHidden']);

        $stmt = $bdd->prepare(
            "UPDATE Tuteur
            SET prenom = ? , nom = ? , telephone = ?, courriel = ?
            WHERE id_tuteur = ?");

        $prenom=$map['prenomTuteur'];
        $nom=$map['nomTuteur'];
        $tel=$map['telTuteur'];
        $courriel=$map['courrielTuteur'];
        $id=$tuteur->getId();

        $stmt->bindParam(1,$prenom);
        $stmt->bindParam(2, $nom);
        $stmt->bindParam(3, $tel);
        $stmt->bindParam(4, $courriel);
        $stmt->bindParam(5,$id);

        $stmt->execute();
    }

    /**   * trouver le joueur sélectionné dans la BD selon le telephone
   *@param string tel
   *@return Tuteur
   */
    function trouverTuteur($telHidden)
    {
        $bdd=ouvrirConnection();

        $stmt=$bdd->prepare("SELECT id_tuteur,prenom,nom, telephone,courriel FROM Tuteur WHERE (telephone = ?)");

        $stmt->bindParam(1,$telHidden);

        $stmt->execute();
        $result=$stmt->fetch();

        $tuteurTrouve=new Tuteur($result[0],$result[1],$result[2],$result[3],$result[4]);

        return $tuteurTrouve;
    }

}
