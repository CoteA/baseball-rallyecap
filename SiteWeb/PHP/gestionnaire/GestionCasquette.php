<?php
/**********************************************************
Fichier : GestionCasquette.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des Casquette (Accès à la base de donner)
===========================================================
Vérification :
Date        Nom
2019-04-24  Christophe Leclerc APPROUVÉ
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-24  Christophe Leclerc  Modification du ROOT_DIR
2019-04-25  Christophe Leclerc  Modification de la function delete
2019-05-05  Maïka Forestal      Ajout d'une description de la classe
**********************************************************/

//Nous permets de défénir le chemin à utiliser
if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
} else {
    require_once ROOT_DIR. "PHP/utils/DatabaseManager.php";
}

/**
 * Cette classe permet de faire la gestion des casquette.
 * C'Est le lien entre les donnée et la base de donnée
*/
class GestionCasquette
{

    /** Ajoute l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public function createInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "INSERT INTO casquette
                (id_casquette, nom_couleur, description)
                VALUES (?, ?, ?)";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in create]";
            return;
        }
        $id     = $obj->getId();
        $nom    = $obj->getNom();
        $desc   = $obj->getDescription();
        
        $stmt->bind_param("iss", $id, $nom, $desc );
        DatabaseManager::persist($stmt, $conn);
    }

    /** Modifi l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public function updateInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "UPDATE casquette SET nom_couleur=? WHERE id_casquette=?";
        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in update]";
            return;
        }

        $id     = $obj->getId();
        $nom    = $obj->getNom();
        
        $stmt->bind_param("si", $nom, $id );
        DatabaseManager::persist($stmt, $conn);
    }

    /** Supprime l'objet dans la base de donnees.
     *
     * @param  mixed $id
     * @param  mixed $conn
     *
     * @return void
     */
    public function deleteInDB($id, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "UPDATE Casquette SET deleted=? WHERE id_casquette = ?;";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in delete]";
            return;
        }

        $del = 'true';
        $stmt->bind_param("si", $del, $id);
        DatabaseManager::persist($stmt, $conn);
    }
}

/** Cree un Casquette a partir d'une map associative
 * @param  mixed $map
 *
 * @return void
 */
function CasquetteFromMap($map)
{
    if (isset($map["id"])) {
        $id = (int)$map["id"];
        $nom = $map["nom"];
        $description = $map["description"];
        return new Casquette($id, $nom, $description);
    }
    return null;
}
