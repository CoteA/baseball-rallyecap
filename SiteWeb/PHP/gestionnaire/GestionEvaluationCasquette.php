<?php
/**********************************************************
Fichier : GestionEvaluationCasquette.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des EvaluationCasquette
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  utililsé?
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once("PHP/utils/DatabaseManager.php");

/** */
class GestionEvaluationCasquette
{
    /** Ajoute l'objet dans la base de donnees
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function createInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "INSERT INTO EvaluationCasquette
                (id_evaluation_casquette, description, date, reussi, resultatNumerique)
                VALUES (?, ?, ?, ?, ?)";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in create]";
            return;
        }

        $id     = $obj->getId();
        $desc   = $obj->getDescription();
        $date   = $obj->getDate();
        $reussi = $obj->getReussi();
        $resultatNumerique = $obj->getResultatNumerique();
        
        $stmt->bind_param(
            "issbi", $id, $desc, $date, $reussi, $resultatNumerique);
        DatabaseManager::persist($stmt, $conn);
    }

    /** Modifie l'objet dans la base de donnees
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function updateInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "UPDATE EvaluationCasquette
                    SET description=?, date=?, reussi=?, resultatNumerique=?
                    WHERE id_evaluation_casquette=?";
        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in update]";
            return;
        }

        $desc   = $obj->getDescription();
        $date   = $obj->getDate();
        $reussi = $obj->getReussi();
        $resultatNumerique = $obj->getResultatNumerique();
        $id     = $obj->getId();
        
        $stmt->bind_param(
            "ssbii",$desc, $date, $reussi, $resultatNumerique, $id);
        DatabaseManager::persist($stmt, $conn);
    }

    /** Supprime l'objet dans la base de donnees
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function deleteInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "DELETE FROM EvaluationCasquette WHERE id_evaluation_casquette = ?;";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in delete]";
            return;
        }
        
        $id = $obj->getId();
        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }
}

/**
 * EvaluationCasquetteFromMap
 *
 * @param  mixed $map
 *
 * @return void
 */
function EvaluationCasquetteFromMap($map)
{
    if (isset($map["id"])) {
        $id = (int)$map["id"];
        $description = $map["description"];
        $date = $map["date"];
        $reussi = (bool)$map["reussi"];
        $resultatNumerique = (int)$map["resultatNumerique"];
        return new EvaluationCasquette($id, $description, $date, $reussi, $resultatNumerique);
    }
    return null;
}
