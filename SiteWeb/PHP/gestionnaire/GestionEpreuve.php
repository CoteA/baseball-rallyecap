<?php
/**********************************************************
Fichier : GestionEpreuve.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des Epreuve
===========================================================
Vérification :
Date        Nom
2019-04-23  Christophe Leclerc
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-25  Christophe Leclerc  Ajout de la function getEpreuveForCasquette
2019-04-28  Christophe Leclerc  Ajout de la fonction getEpreuveWithNoCasquette
2019-04-29  Christophe Leclerc  Ajout de la fonction setCasquetteNull
**********************************************************/

if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
    require_once $root . "PHP/CLASS/Epreuve.php";
} else {
    require_once ROOT_DIR . "PHP/utils/DatabaseManager.php";
    require_once ROOT_DIR . "PHP/CLASS/Epreuve.php";
}

class GestionEpreuve
{

    /** 
     * Ajoute l'objet dans la base de donnees.
     * @param  mixed $obj
     * @param  mixed $conn
     * @return void
     */
    public static function createInDB($obj, $conn = null) {
        if (null == $conn) $conn = DatabaseManager::newConnection();
        if (null == $obj) throw new Exception("Object is null", 1);

        // Genere un id s'il n'y en a pas
        if( -1 == $obj->getId()){
            $nextId = DatabaseManager::nextID("Epreuve","id_epreuve");
            $obj->setId($nextId);
        }
        
        $sql = "INSERT INTO epreuve
                (id_epreuve, nom, description, nbr_minimum_reussite, nbr_essai_total, id_type_epreuve)
                VALUES (?, ?, ?, ?, ?, ?)";

        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in create]"; return;}

        $id           = $obj->getId();
        $nom          = $obj->getNom();
        $desc         = $obj->getDescription();
        $nbEssaiMin   = $obj->getNbEssaiMin();
        $nbEssaiTotal = $obj->getNbEssaiTotal();
        $idTypeEpreuve = $obj->getIdTypeEpreuve();

        $stmt->bind_param("issiii",
            $id, $nom, $desc, $nbEssaiMin, $nbEssaiTotal, $idTypeEpreuve);
        DatabaseManager::persist($stmt, $conn);
        
    }

    
    /** Modifi l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function updateInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "UPDATE Epreuve SET nom=?, description=?, nbr_minimum_reussite=?, nbr_essai_total=?, id_type_epreuve=? WHERE id_epreuve=?";
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in update]"; return;}

        $nom          = $obj->getNom();
        $desc         = $obj->getDescription();
        $nbEssaiMin   = $obj->getNbEssaiMin();
        $nbEssaiTotal = $obj->getNbEssaiTotal();
        $idTypeEpreuve = $obj->getIdTypeEpreuve();
        
        $id = $obj->getId();

        
        $stmt->bind_param("ssiiii",
            $nom, $desc, $nbEssaiMin, $nbEssaiTotal, $idTypeEpreuve, $id);
        DatabaseManager::persist($stmt, $conn);
    }


    /** Supprime l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function deleteInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "DELETE FROM Epreuve WHERE id_epreuve = ?;";

        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in delete]"; return;}

        // Selection du ID
        if (-1 == $obj->getId())
            $id = DatabaseManager::getIdWhere("Epreuve","id_epreuve","nom", $obj->getNom());
        else $id = $obj->getId();
        
        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }


    /** Permet de désaffecter une casquette d'une épreuve.
     *
     * @param [type] $id l'id de l'épreuve mettre à null.
     * @return $epreuve trouver dans la base de données.
     */
    public function setCasquetteNull($id, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "UPDATE epreuve SET id_casquette = NULL WHERE id_epreuve = ?;";

        $stmt = $conn->prepare($sql);

        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }


    /** Permet d'affecter une casquette à une épreuve.
     *
     * @param [type] $id de l'épreuve qu'on veut changer
     * @param [type] $idCasquette que l'on veut relié
     */
    public function setCasquette($id, $idCasquette, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "UPDATE epreuve SET id_casquette = ? WHERE id_epreuve = ?;";

        $stmt = $conn->prepare($sql);

        $stmt->bind_param("ii", $idCasquette, $id);
        DatabaseManager::persist($stmt, $conn);
    }

    /**  Permet d'aller chercher les épreuves pour une casquette
     *
     * @param [id casquette] $id l'id de la casquette pour lesquel on veut l'épreuve.
     * @param [Connection SQL] $conn permet de passer une connection.
     * @return $epreuves est un tableau d'épreuves.
     */
    public function getEpreuveForCasquette($id)
    {

        $sql = "SELECT * FROM epreuve WHERE id_casquette = ".$id.";";

        $rs = DatabaseManager::getResultSet($sql);
        $epreuves = array();
        foreach($rs as $row){
            $epreuves[] = new Epreuve($row['id_epreuve'], $row['nom'], $row['description'], $row['nbr_minimum_reussite'], $row['nbr_essai_total'], $row['id_casquette'], $row['id_type_epreuve']);
        }
        return $epreuves;
    }

    /**  Permet d'aller chercher les épreuves qui n'ont pas de casquette.
     *
     * @param [Connection SQL] $conn permet de passer une connection.
     * @return $epreuves est un tableau d'épreuves.
     */
    public function getEpreuveWithNoCasquette($conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "SELECT * FROM epreuve WHERE id_casquette IS NULL;";

        $rs = DatabaseManager::getResultSet($sql);
        $epreuves = array();
        foreach($rs as $row){
            $epreuves[] = new Epreuve($row['id_epreuve'], $row['nom'], $row['description'], $row['nbr_minimum_reussite'], $row['nbr_essai_total'], $row['id_casquette'], $row['id_type_epreuve']);
        }
        return $epreuves;
    }

    /**
     * getAll
     *
     * @param  mixed $conn
     * @return void
     */
    public function getAll($conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "SELECT * FROM epreuve;";

        $rs = DatabaseManager::getResultSet($sql);
        $epreuves = array();
        foreach($rs as $row){
            $epreuves[] = new Epreuve($row['id_epreuve'], $row['nom'], $row['description'], $row['nbr_minimum_reussite'], $row['nbr_essai_total'], $row['id_casquette'], $row['id_type_epreuve']);
        }
        return $epreuves;
    }

    /**
     * getById
     *
     * @param  mixed $id
     * @param  mixed $conn
     * @return void
     */
    public function getById($id, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "SELECT * FROM epreuve WHERE id_epreuve = ".$id.";";

        $rs = DatabaseManager::getResultSet($sql);
        foreach($rs as $row){
            $epreuve= new Epreuve($row['id_epreuve'], $row['nom'], $row['description'], $row['nbr_minimum_reussite'], $row['nbr_essai_total'], $row['id_casquette'], $row['id_type_epreuve']);
        }
        return $epreuve;
    }
}

/** Cree un Epreuve a partir d'une map associative
 * @param  mixed $map
 * @return void
 */
function EpreuveFromMap($map)
{
    $id         = isset($map['id'])           ? $map['id']          : -1;
    $nom        = isset($map['nom'])          ? $map['nom']         : "_";
    $description= isset($map['description'])  ? $map['description'] : "_";
    $nbEssaiMin = isset($map['nbEssaiMin'])   ? $map['nbEssaiMin']  : 0;
    $nbEssaiTotal=isset($map['nbEssaiTotal']) ? $map['nbEssaiTotal']: 0;

    return new Epreuve( $id, $nom, $description, $nbEssaiMin, $nbEssaiTotal);
}
