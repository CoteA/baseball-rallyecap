<?php
/**********************************************************
Fichier : GestionTypeBenevole.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des TypeBenevole
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  utilisé? 
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once("PHP/utils/DatabaseManager.php");

/** */
class GestionTypeBenevole
{
    /** Ajoute l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function createInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "INSERT INTO TypeBenevole
                (id_type_benevole, nom, description)
                VALUES (?, ?, ?)";
        
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in create]"; return;}
        
        $id     = $obj->getId();
        $nom    = $obj->getNom();
        $desc   = $obj->getDescription();
        
        $stmt->bind_param("iss", $id, $nom, $desc);
        DatabaseManager::persist($stmt, $conn);
    }
    
    /** Modifi l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function updateInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "UPDATE TypeBenevole SET nom=?, description=? WHERE id_type_benevole=?";
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in update]"; return;}
        
        $nom    = $obj->getNom();
        $desc   = $obj->getDescription();
        $id     = $obj->getId();
        
        $stmt->bind_param("ssi", $nom, $desc, $id);

        DatabaseManager::persist($stmt, $conn);
    }
    
    /** Supprime l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function deleteInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "DELETE FROM TypeBenevole WHERE id_type_benevole = ?;";
        
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in delete]"; return;}
        
        $id = $obj->getId();
        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }
} 

/** * TypeBenevoleFromMap
 *
 * @param  mixed $map
 *
 * @return void
 */
function TypeBenevoleFromMap($map)
{
    if(isset($map["id"])){
        $id = (int)$map["id"];
        $nom = $map["nom"];
        $description = $map["description"];
        return new TypeBenevole( $id, $nom, $description);
    }
    return null;
}
