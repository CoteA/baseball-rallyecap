<?php
/**********************************************************
Fichier : GestionJoueur.php
Auteur : Massinissa Talah
Date : 2019-04-16
Fonctionnalité : FW01 / Gestion des joueurs
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
    require_once $root . "PHP/CLASS/Joueur.php";
} else {
    require_once ROOT_DIR . "PHP/utils/DatabaseManager.php";
    require_once ROOT_DIR . "PHP/CLASS/Joueur.php";
}
ini_set('display_errors',1);
error_reporting(E_ALL);

/**
 * ouvrir une connexion
 *@return PDO $bdd
 */
function ouvrirConnection(){
    $bdd= new PDO('mysql:host=localhost;dbname=baseball_db','root','');
    return $bdd;
}

/** la classe permet de manipuler un joueur donc de l'ajouter à la bd, de le modifierJoueur
* de le supprimer ou de le trouver aussi dans la bd */
class GestionJoueur
{
    function __construct()
    {
    }

    /** ajouter un joueur dans la BD avec l'id de son tuteur
     */
    function ajouter($map) {

    //ouvrir une connexion
    $bdd=ouvrirConnection();

    //l'id du joueur à ajouter
    $dernierIDjoueur=0;

    //le joueur à ajouter
    $joueur=new Joueur($dernierIDjoueur,$map['prenom'],$map['nom'],$map['dateNaissance'],$map['casquettes'],$map['etat']);

    //récupérer l'id du tuteur du joueur qu'on veut ajouter
    $stmt=$bdd->prepare("SELECT id_tuteur FROM tuteur WHERE id_tuteur = (SELECT MAX(id_tuteur) FROM tuteur)");
    $stmt->execute();
    $dernierIDtuteur=$stmt->fetch();

    //insérer dans la BD le joueur
    $stmt=$bdd->prepare(
        "INSERT INTO joueur(id_joueur, prenom, nom, date_naissance,id_casquette, est_actif,id_tuteur)
        VALUES (?, ?, ?, ?, ?, ?, ?)");

        $id=$joueur->getId(); ;
        $prenom=$joueur->getPrenom();
        $nom=$joueur->getNom();
        $dateNaissance=$joueur->getDateNaissance();
        $casquette=$joueur->getIdCasquette();
        if ($joueur->getIdEtat()=='true') {
            $etat = 1;
        }
        else if($joueur->getIdEtat()=='false') {
            $etat = 0;
        }

        $stmt->bindParam(1,$id );
        $stmt->bindParam(2,$prenom);
        $stmt->bindParam(3, $nom);
        $stmt->bindParam(4, $dateNaissance);
        $stmt->bindParam(5, $casquette);
        $stmt->bindParam(6,$etat);
        $stmt->bindParam(7, $dernierIDtuteur[0]);

        $stmt->execute();
    }


    /** modifier le joueur sélectionné
     */
    function modifier($map)
    {
        $joueur=$this->trouverJoueur($map['nomHidden'],$map['prenomHidden'],$map['dateNaissanceHidden']);

        $bdd=ouvrirConnection();

        $stmt = $bdd->prepare(
            "UPDATE Joueur
            SET prenom = ? , nom = ? , date_naissance = ?, id_casquette = ?, est_actif = ?
            WHERE id_joueur = ?");

        $prenom=$map['prenom'];
        $nom=$map['nom'];
        $dn=$map['dateNaissance'];
        $casquette=$map['casquettes'];
        if ($map['etat']=='true') {
            $etat = 1;
        }
        else if($map['etat']=='false') {
            $etat = 0;
        }
        $id=$joueur->getId();

        $stmt->bindParam(1,$prenom);
        $stmt->bindParam(2, $nom);
        $stmt->bindParam(3, $dn);
        $stmt->bindParam(4, $casquette);
        $stmt->bindParam(5,$etat);
        $stmt->bindParam(6,$id);

        $stmt->execute();
    }

    /**  trouver le joueur sélectionné dans la BD selon
     *@param string nom
    *@param string prenom
    *@param string date de naissance
    *@return Joueur
    */
    function trouverJoueur($nom,$prenom,$dn)
    {
        $bdd=ouvrirConnection();

        $stmt=$bdd->prepare("SELECT id_joueur,prenom,nom, date_naissance,id_casquette,est_actif, id_tuteur FROM Joueur WHERE (prenom = ? AND nom = ? AND date_naissance = ?)");

        $stmt->bindParam(1,$prenom);
        $stmt->bindParam(2, $nom);
        $stmt->bindParam(3, $dn);

        $stmt->execute();
        $result=$stmt->fetch();

        $joueurTrouve=new Joueur($result[0],$result[1],$result[2],$result[3],$result[4],$result[5]);

        return $joueurTrouve;
    }

    /**  trouver le joueur sélectionné dans la BD selon
     *@param int id
    *@return Joueur
    */
    function trouverJoueurById($id)
    {
        $bdd=ouvrirConnection();
        $stmt=$bdd->prepare("SELECT * FROM Joueur WHERE (id_joueur = ?)");
        $stmt->bindParam(1,$prenom);
        $stmt->execute();
        $result=$stmt->fetch();
        $joueurTrouve=new Joueur($result[0],$result[1],$result[2],$result[3],$result[4],$result[5]);
        return $joueurTrouve;
    }


    /** supprimer le joueur selectionné de la BD
    * @param  mixed $map
    *
    * @return void
    */
    function supprimer($map) {

        $joueurSupp=$this->trouverJoueur($map['nomHidden'],$map['prenomHidden'],$map['dateNaissanceHidden']);

        $bdd=ouvrirConnection();

        $queryDelete="DELETE FROM Joueur WHERE id_joueur = ?;";
        $statement=$bdd->prepare($queryDelete);

        $id_Joueur=$joueurSupp->getId();
        $statement->bindParam(1,$id_Joueur);

        $statement->execute();
    }

}


?>
