<?php
/**********************************************************
Fichier : GestionEvaluationEpreuve.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des EvaluationEpreuve
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  utilisé?
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once("PHP/utils/DatabaseManager.php");

/** */
class GestionEvaluationEpreuve
{

    /** Ajoute l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function createInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "INSERT INTO EvaluationEpreuve
                (id_evaluation_epreuve, description, date, reussi, resultatNumerique, idEpreuve)
                VALUES (?, ?, ?, ?, ?, ?)";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in create]";
            return;
        }

        $id         = $obj->getId();
        $desc       = $obj->getDescription();
        $date       = $obj->getDate();
        $reussi     = $obj->getReussi();
        $resultatNumerique = $obj->getResultatNumerique();
        $idEpreuve  = $obj->getIdEpreuve();
        
        $stmt->bind_param("issbii",
            $id, $desc, $date, $reussi,
            $resultatNumerique, $idEpreuve);
        DatabaseManager::persist($stmt, $conn);
    }

    /** Modifi l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function updateInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = newConnection();

        $sql = "UPDATE EvaluationEpreuve
                SET description=?, date=?, reussi=?, resultatNumerique=?, idEpreuve=?
                WHERE id_evenement_evaluation=?";
        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in update]";
            return;
        }

        $id         = $obj->getId();
        $desc       = $obj->getDescription();
        $date       = $obj->getDate();
        $reussi     = $obj->getReussi();
        $resultatNumerique = $obj->getResultatNumerique();
        $idEpreuve  = $obj->getIdEpreuve();
        
        $stmt->bind_param(
            "ssbiii", $desc, $date, $reussi,
            $resultatNumerique, $idEpreuve, $id);
        
        DatabaseManager::persist($stmt, $conn);
    }

    /** Supprime l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function deleteInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "DELETE FROM EvaluationEpreuve WHERE id_evaluation_epreuve = ?;";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in delete]";
            return;
        }

        $id = $obj->getId();
        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }
}

/**
 * EvaluationEpreuveFromMap
 *
 * @param  mixed $map
 *
 * @return void
 */
function EvaluationEpreuveFromMap($map)
{
    if (isset($map["id"])) {
        $id = (int)$map["id"];
        $description = $map["description"];
        $date = $map["date"];
        $reussi = (bool)$map["reussi"];
        $resultatNumerique = (int)$map["resultatNumerique"];
        $idEpreuve = (int)$map["idEpreuve"];
        return new EvaluationEpreuve($id, $description, $date, $reussi, $resultatNumerique, $idEpreuve);
    }
    return null;
}
