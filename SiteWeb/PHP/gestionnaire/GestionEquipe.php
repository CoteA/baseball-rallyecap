<?php
/**********************************************************
Fichier : GestionEquipe.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des Equipe
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  utilisé?
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once("PHP/utils/DatabaseManager.php");

/** */
class GestionEquipe
{
    /** Ajoute l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     * @return void
     */
    public static function createInDB($obj, $conn = null)
    {
        if ($conn == null) $conn = DatabaseManager::newConnection();

        $sql = "INSERT INTO Equipe (id_equipe, nom) VALUES (?, ?)";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in create]";
            return;
        }

        $id  = $obj->getId();
        $nom = $obj->getNom();
        
        $stmt->bind_param("is", $id, $nom);
        DatabaseManager::persist($stmt, $conn);
    }
    
    /** Modifi l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     * @return void
     */
    public static function updateInDB($obj, $conn = null) {
            if ($conn == null) $conn = DatabaseManager::newConnection();
            
            $sql = "UPDATE Equipe SET nom=? WHERE id_equipe=?";
            $stmt = $conn->prepare($sql);
            if (false == $stmt){ echo "[Error in update]"; return;}
            
            $nom = $obj->getNom();
            $id  = $obj->getId();
            
            $stmt->bind_param("si", $nom, $id);
            DatabaseManager::persist($stmt, $conn);
    }
    
    /** Supprime l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     * @return void
     */
    public static function deleteInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "DELETE FROM Equipe WHERE id_equipe = ?;";
        
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in delete]"; return;}
        
        $id = $obj->getId();
        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }
} 

/** Cree un Equipe a partir d'une map associative
 * @param  mixed $map
 * @return void
 */
function EquipeFromMap($map)
{
    if(isset($map["id"])){
        $id = (int)$map["id"];
        $nom = $map["nom"];
        return new Equipe( $id, $nom);
    }
    return null;
}
?>

