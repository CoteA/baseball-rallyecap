<?php
/**********************************************************
Fichier : GestionGroupeEvaluation.php
Auteur : Massinissa Talah
Date : 2019-04-21
Fonctionnalité : Gestion des GroupeEvaluation
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
ini_set ('max_execution_time', 0);


require_once("../utils/DatabaseManager.php");
require_once("../CLASS/Joueur.php");
require_once("../CLASS/Benevole.php");

require_once("../CLASS/GroupeEvaluation.php");
require_once("../gestionnaire/GestionBenevole.php");

/** Retourne une connexion à la BD
 *@return PDO $bdd
 */
function ouvrirConnection(){
    $bdd= new PDO('mysql:host=localhost;dbname=baseball_db','root','');
    return $bdd;
}

define("NB_JOUEUR_GROUPE", "8"); //nombre de joueur par groupe d'évaluation

/**
 * GestionGroupeEvaluation permet la manipulation en lien avec le GroupeEvaluation
 * Elle nous permet d'ajouter un groupe de mettre à jour le groupe d'un joueur, de composer les groupes d'Evaluation
 */
class GestionGroupeEvaluation
{


    function __construct()
    {}

    /** Composer les groupes d'évaluation selon la date choisie et fait l'assiniation des joueurs
     *@return int   $nb_groupe_cree    nb de groupe total crée
      */
    function composer($map)
    {
        $idBenevoleGroupe=1;  //le bénévole qui s'occupe du groupe(chaque groupe un benevole)
        $nb_groupe_cree=0;    //nb de groupe total crée

        for ($j=1; $j <7; $j++)
        {
            //pour chaque casquette
            $casquetteJoueur=$j;
            $listejoueurParCasquette=$this->listJoueursCasquette($casquetteJoueur);

            if (count($listejoueurParCasquette)>0)
            {
                //définir le nombre de groupe à créer pour cette casquette
                $nbGroupe=(int)(count($listejoueurParCasquette)/NB_JOUEUR_GROUPE);
                if ($nbGroupe==0) {
                    $nbGroupe=1;
                }
                else if($nbGroupe % NB_JOUEUR_GROUPE != '2' && $nbGroupe % NB_JOUEUR_GROUPE != '0')
                {
                    $nbGroupe++;
                }
                //incrémenter le nombre de groupe créé pour cet evenement
                $nb_groupe_cree += $nbGroupe;

                $gestionBenevole=new gestBenevole();
                $lesBenevoles=$gestionBenevole->getAll();

                //créer les groupes
                for ($i=0; $i < $nbGroupe; $i++)
                {
                    $this->ajouterGroupe($lesBenevoles[$idBenevoleGroupe],$map['evenements']);
                    $idBenevoleGroupe++;
                }

                $compteur=0;
                //récupérer l'id du groupe qu'on a ajouté
                $dernierIDGroupe=$this->reccupererIdGroupe();

                //assigner les joueurs aux groupes
                for ($k=0; $k <count($listejoueurParCasquette) ; $k++)
                {
                    $compteur++;
                    $idJ = $listejoueurParCasquette[$k]->getId();
                    $this->upDateGroupejoueur($idJ,$dernierIDGroupe);
                    if ($compteur==NB_JOUEUR_GROUPE)
                    {
                        $dernierIDGroupe--;
                    }
                }
            }
        }
        return $nb_groupe_cree;
    }

    /** crée et ajoute un groupe dans la BD avec le benevole et l'evenement qui lui convient
     * @param  Benevole $ben
     * @param  int $event
     *
     * @return void
     */
    function ajouterGroupe($ben,$event)
    {
        $bdd=ouvrirConnection();
        $id=0;
        $groupe=new GroupeEvaluation($id,$ben,$event);

        $stmt=$bdd->prepare("INSERT INTO GroupeEvaluation(id_groupe_evaluation,id_evenement_evaluation,id_benevole) VALUES (?, ?, ?)");
        $id_groupe_evaluation=$groupe->getIdGroupe();
        $id_evenement_evaluation=$groupe->getIdEvent();
        $id_benevole=$groupe->getBenevole()->getId();

        $stmt->bindParam(1, $id_groupe_evaluation);
        $stmt->bindParam(2, $id_evenement_evaluation);
        $stmt->bindParam(3, $id_benevole);

        $stmt->execute();

        $stmt->closeCursor();
        $bdd = null;
    }

    /** Retourne l'id du dernier groupe inséré dans la BD
     *@return int idDernierGroupe
     */
    function reccupererIdGroupe()
    {
        $bdd=ouvrirConnection();

        $stmt=$bdd->prepare("SELECT id_groupe_evaluation FROM GroupeEvaluation WHERE id_groupe_evaluation = (SELECT MAX(id_groupe_evaluation) FROM GroupeEvaluation)");
        $stmt->execute();
        $result=$stmt->fetch();
        return $result['id_groupe_evaluation'];

        $stmt->closeCursor();
        $bdd = null;
    }

    /**
     * Récupère et retourne la liste des joueurs selon la couleur de la casquette
     * @param int idCasquette
     * @return array $listejoueur
     */
    function listJoueursCasquette($casquetteJoueur)
    {
        $link = mysqli_connect("127.0.0.1", "root", "", "baseball_db");//= DatabaseManager::newConnection();

        $requete = "SELECT * FROM Joueur WHERE id_casquette=?";
        $state = $link->prepare($requete);
        $state->bind_param("i", $casquetteJoueur);
        $state->execute();
        $result = $state->get_result();

        $listejoueur=array();
        while($row = mysqli_fetch_assoc($result))
        {
            $joueur=new Joueur($row['id_joueur'],$row['prenom'],$row['nom'],$row['date_naissance'],$row['id_casquette'],$row['est_actif']);
            array_push($listejoueur,$joueur);
        }
        $state->close();
        mysqli_close($link);

        return $listejoueur;
    }

    /**
     * fait une mise à jour du groupe auquel le jour appartient
     *@param int $idJoueur
     *@param int $idGroupe
     */
    function upDateGroupejoueur($idJoueur,$idGroupe)
    {
        $bdd=ouvrirConnection();

        $stmt = $bdd->prepare(
          "UPDATE Joueur
          SET id_groupe_evaluation = ?
          WHERE id_joueur = ?");

        $stmt->bindParam(1,$idGroupe);
        $stmt->bindParam(2, $idJoueur);
        $stmt->execute();

        $stmt->closeCursor();
        $bdd = null;
    }


    /** Retourne la liste des joueur selon le groupe
     *@param int $idGroupe
     *@return array listJoueur
     */
    function getJoueurParGroupe($idGroupe)
    {
        $bdd=ouvrirConnection();

        $stmt=$bdd->prepare("SELECT * FROM Joueur WHERE id_groupe_evaluation = ?");
        $stmt->bindParam(1,$idGroupe);
        $stmt->execute();

        $listejoueur=array();
        while ($row=$stmt->fetch())
        {
          $joueur=new Joueur($row['id_joueur'],$row['prenom'],$row['nom'],$row['date_naissance'],$row['id_casquette'],$row['est_actif']);
          array_push($listejoueur,$joueur);
        }
        $stmt->closeCursor();
        $bdd = null;

        return $listejoueur;
    }

    /**
     * retourn le nom et prenom d'un benevole qui s'ocuupe d'un GroupeEvaluation
     *@param int
     *@return string
     */
    function affcherBenevoleDuGroupe($id)
    {
        $bdd=ouvrirConnection();

        $stmt=$bdd->prepare("SELECT CONCAT(b.nom,' ',b.prenom)
                      FROM GroupeEvaluation g
                      INNER JOIN Benevole b ON b.id_benevole=g.id_benevole
                      WHERE g.id_groupe_evaluation = ?");

        $stmt->bindParam(1,$id);
        $stmt->execute();

        $nomBenevole=$stmt->fetch();
        return $nomBenevole[0];
    }

}
