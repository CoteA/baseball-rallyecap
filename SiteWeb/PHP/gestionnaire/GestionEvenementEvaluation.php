<?php
/**********************************************************
Fichier : GestionEvenementEvaluation.php
Auteur : Talah Massinissa
Date : 2019-04-21
Fonctionnalité : Gestion des EvenementEvaluation
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/

require_once("../utils/DatabaseManager.php");
require_once ("../CLASS/EvenementEvaluation.php");

function ouvrirConnection(){
  $bdd= new PDO('mysql:host=localhost;dbname=baseball_db','root','');
  return $bdd;
}

/**
 * La classe GestionEvenementEvaluation permet de faire un lien avec la BD et manipuler un evenement.
 * On y trouve les méthodes pour ajouter un evenement
 */
class GestionEvenementEvaluation
{
  function __construct()
  {
  }

/**
 *ajoute dans la BD un évenement à partir des informations entrées par l'utilisateur
   * @param  mixed $map
   *
   * @return void
   */
  function ajouter($map)
  {
    $bdd=ouvrirConnection();

    $idEvent=0;
    $event=new EvenementEvaluation($idEvent,$map["adresse"],$map["jour"]."-".$map['mois']."-".$map['annee'],$map["heure"].":".$map['min']);

    $sql = "INSERT INTO evenementevaluation
                (id_evenement_evaluation, adresse, date, heure)
                VALUES (?, ?, ?, ?)";
    $stmt=$bdd->prepare($sql);

    $id     = $event->getId();
    $adresse= $event->getAdresse();
    $date   = $event->getDate();
    $heure  = $event->getHeure();

    $stmt->bindParam(1,$id);
    $stmt->bindParam(2,$adresse);
    $stmt->bindParam(3,$date);
    $stmt->bindParam(4,$heure);

    $stmt->execute();

  }
}
