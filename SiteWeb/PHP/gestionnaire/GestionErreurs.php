<!--/**********************************************************
Fichier :           GestionErreurs.php
Auteur  :           Maïka Forestal
Date    :           30 avril 2019
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/-->

<?php
/**
 * Fonctions indiquant qu'un champs est invalide.
 *********************************************************/
/**
 * Le nom est trop long. Il doit avoir moins de 255 caractères.
 * Le nom doit débuter par une majuscule.
 * Le nom ne peux pas contenir de nombre.
 */
function nomInvalide(){
   echo " <div id=\"nomInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}
/**
 * Le prénom est trop long. Il doit avoir moins de 255 caractères.
 * Le prénom doit débuter par une majuscule.
 * Le prénom ne peux pas contenir de nombre.
 */
function prenomInvalide(){
    echo " <div id=\"prenomInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

/**
 * Le no. de téléphone n'est pas saisie dans le bon format.
 */
function telInvalide(){
    echo " <div id=\"telInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

/**
 * Le courriel est trop long. Il doit avoir moins de 255 caractères.
 * L'addresse courriel n'existe pas
 */
function courrielInvalide(){
    echo " <div id=\"courrielInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

/**
 * Ce nom d'utilisateur est déjà utilisé! Essayez-en un autre.
 * Le nom d'utilisateur ne peux pas avoir plus de 255 caractères.
 */
function utilisateurInvalide(){
    echo " <div id=\"utilisateurInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

/**
 * Le mot de passe ne peux pas avoir plus de 255 caractères.
 */
function motDePasseInvalide(){
    echo " <div id=\"motDePasseInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

/**
 * La date est invalide.
 * Il n'y a aucun groupe d'enfant pour cette date.
 */
function dateInvalide(){
    echo " <div id=\"dateInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

function anneeInvalide(){
    echo " <div id=\"anneeInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

function adresseInvalide(){
    echo " <div id=\"adresseInvalide\" class=\" col-12 col-md-12 col-xl-12 textError\" hidden></div>";
}

?>
