<?php
/**********************************************************
Fichier :           gestBenevoles.php
Auteur  :           Maïka Forestal
Date    :           18 avril 2019
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
**********************************************************/

/**
 * La classe gestBenevole contient un bénévole,
 * et permet de faire un lien avec la base de donnée.
 * On y trouve les méthodes pour ajouter, modifier, rechercher,
 * et supprimer un bénévole dans la base de données.
 */
class gestBenevole
{
    private $benevole;

    /**
     * Constructeur de bénévole
     * @param Benevole $benevole (default: NULL)
     */
    public function __construct($benevole = null)
    {
        // Si le bénévole est null on en crée un
        if ($benevole == null) {
            $this->benevole = new Benevole();
        }
        // Sinon, on set le bénévole de la classe, et on lui met un id
        else {
            $this->benevole = $benevole;
            $this->benevole->setId($this->nextID());
        }
    }

    /** Renvoie le bénévole
     * @return Benevole $this->benevole
     */
    public function getBenevole()
    {
        return $this->benevole;
    }

    /** Ajoute le bénévole dans la base de données
     * @param  mixed $conn
     *
     * @return void
     */
    public function ajouter($conn = null)
    {
        if ($conn == null) {
            $conn = DatabaseManager::newConnection();
            ;
        }

        $sql = "INSERT INTO benevole(id_benevole, prenom, nom, telephone, courriel, username, mot_de_passe, est_actif, id_type_benevole)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Erreur dans l'ajout d'un bénévole dans la base de donnée]";
            return;
        }

        $stmt->bind_param(
            "issssssii",
            $this->benevole->getId(),
            $this->benevole->getPrenom(),
            $this->benevole->getNom(),
            $this->benevole->getTelephone(),
            $this->benevole->getCourriel(),
            $this->benevole->getUsername(),
            $this->benevole->getMotDePasse(),
            $this->benevole->getActif(),
            $this->benevole->getTypeBenevole()
        );
        DatabaseManager::persist($stmt, $conn);
    }

    /**
     * Modifie le bénévole dans la base de donnée
     * @param  mixed $conn
     * @return void
     */
    public function modifier($conn = null)
    {
        if ($conn == null) {
            $conn = DatabaseManager::newConnection();
        }

        $sql = "UPDATE Benevole SET prenom=?, nom=?, telephone=?,courriel=?, username=?, mot_de_passe=?, est_actif=?, id_type_benevole=?
                WHERE id_benevole=?;";
        $stmt = $conn->prepare($sql);

        if (false == $stmt) {
            echo "[Error in update]";
            return;
        }
        echo "Prénom : " . $this->benevole->getId();
        $stmt->bind_param(
            "ssssssiii",
            $this->benevole->getPrenom(),
            $this->benevole->getNom(),
            $this->benevole->getTelephone(),
            $this->benevole->getCourriel(),
            $this->benevole->getUsername(),
            $this->benevole->getMotDePasse(),
            $this->benevole->getActif(),
            $this->benevole->getTypeBenevole(),
            $this->benevole->getId()
        );
        DatabaseManager::persist($stmt, $conn);
    }

    /** Supprime le bénévole de la base de donnée
     * @param  mixed $conn
     *
     * @return void
     */
    public function supprimer($conn = null)
    {
        if ($conn == null) {
            $conn = DatabaseManager::newConnection();
        }
        $sql = "UPDATE benevole
                SET deleted='true'
                WHERE id_benevole=?";

        $stmt = $conn->prepare($sql);
        if (false == $stmt) {
            echo "[Error in delete]";
            return;
        }
        $stmt->bind_param("i", $this->benevole->getId());
        DatabaseManager::persist($stmt, $conn);
    }


    /**
     * Récupère tout les bénévoles de la base de donnée
     * et renvoie la liste.
     * @return array $listBenevoles
     */
    public function getAll()
    {
        $listBenevoles = array();
        for ($i = 1; $i < (int) $this->nextID() ; $i++) {
            $ben= $this->trouverBenevoleById((int)$i);
            array_push($listBenevoles, $ben);
        }
        return $listBenevoles;
    }

    /**
     * Renvoie le prochain id disponible dans la base de donnée
     * @return int $id
     */
    public function nextID()
    {
        $link = DatabaseManager::newConnection();
        $requete = "SELECT MAX(id_benevole) AS 'dernierID' FROM Benevole";
        $state = $link->prepare($requete);
        $state->execute();
        $result = $state->get_result();
        $row = mysqli_fetch_assoc($result);

        $id = $row['dernierID'] + 1;
        $state->close();

        return $id;
    }

    /**
     * Affiche le type de bénévole en mot
     * @param   int      $id           Identifiant du bénévole
     * @return  string   $type         Type de bénévole
     */
    public function typeBenevole($id)
    {
        $link = DatabaseManager::newConnection();
        $requete = "SELECT t.nom FROM benevole as ben
                INNER JOIN typebenevole as t
                ON t.id_type_benevole = ben.id_type_benevole
                WHERE ben.id_benevole=?";

        $state = $link->prepare($requete);
        $state->bind_param("i", $id);
        $state->execute();
        $result = $state->get_result();
        $row = mysqli_fetch_assoc($result);

        $type = $row['nom'];
        $state->close();
        return $type;
    }

    /**
     * Trouve le bénévole à partir de son ID
     * @param int           $no                 Identifiant du bénévole
     * @return Benevole     $this->benevole     Le bénévole trouvé
     */
    public function trouverBenevoleById($no)
    {
        $link = DatabaseManager::newConnection();

        $requete = "SELECT * FROM Benevole WHERE id_benevole=?";
        $state = $link->prepare($requete);
        $state->bind_param("i", $no);
        $state->execute();
        $result = $state->get_result();
        $row = mysqli_fetch_assoc($result);

        $this->benevole = new Benevole($no, $row['prenom'], $row['nom'], $row['telephone'], $row['courriel'], $row['username'], $row['mot_de_passe'], $row['est_actif'], $row['id_type_benevole']);
        $state->close();
        return $this->benevole;
    }

    /**
     * Trouve le bénévole à partir de ses informations personnels
     * (Nom, prénom, no. de téléphone, adresse courriel)
     * @param Benevole $benevole  Le bénévole rechercher
     */
    public function trouverBenevole($benevole)
    {
        $link = DatabaseManager::newConnection();

        $requete = "SELECT * FROM Benevole WHERE nom=? AND prenom=? AND telephone=? AND courriel=?";
        $state = $link->prepare($requete);
        $state->bind_param(
            "ssss",
            $benevole->getNom(),
            $benevole->getPrenom(),
            $benevole->getTelephone(),
            $benevole->getCourriel()
    );
        $state->execute();
        $result = $state->get_result();
        $row = mysqli_fetch_assoc($result);
        $this->benevole = new Benevole($row['id_benevole'], $row['prenom'], $row['nom'], $row['telephone'], $row['courriel'], $row['mot_de_passe'], $row['username'], $row['est_actif'], $row['id_type_benevole']);
        $state->close();
    }

    /**
     * Trouve l'id du bénévole à a partir de son nom
     * @param Benevole $benevole  Le bénévole rechercher
     */
    public function trouverID($benevole)
    {
        $link = DatabaseManager::newConnection();

        $requete = "SELECT * FROM Benevole WHERE nom=? AND prenom=? OR courriel=?";
        $state = $link->prepare($requete);

        echo $state->bind_param(
            "sss",
            $benevole->getNom(),
            $benevole->getPrenom(),
            $benevole->getCourriel()
    );
        $state->execute();
        $result = $state->get_result();
        $row = mysqli_fetch_assoc($result);
        $state->close();
        $this->benevole->setId($row['id_benevole']);
    }
}

/**
 * Vérifie si le bénévole existe déjà dans la base de donnée
 * @param Benevole $benevole    Le bénévole rechercher
 * @return bool Vrai s'il existe
 */
function existe($benevole)
{
    $link = DatabaseManager::newConnection();

    $requete = "SELECT COUNT(*) AS count FROM Benevole WHERE nom=? AND prenom=? AND telephone=? AND courriel=?";
    $state = $link->prepare($requete);
    $state->bind_param(
        "ssss",
        $benevole->getNom(),
        $benevole->getPrenom(),
        $benevole->getTelephone(),
        $benevole->getCourriel()
    );

    $state->execute();
    $result = $state->get_result();
    $row = mysqli_fetch_assoc($result);
    $state->close();
    return $row["count"] != 0;
}

/**
 * Vérifie si le bénévole existe déjà dans la base de donnée
 * @param Benevole $benevole    Le bénévole rechercher
 * @return bool Vrai s'il existe
 */
function pasSupprimer($benevole) // TODO : Trouver un nom plus descriptif
{
    $link = DatabaseManager::newConnection();

    $requete = "SELECT deleted FROM Benevole WHERE nom=? AND prenom=? AND telephone=? AND courriel=?";
    $state = $link->prepare($requete);
    $state->bind_param(
        "ssss",
        $benevole->getNom(),
        $benevole->getPrenom(),
        $benevole->getTelephone(),
        $benevole->getCourriel()
    );

    $state->execute();
    $result = $state->get_result();
    $row = mysqli_fetch_assoc($result);
    $state->close();
    return $row["deleted"] != 'true';
}

/**
 * Crée un bénévole à partir d'une map
 * @param  mixed $map
 *
 * @return void
 */
function benevoleFromMap($map)
{
    if (isset($map["nomEvaluateur"])) {
        $prenom = $map["prenomEvaluateur"];
        $nom = $map["nomEvaluateur"];
        $telephone = $map["telEvaluateur"];
        $courriel = $map["courrielEvaluateur"];
        $username = $map["utilisateur"];
        $motDePasse = $map["mdp"];
        if ($map["Actif"] == 'on') {
            $actif = true;
        } else {
            $actif = false;
        }
        $type = $map["typeBenevole"];

        return new Benevole(0, $prenom, $nom, $telephone, $courriel, $username, $motDePasse, $actif, $type);
    }
    else if(isset($map["nom"])){
        $name = $map["nom"];
        $typeBen = $map["type"];
        $tel = $map["telephone"];
        $email = $map["courriel"];
        return new Benevole(0, NULL, $name, $tel, $email, NULL, NULL, false , $typeBen);
    }
    return new Benevole();
}
