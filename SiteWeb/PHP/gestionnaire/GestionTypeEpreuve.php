<?php
/**********************************************************
Fichier : GestionTypeEpreuve.php
Auteur : Anthony Cote
Date : 2019-04-21
Fonctionnalité : Gestion des TypeEpreuve
===========================================================
Vérification :
Date        Nom
2019-04-29  Christophe Leclerc  Approuvé  
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-29  Christophe Leclerc  Ajout de la condition pour le root dir
**********************************************************/

if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
} else {
    require_once ROOT_DIR. "PHP/utils/DatabaseManager.php";
}

/** */
class GestionTypeEpreuve
{
    
    /** Ajoute l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function createInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "INSERT INTO typeepreuve (id_type_epreuve, nom, description) VALUES (?, ?, ?)";
        
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ 
            echo "[Error in create]"; return;
        }
        
        $id     = $obj->getId();
        $nom    = $obj->getNom();
        $desc   = $obj->getDescription();
        
        $stmt->bind_param("iss", $id, $nom, $desc);
        
        DatabaseManager::persist($stmt, $conn);
    }
    
    /** Modifi l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function updateInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "UPDATE TypeEpreuve SET nom=?, description=? WHERE id_type_epreuve=?";
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in update]"; return;}
        
        $id     = $obj->getId();
        $nom    = $obj->getNom();
        $desc   = $obj->getDescription();
        
        $stmt->bind_param("ssi", $nom, $desc, $id);
        DatabaseManager::persist($stmt, $conn);
    }
    
    /** Supprime l'objet dans la base de donnees.
     *
     * @param  mixed $obj
     * @param  mixed $conn
     *
     * @return void
     */
    public static function deleteInDB($obj, $conn = null) {
        if ($conn == null) $conn = DatabaseManager::newConnection();
        
        $sql = "DELETE FROM TypeEpreuve WHERE id_type_epreuve = ?;";
        
        $stmt = $conn->prepare($sql);
        if (false == $stmt){ echo "[Error in delete]"; return;}
        
        $id = $obj->getId();
        $stmt->bind_param("i", $id);
        DatabaseManager::persist($stmt, $conn);
    }
}

/**
 * TypeEpreuveFromMap
 *
 * @param  mixed $map
 *
 * @return void
 */
function TypeEpreuveFromMap($map)
{
    if(isset($map["id"])){
        $id = (int)$map["id"];
        $nom = $map["nom"];
        $description = $map["description"];
        return new TypeEpreuve( $id, $nom, $description);
    }
    return null;
}
?>

