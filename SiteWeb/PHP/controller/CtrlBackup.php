<?php
/**********************************************************
Fichier :   CtrlBackup.php
Auteur :    Anthony Cote
Date :      2019-0?-??
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom                 Approuvé
---------------------------------------------
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
Date        Nom             Description
2019-05-05  Maïka Forestal  Ajout de l'entête de fichier
**********************************************************/
    require_once 'PHP/utils/DatabaseManager.php';

    
    /** Lit l'action a effectuer dans le POST et lance l'action
     * @return void
     */
    function handlePost()
    {
        if (isset($_POST["action"]) && "backup" == $_POST["action"]){
            DatabaseManager::doBackup();
            echo "Backup done.";
        }
        
        if (isset($_POST["action"]) && "restore" == $_POST["action"]){
            DatabaseManager::restoreBackup();
            echo "Backup restored.";
        }
        
    }
    
?>