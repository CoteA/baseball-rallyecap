<?php
/**********************************************************
Fichier : CtrlCasquettes.php
Auteur : Christophe Leclerc
Date : 2019-04-24
Fonctionnalité :
===========================================================
Vérification :
Date        Nom
2019-05-05  Maïka Forestal Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-05-01  Christophe Leclerc  Ajout de reload page
2019-05-01  Christophe Leclerc  Modification de toCasquetteComboBox
 **********************************************************/

ini_set('display_errors', 1);
error_reporting(E_ALL);

//Nous permets de défénir le chemin à utiliser
if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
    require_once $root . "PHP/CLASS/Casquette.php";
    require_once $root . "PHP/gestionnaire/GestionCasquette.php";
    require_once $root . "PHP/gestionnaire/GestionEpreuve.php";
    require_once $root . "PHP/CLASS/epreuve.php";
} else {
    require_once ROOT_DIR . "PHP/utils/DatabaseManager.php";
    require_once ROOT_DIR . "PHP/CLASS/Casquette.php";
    require_once ROOT_DIR . "PHP/gestionnaire/GestionCasquette.php";
    require_once ROOT_DIR . "PHP/gestionnaire/GestionEpreuve.php";
    require_once ROOT_DIR . "PHP/CLASS/epreuve.php";
}



/**
 * permet de défénir l'action a exécuté.
 */
if (isset($_POST['reloadPage'])) {
    reloadPage();
} elseif (isset($_POST['loadEpreuveForCasquette'])) {
    loadEpreuveForCasquette();
} elseif (isset($_POST['loadEpreuveNull'])) {
    loadEpreuveNull();
} elseif (isset($_POST['addEpreuveToCasquette'])) {
    addEpreuveToCasquette();
} elseif (isset($_POST['removeEpreuveFromCasquette'])) {
    removeEpreuveFromCasquette();
} elseif (isset($_POST['loadComboBoxCasquette'])) {
    loadComboBoxCasquette();
} elseif (isset($_POST['modifier'])) {
    // edit-button was clicked
    modifier();
} elseif (isset($_POST['supprimer'])) {                 // delete-button was clicked    
    supprimer();
} elseif (isset($_POST['ajoutBD'])) {                   // add-button was clicked   
    ajouter();
}


function loadComboBoxCasquette()
{
    $array = array();
    $query = "SELECT * FROM casquette WHERE deleted = 'false'";
    $result = DatabaseManager::getResultSet($query);
    while ($row = mysqli_fetch_array($result)) {
        $array[] = array('id' => $row[0], 'nom' => $row[1]);
    }
    echo json_encode($array);
}

/** Permet de faire un combo list de tous les casquettes dans la base de données
 * @return void
 */
function toCasquetteComboBox()
{
    $send = '';
    $query = "SELECT * FROM casquette WHERE deleted = 'false'";
    $result = DatabaseManager::getResultSet($query);
    while ($row = mysqli_fetch_array($result)) {
        $send .= '<option value=" ' . $row[0] . '"> ' . $row[1] . '</option>';
    }
    echo $send;
}


/** Permet d'ajouter une casquette dans la base de données.
 * @return void
 */
function ajouter()
{
    $gest = new GestionCasquette();
    $casquette = new Casquette(0, $_POST['casquette'], null);
    $gest->createInDB($casquette);
}

/**
 * modifier
 * @return void
 */
function modifier()
{
    $gest = new GestionCasquette();
    $casquette = new Casquette($_POST['casquette'], $_POST['newValue'], null);
    $gest->updateInDB($casquette);
}

/** Permet de supprimer une casquette de la base de données.
 * @return void
 */
function supprimer()
{
    $gest = new GestionCasquette();
    $gest->deleteInDB($_POST['casquette']);
}

/** Reload la page
 * @return void
 */
function reloadPage()
{
    header('Location: ../../GUICasquettes.php');
}

/**
 * loadEpreuveForCasquette
 *
 * @return void
 */
function loadEpreuveForCasquette()
{
    $gest = new GestionEpreuve();
    $epreuves = $gest->getEpreuveForCasquette($_POST['casquette']);

    $result = '';

    foreach ($epreuves as $epreuve) {
        $result .= '<tr onclick="selectedLigne(this.id, \'epreuveCasquettes\')" id="' . $epreuve->getId() . '"><td>' . $epreuve->getNom() . '</td><td>' . $epreuve->getDescription() . '</td></tr>';
    }
    echo $result;
}

/**
 * loadEpreuveNull
 *
 * @return void
 */
function loadEpreuveNull()
{
    $gest = new GestionEpreuve();
    $epreuves = $gest->getEpreuveWithNoCasquette();

    $result = '';

    foreach ($epreuves as $epreuve) {
        $result .= '<tr onclick="selectedLigne(this.id, \'epreuveDispos\')" id="' . $epreuve->getId() . '" class="tabRow"><td>' . $epreuve->getNom() . '</td><td>' . $epreuve->getDescription() . '</td></tr>';
    }
    echo $result;
}

/**
 * permet d'ajouter une épreuve à une casquette
 */
function addEpreuveToCasquette()
{
    $gest = new GestionEpreuve();
    $gest->setCasquette($_POST['id'], $_POST['casquette']);
}

function removeEpreuveFromCasquette()
{
    $gest = new GestionEpreuve();
    $gest->setCasquetteNull($_POST['id']);
}
