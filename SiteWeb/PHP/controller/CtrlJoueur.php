<?php

/**********************************************************
Fichier :           CtrlJoueur.php
Auteur  :           Massinisa Talah
Date    :           17 avril 2019
Fonctionnalité :    FW-02 / Gestion des Joueurs
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
//Nous permets de définir le chemin à utiliser
if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
    require_once $root . "PHP/utils/TableUtil.php";
    require_once $root . "PHP/CLASS/Joueur.php";
    require_once $root . "PHP/CLASS/Tuteur.php";
    require_once $root . "PHP/gestionnaire/GestionJoueur.php";
    require_once $root . "PHP/gestionnaire/GestionTuteur.php";
} else {
    require_once ROOT_DIR . "PHP/utils/DatabaseManager.php";
    require_once ROOT_DIR . "PHP/utils/TableUtil.php";
    require_once ROOT_DIR . "PHP/CLASS/Joueur.php";
    require_once ROOT_DIR . "PHP/CLASS/Tuteur.php";
    require_once ROOT_DIR . "PHP/gestionnaire/GestionJoueur.php";
    require_once ROOT_DIR . "PHP/gestionnaire/GestionTuteur.php";
}

// Gestion du POST
if (isset($_POST['ajoutJoueur'])){
    ajouter();
  //  header('Location: ../../GUIJoueurs.php');
}
else if (isset($_POST['suppJoueur'])) {
    supprimer();
  //  header('Location: ../../GUIJoueurs.php');
}
else if (isset($_POST['modifJoueur'])){
    modifier();
  //  header('Location: ../../GUIJoueurs.php');
}


/** Ajoute un joueur a partir du post avec son tuteur
 * @return void
 */
function ajouter()
{
    $gj=new GestionJoueur();
    $gt=new GestionTuteur();
    $gt->ajouterTuteur($_POST);
    $gj->ajouter($_POST);
}

/** Modifi un joueur et tuteur a partir du POST
 * @return void
 */
function modifier()
{
    $gj=new GestionJoueur();
    $gt=new GestionTuteur();
    $gt->modifierTuteur($_POST);
    $gj->modifier($_POST);
}

/** Supprime un joueur a partir du post
 * @return void
 */
function supprimer()
{
    $gj=new GestionJoueur();
    $gj->supprimer($_POST);

}

/** Affiche les joueurs dans un tableau
 * @return void
 */
function afficher()
{
    $sql = "SELECT j.prenom,j.nom, j.date_naissance,c.nom_couleur,j.est_actif,t.prenom AS prenomTuteur,t.nom AS nomTuteur,t.telephone AS telTuteur,t.courriel AS courrielTuteur
            FROM Joueur AS j
            INNER JOIN Casquette AS c ON c.id_casquette=j.id_casquette
            INNER JOIN Tuteur AS t ON j.id_tuteur=t.id_tuteur";

    $header  = "<table  class='table'  id='listJoueur'>
                <thead>
                    <th><b>Prénom</b></th>
                    <th><b>Nom</b></th>
                    <th><b>Date de naissance</b></th>
                    <th><b>Casquette</b></th>
                    <th><b>État</b></th>
                    <th><b>Prénom tuteur</b></th>
                    <th><b>Nom tuteur</b></th>
                    <th><b>Téléphone tuteur</b></th>
                    <th><b>Courriel tuteur</b></th>
                </thead>";
    $rs = DatabaseManager::getResultSet($sql);
    echo "<br>" . TableUtil::toRsTable($rs, $header);
}
?>
