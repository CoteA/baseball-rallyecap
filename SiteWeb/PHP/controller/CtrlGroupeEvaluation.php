<?php
/**********************************************************
Fichier :           CtrlGroupeEvaluation.php
Auteur  :           Massinisa Talah
Date    :           25 avril 2019
Fonctionnalité :    FW-06 / Gestion des groupe d'evaluation
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once '../../config.php';
include ROOT_DIR . 'HTML/global_head.html';

require_once '../gestionnaire/GestionGroupeEvaluation.php';


composer();
echo "<div class='alert alert-success'>Opération réussie</div>";


/** Composition automatique des groupe d'evaluation
 * @return void
 */
function composer()
{
    $gestion = new GestionGroupeEvaluation();
    $nombreGoupes = $gestion->composer($_POST);

    afficher($gestion,$nombreGoupes);
}

/** Affiche les groupes d'evaluation
 * @param  GestionGroupeEvaluation $gestion
 * @param  int $nombreGoupes
 *
 * @return void
 */
function afficher($gestion,$nombreGoupes)
{
    //id du dérnier groupe ajouté
    $idGrp=$gestion->reccupererIdGroupe();
    for ($i=$nombreGoupes; $i >0 ; $i--) {

        $listejoueur=$gestion->getJoueurParGroupe($idGrp);

        echo "<div class='row'>
                <div class='col-12 col-md-10 ml-auto mr-auto'>";
        echo "<h2>Groupe : </h2>".$idGrp."<br />";
        echo "<h2>Bénévole : </h2>".$gestion->affcherBenevoleDuGroupe($idGrp)."<br />";

        echo "<table class='table groupe' border>
            <thead>
                <th>Nom</th><th>Prenom</th><th>Casquette</th>
            </thead>
            <tbody>";
        foreach ($listejoueur as $joueur) {
            echo "<tr>
            <td>" . $joueur->getNom() ."</td>
            <td>" . $joueur->getPrenom() ."</td>
            <td>" . $joueur->getIdCasquette() ."</td>
            </tr>";
        }
        echo " </tbody></table>";
        echo "</div>";
        echo "</div>";
        $idGrp--;
    }
}

?>
