<?php
/**********************************************************
Fichier : GestionTypeEpreuve.php
Auteur : Anthony Cote
Date : Inconnu
Fonctionnalité : Permert de controller l'affichage dans l'interface.
===========================================================
Vérification :
Date        Nom
2019-04-03  Christophe Leclerc  Approuvé  
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-03  Christophe Leclerc  Modification pour recevoir les post de javascript (ajout de ajouter, modifier, supprimer)
                                Modification des fonctions toTypeEpreuveComboBox, loadTableEpreuve
 **********************************************************/
ini_set('display_errors', 1);
error_reporting(E_ALL);

//Nous permets de définir le chemin à utiliser
if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/utils/DatabaseManager.php";
    require_once $root . "PHP/utils/TableUtil.php";
    require_once $root . "PHP/CLASS/TypeEpreuve.php";
    require_once $root . "PHP/CLASS/Epreuve.php";
    require_once $root . "PHP/gestionnaire/GestionEpreuve.php";
    require_once $root . "PHP/gestionnaire/GestionTypeEpreuve.php";
} else {
    require_once ROOT_DIR . "PHP/utils/DatabaseManager.php";
    require_once ROOT_DIR . "PHP/utils/TableUtil.php";
    require_once ROOT_DIR . "PHP/CLASS/TypeEpreuve.php";
    require_once ROOT_DIR . "PHP/CLASS/Epreuve.php";
    require_once ROOT_DIR . "PHP/gestionnaire/GestionEpreuve.php";
    require_once ROOT_DIR . "PHP/gestionnaire/GestionTypeEpreuve.php";
}


if (isset($_POST['ajoutTypeEpreuve']))  {   CtrlEpreuve::ajoutTypeEpreuve();}
elseif (isset($_POST['rechEpreuve']))   {   CtrlEpreuve::rechEpreuve();}
elseif (isset($_POST['ajoutEpreuve']))  {   CtrlEpreuve::ajouter();}
elseif (isset($_POST['modifEpreuve']))  {   CtrlEpreuve::modifier();}
elseif (isset($_POST['suppEpreuve']))   {   CtrlEpreuve::supprimer();}
elseif (isset($_POST['getById']))       {   CtrlEpreuve::getById();}

    
/** Fait le liens entre les donnees dans l'interface et le gestionnaire */
class CtrlEpreuve
{

    public static function rechEpreuve()
    { }

    /** Ajoute une epreuve dans la DB
     * @return void
     */
    public static function ajoutTypeEpreuve()
    {
        $te = new TypeEpreuve(0, $_POST["valueToAdd"], null);
        GestionTypeEpreuve::createInDB($te);
    }

    /** Recupere les Type d'Epreuve de la DB
     * @return void
     */
    public static function loadTypeEpreuve()
    {
        $rs = getResultSet("SELECT * FROM TypeEpreuve");
        $arr[] = array();
        
        // Convertion de chaque Epreuve
        while ($row = mysql_fetch_assoc($rs)) {
            $arr[] = EpreuveFromMap($row);
        }

        return $arr;
    }

    /** Recupere le id d'un type d'epreuve a partir de son nom
     * @param  mixed $nom
     * @return void
     */
    public static function idTypeEpreuveFromNom($nom)
    {
        // TODO Prevenir l'injection SQL : Low-risk (source: comboBox)
        $sql = "SELECT id_type_epreuve FROM TypeEpreuve WHERE nom='$nom';";
        $rs = DatabaseManager::getResultSet($sql);
        $row = mysqli_fetch_assoc($rs);
        return $row["id_type_epreuve"];
    }

    /** Recupere le id d'une epreuve a partir de son nom
     * @param  mixed $nom
     * @return void
     */
    public static function idCasquetteFromNom($nom)
    {
        // TODO Prevenir l'injection SQL : Low-risk (source: comboBox)
        $sql = "SELECT id_casquette FROM Casquette WHERE nom_couleur='$nom';";
        $rs = DatabaseManager::getResultSet($sql);
        $row = mysqli_fetch_assoc($rs);
        return $row["id_casquette"];
    }

    /** Permet de loader la combobox du type d'épreuve
    * @return void
     */
    public static function toTypeEpreuveComboBox()
    {
        $send = '';
        $query = "SELECT * FROM typeepreuve";
        $result = DatabaseManager::getResultSet($query);
        $send .= '<select name="TypeEpreuve" id="TypeEpreuve">
        <option disabled selected value> -- Sélectionner -- </option>';
        while ($row = mysqli_fetch_array($result)) {
            $send .= '<option value=" ' . $row[0] . '"> ' . $row[1] . '</option>';
        }
        $send .= '</select>';
        echo $send;
    }

    /** Permet de loader la table avec son contenu
     * @return void
     */
    function loadTableEpreuve()
    {
        $gest = new GestionEpreuve();
        $listEpreuves = $gest->getAll();
        foreach ($listEpreuves as $epreuve) {
            echo "<tr onclick=\"selectedLigne(this.id, 'epreuve')\" id=\"". $epreuve->getId() ."\">
                <td>" . $epreuve->getNom() . "</td>
                <td>" . $epreuve->getDescription() . "</td>
                <td>" . $epreuve->getNbEssaiMin() . "</td>
                <td>" . $epreuve->getNbEssaiTotal() . "</td>           
            </tr>";
        }
    }

    /** Ajoute une epreuve dans la DB a partir du POST
     * @return void
     */
    public static function ajouter()
    {
        $epreuve = new Epreuve(0, $_POST['nom'], $_POST['description'], $_POST['nbEssaiTotal'], $_POST['nbEssaiMin'], null, $_POST['idTypeEpreuve']);
        GestionEpreuve::createInDB($epreuve);
    }

    /** Met a jour une epreuve a partir ddes donnes du post
     * @return void
     */
    public static function modifier()
    {
        $epreuve = new Epreuve($_POST['id'], $_POST['nom'], $_POST['description'], $_POST['nbEssaiMin'], $_POST['nbEssaiTotal'], null, $_POST['idTypeEpreuve']);
        GestionEpreuve::updateInDB($epreuve);
    }

    /** Supprime une epreuve a partir du id dans le post
     * @return void
     */
    public static function supprimer()
    {
        $e = new Epreuve($_POST['id'],null,null,null,null,null,null);
        GestionEpreuve::deleteInDB($e);
    }

    /** Recupere une epreuve a partir du id dans le post
     * @return void
     */
    public static function getById() {
        $gest = new GestionEpreuve();
        $rs = $gest->getById($_POST['id']);
        $array = array();
        $array[] = array('id'=> $rs->getId() , 'nom'=> $rs->getNom(), 'description'=> $rs->getDescription(), 'nbMin'=> $rs->getNbEssaiMin(), 'nbMax' => $rs->getNbEssaiTotal(), 'idType' => $rs->getIdTypeEpreuve());
        echo json_encode($array);
    }
}
