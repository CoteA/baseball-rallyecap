<?php
/**********************************************************
Fichier :           ctrlBenevole.php
Auteur  :           Maïka Forestal
Date    :           17 avril 2019
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once '../utils/DatabaseManager.php';
require_once '../CLASS/Benevole.php';
require_once '../gestionnaire/GestionBenevole.php' ;;


/** Nettoyage de l'input utilisateur
 * @param  mixed $data
 * @return void
 */
function secure_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/**
 * Execute une action relié au bouton qui a été 
 * pressé par l'utilisateur.
 * (Rechercher, Ajouter, Modifier ou Supprimer un bénévole)
 */
function getButtonPressed()
{   
    $action = secure_input($_POST["action"]);
    if (isset($action)) {
        switch ($action) {
        case 'rechercher':
            rechercher();
            break;
        case 'ajouter':
            ajouter();
            break;
        case 'modifier':
            modifier();
            break;
        case 'supprimer':
            supprimer();
            break;
    }
    }
    else{
        echo "Erreur: Aucun bouton n'a été sélectionner, ou l'action ne s'est pas set.";
    }
}

/**
 * Permet de faire une recherche de bénévole existant
 * @return void
 */
function rechercher()
{
    exit;
}

/**
 * Ajoute un bénévole dans la base de donnée.
 * @return void
 */
function ajouter()
{

    $benevole = benevoleFromMap($_POST);
    $gestion = new gestBenevole($benevole);
    //if(!existe($benevole))
    $gestion->ajouter();
    
    header('Location: ../../GUIBenevoles.php');
}

/**
 * Modifie le bénévole qui a été sélectionner dans le tableau par
 * l'utilisateur.
 * @return void
 */
function modifier()
{
    $benevole = benevoleFromMap($_POST);
    $gestion = new gestBenevole($benevole);
    $gestion->trouverID($benevole);
    $gestion->modifier();

    //header('Location: ../../GUIBenevoles.php');
    exit;
}

/**
 * Supprime le Bénévole qui a été sélectionner
 * dans le tableau, par l'utilisateur.
 * @return void
 */
function supprimer()
{
    $benevole = benevoleFromMap($_POST);
    $gestion = new gestBenevole($benevole);
    $gestion->trouverBenevole($benevole);
    $gestion->supprimer();
    header('Location: ../../GUIBenevoles.php');
}

getButtonPressed();
?>