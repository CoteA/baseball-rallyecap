<?php
/**********************************************************
Fichier :           CtrlEvenementEvaluation.php
Auteur  :           Massinisa Talah
Date    :           22 avril 2019
Fonctionnalité :    FW-06 / Gestion des groupe d'evaluation
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/
require_once '../gestionnaire/GestionEvenementEvaluation.php';
require_once '../utils/DatabaseManager.php';

// Gere l'ajout selon le POST
if (!empty($_POST['adresse']) && !empty($_POST['annee'])) {
    ajouter();
    // Reussite
    echo "<div class='alert alert-success'>L'ajout s'est bien effectué</div>";
}
else {
    // Echec
    echo "<div class='alert alert-danger'>Veuillez compléter tous les champs</div>";
}


/** Ajoute un Evenement d'evaluation dans la DB
 * @return void
 */
function ajouter()
{
    $gestion = new GestionEvenementEvaluation();
    $gestion->ajouter($_POST);
    //header('Location: ../../GUIEvenement.php');
}

?>
