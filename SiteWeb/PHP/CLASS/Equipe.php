<?php
/**********************************************************
Fichier : Equipe.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom
2019-04-23  Chrisotphe Leclerc  APPROUVÉ
2019-05-05  Maïka Forestal      Faire en sorte que la classe ne soit plus publique, sinon
                                Domingo il sera pas content et on va perdre plein de points
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Equipe de competition de baseball */
class Equipe 
{
        public $id = -1;
        public $nom = "";
        
        
        /** Get the value of id  */  
        public function getId() {  return $this->id; } 
        /** Get the value of nom  */  
        public function getNom() {  return $this->nom; } 
        
        
        /** Set the value of id  *  * 
         * @return  self  */  
        public function setId($id) {  $this->id = $id;  return $this; }   
        /** Set the value of nom  *  * 
         * @return  self  */  
        public function setNom($nom) {  $this->nom = $nom;  return $this; }
        
        public function __construct($id = -1, $nom = "_"){
                $this->id = $id;
                $this->nom = $nom;
        }
        
        
}
?>
