<?php
/**********************************************************
Fichier : Joueur.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date    Nom         Approuvé
2019-05-05  Maïka Forestal      Ok
2019-04-15
===========================================================
Historique de modifications :
Date    Nom         Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Information sur un joueurs de baseball d'age mineur */
class Joueur
{
    private $id;
    private $prenom;
    private $nom;
    private $dateNaissance;
    private $idCasquette;
    private $idEtat;

    public function __construct($id = 0, $prenom = null, $nom = null,
            $dateNaissance = null, $idCasquette = null, $idEtat=false){
        $this->setId($id);
        $this->setPrenom($prenom);
        $this->setNom($nom);
        $this->setDateNaissance($dateNaissance);
        $this->setIdCasquette($idCasquette);
        $this->setIdEtat($idEtat);
    }

    /** Get the value of id  */
    public function getId()
    {  return $this->id; }
    /** Get the value of prenom  */
    public function getPrenom()
    {  return $this->prenom; }
    /** Get the value of nom  */
    public function getNom()
    {  return $this->nom; }
    /** Get the value of dateNaissance  */
    public function getDateNaissance()
    {  return $this->dateNaissance; }
    /** Get the value of idCasquette  */
    public function getIdCasquette()
    {  return $this->idCasquette; }
    /** Get the value of idEtat  */
    public function getIdEtat()
    {  return $this->idEtat; }


    /** Set the value of id  *  *
     @return  self  */
    public function setId($id) {  $this->id = $id;  return $this; }
    /** Set the value of prenom  *  *
     @return  self  */
    public function setPrenom($prenom) {  $this->prenom = $prenom;  return $this; }
    /** Set the value of nom  *  *
     * @return  self  */
    public function setNom($nom) {  $this->nom = $nom;  return $this; }
    /** Set the value of dateNaissance  *  *
     @return  self  */
    public function setDateNaissance($dateNaissance) {  $this->dateNaissance = $dateNaissance;  return $this; }
    /** Set the value of idCasquette  *  *
     @return  self  */
    public function setIdCasquette($idCasquette) {  $this->idCasquette = $idCasquette;  return $this; }
    /** Set the value of idEtat  *  *
     @return  self  */
    public function setIdEtat($idEtat) {  $this->idEtat = $idEtat;  return $this; }

}
