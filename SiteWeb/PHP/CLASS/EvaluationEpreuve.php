<?php
/**********************************************************
Fichier : EvaluationEpreuve.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal      Faire en sorte que la classe ne soit plus publique, sinon
                                Domingo il sera pas content et on va perdre plein de points
2019-04-15  
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Evaluation d'un joueur a une epreuve avec les resultats */
class EvaluationEpreuve 
{
        public $id = -1;
        public $description = "";
        public $resultatNumerique = -1;
        public $date = "0000-00-00";
        public $reussi = false;
        public $idEpreuve = -1;

        /** Get the value of id  */  
        public function getId() {  return $this->id; } 
        /** Get the value of description  */  
        public function getDescription() {  return $this->description; } 
        /** Get the value of resultat_numerique  */  
        public function getResultatNumerique() {  return $this->resultatNumerique; } 
        /** Get the value of date  */  
        public function getDate() {  return $this->date; } 
        /** Get the value of reussi  */  
        public function getReussi() {  return $this->reussi; } 
        /** Get the value of id_epreuve  */  
        public function getIdEpreuve() {  return $this->idEpreuve; } 


        /** Set the value of id  *  * 
         * @return  self  */ 
        public function setId($id) {  $this->id = $id;  return $this; } 
        /** Set the value of description  *  * 
         * @return  self  */  
        public function setDescription($description) {  $this->description = $description;  return $this; } 
        /** Set the value of date
         * @return  self  */  
        public function setDate($date) {  $this->date = $date;  return $this; } 
        /** Set the value of reussi
         * @return  self  */  
        public function setReussi($reussi) {  $this->reussi = $reussi;  return $this; } 
        /** Set the value of resultat_numerique  *  * 
         * @return  self  */  
        public function setResultat_numerique($resultatNumerique) {  $this->resultatNumerique = $resultatNumerique;  return $this; } 
        /** Set the value of id_epreuve  *  * 
         * @return  self  */  
        public function setIdEpreuve($idEpreuve) {  $this->idEpreuve = $idEpreuve;  return $this; }


        public function __construct($id = -1, $description = "_", $date = "_",
                        $reussi = false, $resultatNumerique = -1, $idEpreuve = -1){
                $this->id = $id;
                $this->description = $description;
                $this->date = $date;
                $this->reussi = $reussi;
                $this->resultatNumerique = $resultatNumerique;
                $this->idEpreuve = $idEpreuve;
        }
        
}
?>
