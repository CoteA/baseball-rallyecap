<?php
/**********************************************************
Fichier : EvenementEvaluation.php
Auteur : Talah Massinissa
Date : 2019-04-22
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal    Ok
2019-04-15
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Information sur une journee d'evaluation du niveau de competences de joueurs */
class EvenementEvaluation
{
      private $id;
      private $adresse;
      private $date;
      private $heure;

      public function __construct($id =0, $adresse = null, $date = null, $heure = null)
      {
            $this->setId($id);
            $this->setAdresse($adresse);
            $this->setDate($date);
            $this->setHeure($heure);
      }

      /** Get the value of id  */
      public function getId() {  return $this->id; }
      /** Get the value of adresse  */
      public function getAdresse() {  return $this->adresse; }
      /** Get the value of date  */
      public function getDate() {  return $this->date; }
      /** Get the value of heure  */
      public function getHeure() {  return $this->heure; }


      /** Set the value of id  *  *
       @return  self  */
      public function setId($id) {  $this->id = $id;  return $this; }
      /** Set the value of adresse  *  *
       * @return  self  */
      public function setAdresse($adresse) {  $this->adresse = $adresse;  return $this; }
      /** Set the value of date  *  *
       * @return  self  */
      public function setDate($date) {  $this->date = $date;  return $this; }
      /** Set the value of heure  *  *
       * @return  self  */
      public function setHeure($heure) {  $this->heure = $heure;  return $this; }
}
?>
