<?php
/**********************************************************
Fichier : EvaluationCasquette.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal      Faire en sorte que la classe ne soit plus publique, sinon
                                Domingo il sera pas content et on va perdre plein de points
2019-04-15  
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Evaluation d'un joueur pour une casquette avec ses resultats */
class EvaluationCasquette 
{
        public $id = -1;
        public $description = "";
        public $date = "0000-00-00";
        public $reussi = false;
        public $resultatNumerique = -1;
        

        /** Get the value of id  */  
        public function getId() {  return $this->id; } 
        /** Get the value of description  */  
        public function getDescription() {  return $this->description; } 
        /** Get the value of date  */  
        public function getDate() {  return $this->date; } 
        /** Get the value of reussi  */  
        public function getReussi() {  return $this->reussi; } 
        /** Get the value of resultatNumerique  */  
        public function getResultatNumerique() {  return $this->resultatNumerique; } 
        
        /** Set the value of id  *  * 
         * @return  self  */  
        public function setId($id) {  $this->id = $id;  return $this; } 
        /** Set the value of description  *  * 
         * @return  self  */  
        public function setDescription($description) {  $this->description = $description;  return $this; } 
        /** Set the value of date  *  * @return  self  */  
        public function setDate($date) {  $this->date = $date;  return $this; } 
        /** Set the value of reussi  *  * 
        @return  self  */  
        public function setReussi($reussi) {  $this->reussi = $reussi;  return $this; } 
        /** Set the value of resultatNumerique  *  * 
         * @return  self  */  
        public function setResultatNumerique($resultatNumerique) {  $this->resultatNumerique = $resultatNumerique;  return $this; }    

        public function __construct($id = -1, $description = "_", $date = "_",
                        $reussi = false, $resultatNumerique = -1){
                $this->id = $id;
                $this->description = $description;
                $this->date = $date;
                $this->reussi = $reussi;
                $this->resultatNumerique = $resultatNumerique;
        }
        
}
