<?php
/**********************************************************
Fichier : Epreuve.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom
2019-04-23  Christophe Leclerc APPROUVÉ
2019-05-05  Maïka Forestal      Faire en sorte que la classe ne soit plus publique, sinon
                                Domingo il sera pas content et on va perdre plein de points
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Information sur une epreuve d'un point de vue theorique */
class Epreuve 
{
    public $id = -1;
    public $nom = "";
    public $description = "";
    public $nbEssaiTotal = -1;
    public $nbEssaiMin = -1;
    public $casquette = -1;
    public $idCasquette = -1;
    public $idTypeEpreuve = -1;

    
    /** Get the value of id  */  
    public function getId() {  return $this->id; }  
    /** Get the value of nom  */  
    public function getNom() {  return $this->nom; } 
    /** Get the value of description  */  
    public function getDescription() {  return $this->description; } 
    /** Get the value of nbEssaieTotal  */  
    public function getNbEssaiTotal() {  return $this->nbEssaiTotal; } 
    /** Get the value of nbEssaieMin  */  
    public function getNbEssaiMin() {  return $this->nbEssaiMin; } 
    /** Get the value of casquette  */  
    public function getIdCasquette() { return $this->casquette; } 
    /** Get the value of idTypeEpreuve  */  
    public function getIdTypeEpreuve() { return $this->idTypeEpreuve; } 
    
    
    /** Set the value of id
     * @return  self  */  
    public function setId($id) {  $this->id = $id;  return $this; } 
    /** Set the value of nom
     * @return  self  */  
    public function setNom($nom) {  $this->nom = $nom;  return $this; } 
    /** Set the value of description
     * @return  self  */  
    public function setDescription($description) { $this->description = $description;  return $this; } 
    /** Set the value of nbEssaieTotal
     * @return  self  */  
    public function setNbEssaiTotal($nbEssaiTotal) { $this->nbEssaiTotal = $nbEssaiTotal; return $this; } 
    /** Set the value of nbEssaieMin
     * @return  self  */  
    public function setNbEssaiMin($nbEssaiMin) {  $this->nbEssaiMin = $nbEssaiMin;  return $this; } 
    /** Set the value of idCasquette
     * @return  self  */  
    public function setIdCasquette($casquette) {  $this->casquette = $casquette;  return $this; }
    /** Set the value of idTypeEpreuve
     * @return  self  */  
    public function setIdTypeEpreuve($idTypeEpreuve) {  $this->idTypeEpreuve = $idTypeEpreuve;  return $this; }

    public function __construct($id = -1, $nom = "_", $description = "_",
                    $nbEssaiMin = -1, $nbEssaiTotal = -1, $idCasquette = -1, $idTypeEpreuve = -1){
        $this->id = $id;
        $this->nom = $nom;
        $this->description = $description;
        $this->nbEssaiMin = $nbEssaiMin;
        $this->nbEssaiTotal = $nbEssaiTotal;
        $this->idCasquette = $idCasquette;
        $this->idTypeEpreuve = $idTypeEpreuve;
    }
}
?>
