<?php
/**********************************************************
Fichier : Casquette.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom
2019-05-05  Maïka Forestal      Faire en sorte que les variable ne soit plus publique (fait)
2019-04-23  Christophe Leclerc APPROUVÉ
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Niveau de competences de joueurs suivant l'evaluation d'epreuve */
class Casquette 
{
        private $id = -1;
        private $nom = "";
        private $description = "";



        /** Get the value of id */         
        public function getId() { return $this->id; }        
        /** Get the value of nom */         
        public function getNom() { return $this->nom; }        
        /** Get the value of description */         
        public function getDescription() { return $this->description; }       


        /** Set the value of id
         * @return  self */         
        public function setId($id) { $this->id = $id; return $this; }        
        /** Set the value of nom
         * @return  self */         
        public function setNom($nom) { $this->nom = $nom; return $this; }        
        /** Set the value of description
         * @return  self */         
        public function setDescription($description) { $this->description = $description; return $this; }    

        public function __construct($id = -1, $nom = "_", $description = "_"){
                $this->id = $id;
                $this->nom = $nom;
                $this->description = $description;
        }
        
        
} 


?>
