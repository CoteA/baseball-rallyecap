<?php
/**********************************************************
Fichier :           Benevole.php
Auteur  :           Anthony Cote
Date    :           2019-04-13
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom
2019-04-23  Christophe Leclerc  APPROUVÉ
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
Description:        - Changer la propriété des attribut pour private
                    - Ajout d'une méthode pour initialiser
                    - Ajout des attributs; actif et typeBenevole,
                      ainsi que de leur getteurs/setteurs
                    - Suppression des méthodes faisant le lien vers la base
                      base de donnée. Ils sont désormais dans le fichier:
                      gestBenevole.php

Date : 18 avril 2019
Modifié par : Maïka Forestal
**********************************************************/
ini_set('display_errors', 1);
error_reporting(E_ALL);

/**
 * La classe bénévole rassemble l'ensemble des attributs, ainsi que
 * leur getteurs et leur setteur respectifs, nécessaire à la création
 * d'un bénévole.
 * L'instanciation d'un bénévole est majoritairement utiliser
 * pour garder en mémoire les informations que l'on souhaite ajouter,
 * modifier ou supprimer dans la base de donnée.
 */
class Benevole
{
    private $id;                // Identifiant du bénévole.             (default: 0) 
    private $prenom;            // Prénom du bénévole.                  (default: NULL)
    private $nom;               // Nom du bénévole.                     (default: NULL)
    private $telephone;         // Numéro de téléphone du bénévole.     (default: NULL)
    private $courriel;          // Adresse courriel du bénévole.        (default: NULL)

    private $username;          // Nom d'utilisateur du bénévole.       (default: NULL)
    private $motDePasse;        // Mot de passe du bénévole.            (default: NULL)
    private $actif;             // Si le compte du bénévole est activé. (default: TRUE)
    private $typeBenevole;      // Type de bénévole.                    (default: 2)

    /**
     * Constructeur de Bénévole
     * @param int       $id             (default: 0) 
     * @param string    $prenom         (default: NULL)
     * @param string    $nom            (default: NULL)
     * @param string    $telephone      (default: NULL)
     * @param string    $courriel       (default: NULL)
     * @param string    $userName       (default: NULL)
     * @param string    $motDePasse     (default: NULL)
     * @param bool      $actif          (default: TRUE)
     * @param int       $type           (default: 2)
     */
    public function __construct($id = 0, $prenom = NULL, $nom = NULL, $telephone = NULL, $courriel = NULL, $username = NULL, $motDePasse = NULL, $actif = TRUE, $type = 2) 
    {
        $this->init($id, $prenom, $nom, $telephone, $courriel,$username, $motDePasse, $actif, $type);
    }

    /**
     * Initialise un bénévole
     * @param int       $id
     * @param string    $prenom
     * @param string    $nom
     * @param string    $telephone
     * @param string    $courriel
     * @param string    $userName
     * @param string    $motDePasse
     * @param bool      $actif
     * @param int       $type
     */
    private function init($id,$prenom, $nom, $telephone, $courriel,$username, $motDePasse, $actif, $type)
    {
        $this->setId($id);
        $this->setInfoPersonnel($prenom, $nom, $telephone, $courriel);
        $this->setCompte($type, $username, $motDePasse, $actif);       
    }

    /**
     * Set les informations personnel du bénévole
     * @param string    $prenom
     * @param string    $nom
     * @param string    $telephone
     * @param string    $courriel
     */
    public function setInfoPersonnel($prenom, $nom, $telephone, $courriel){
        $this->setPrenom($prenom);
        $this->setNom($nom);
        $this->setTelephone($telephone);
        $this->setCourriel($courriel);
    }

    /**
     * Set les information du compte utilisateur du bénévole
     * @param int       $type
     * @param string    $userName
     * @param string    $motDePasse
     * @param bool      $actif
     */
    public function setCompte($type, $username, $motDePasse, $actif){      
        $this->setType($type);
        $this->setUsername($username);
        $this->setMotDePasse($motDePasse);
        $this->setActif($actif);
    }

    /**
     * Renvoie l'id du bénévole
     * @return int $this->id  
     * */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Renvoie le prénom du bénévole
     * @return string $this->prenom  
     * */
    public function getPrenom()
    {
        return $this->prenom;
    }
    
    /**
     * Renvoie le nom du bénévole
     * @return string $this->nom  
     * */
    public function getNom()
    {
        return $this->nom;
    }
    
    /**
     * Renvoie le numéro de téléphone du bénévole
     * @return string $this->telephone  
     * */
    public function getTelephone()
    {
        return $this->telephone;
    }
    
    /**
     * Renvoie le courriel du bénévole
     * @return string $this->courriel  
     * */
    public function getCourriel()
    {
        return $this->courriel;
    }
    
    /**
     * Renvoie le nom d'utilisateur du bénévole
     * @return string $this->username  
     * */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Renvoie le mot de passe du bénévole
     * @return string $this->motDePasse  
     * */
    public function getMotDePasse()
    {
        return $this->motDePasse;
    }
    
    /**
     * Renvoie vrai si le compte du bénévole est actif
     * @return bool $this->actif  
     * */
    public function getActif()
    {
        return $this->actif;
    }
    
    /**
     * Renvoie le type de bénévole du bénévole
     * @return string $this->typeBenevole  
     * */
    public function getTypeBenevole()
    {
        return $this->typeBenevole;
    }
        
        
    /**
     * Modifie l'id du bénévole
     * @param int $id 
     * */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Modifie le prénom du bénévole
     * @param string $prenom 
     * */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * Modifie le nom du bénévole
     * @param string $nom 
     * */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * Modifie le numéro de téléphone du bénévole
     * @param string $telephone 
     * */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Modifie l'adresse courriel du bénévole
     * @param string $courriel 
     * */
    public function setCourriel($courriel)
    {
        $this->courriel = $courriel;
    }

    /**
     * Modifie le nom d'utilisateur du bénévole
     * @param string $username 
     * */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Modifie le mot de passe du bénévole
     * @param string $motDePasse 
     * */
    public function setMotDePasse($motDePasse)
    {
        $this->motDePasse = $motDePasse;
    }

    /**
     * Modifie si le compte du bénévole est actif
     * @param bool $actif 
     **/
    public function setActif($actif){
        $this->actif = $actif;
    }

    /**
     * Modifie le type du bénévole
     * @param int $type 
     **/
    public function setType($type){
        $this->typeBenevole = $type;
    }
}