<?php
/**********************************************************
Fichier : GroupeEvaluation.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal      Ok
2019-04-15
===========================================================
Historique de modifications :
Date        Nom             Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/** Groupe de joueur qui participent a une evaluation donnee par un benevole*/
class GroupeEvaluation
{
        private $idGroupe;
        private $benevole;
        private $idEvent;


        public function __construct($id = 0, $benevole =null, $idEvent = null){
                $this->setIdGroupe($id);
                $this->setBenevole($benevole);
                $this->setIdEvent($idEvent);
        }

        /** Get the value of id  */
        public function getIdGroupe() {  return $this->idGroupe; }
        /** Get the value of benevole  */
        public function getBenevole() {  return $this->benevole; }
        /** Get the value of event  */
        public function getIdEvent() {  return $this->idEvent; }


        /** Set the value o f id  *  *
         * @return  self  */
        public function setIdGroupe($id) { $this->idGroupe = $id;  return $this; }
        /** Set the value of benevole  *  *
         * @return  self  */
        public function setBenevole($benevole) {  $this->benevole = $benevole;  return $this; }
        /** Set the value of event  *  *
         * @return  self  */
        public function setIdEvent($idEvent) { $this->idEvent = $idEvent;  return $this; }


}
