<?php
/**********************************************************
Fichier : Tuteur.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date    Nom                     Approuvé
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
Date    Nom         Description
**********************************************************/
ini_set('display_errors',1);
error_reporting(E_ALL);

/**  Information sur un parent d'un joueur */
class Tuteur
{
    private $id;
    private $prenom;
    private $nom;
    private $telephone;
    private $courriel;

    public function __construct($id = 0, $prenom = null, $nom = null,
            $telephone = null, $courriel = null){

        $this->setId($id);
        $this->setPrenom($prenom);
        $this->setNom($nom);
        $this->setTelephone($telephone);
        $this->setCourriel($courriel);
    }


    /**Get the value of id */
    public function getId() { return $this->id; }
    /**Get the value of prenom */
    public function getPrenom() { return $this->prenom; }
    /**Get the value of nom */
    public function getNom() { return $this->nom; }
    /**Get the value of telephone */
    public function getTelephone() { return $this->telephone; }
    /**Get the value of courriel */
    public function getCourriel() { return $this->courriel; }

    /**Set the value of id *
     * @return  self */
    public function setId($id) { $this->id = $id; return $this; }
    /**Set the value of prenom *
     * @return  self */
    public function setPrenom($prenom) { $this->prenom = $prenom; return $this; }
    /**Set the value of nom *
     * @return  self */
    public function setNom($nom) { $this->nom = $nom; return $this; }
    /**Set the value of telephone
     * @return  self */
    public function setTelephone($telephone) { $this->telephone = $telephone; return $this; }
    /**Set the value of courriel
     * @return  self */
    public function setCourriel($courriel) { $this->courriel = $courriel; return $this; }


}
?>
