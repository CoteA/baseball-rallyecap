<?php
/**********************************************************
Fichier : changePassword.php
Auteur  : Anthony Cote
Date    : 2019-04-16
Fonction: Gere le changement de mot de passe
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  TODO: Ajouter des commentaires
  
===========================================================
Historique de modifications :
Date        Nom             Description
2019-04-15  
**********************************************************/
    // TODO DOIT ETRE REPENSER CAR PRESENTEMENT NIMPORTE QUI PEUX RESETTER ET UTILISER UN COMPTE
    
    ini_set('display_errors',1);
    error_reporting(E_ALL);
    
    require_once("../PHP/utils/DatabaseManager.php");
    
    function isSamePassword()
    {
        return $_POST["nouvMdp"] == $_POST["confMdp"];
    }
    
    function isFieldsFilled()
    {
        return isset($_POST["user"])
            && isset($_POST["nouvMdp"])
            && isset($_POST["confMdp"]);
    }
    
    function isFieldsEnteredCorrectly()
    {
        return isFieldsFilled() && isSamePassword();
    }
    
    function sendPasswordRecoveryEmail($to)
    {
        $subject = "Mise a jour de mot de passe RallyeCap";
        $msg = "Votre mot de passe a ete change.";
        $headers = "From: webmaster@TODO.com" . "\r\n"; //TODO
        mail($to, $subject, $msg, $headers);
    }
    
    function changePassword($rs)
    {
        $row = mysql_fetch_assoc($rs);
        sendPasswordRecoveryEmail($row["courriel"]);
        echo "Un courriel vous a ete envoye"; // TODO
    }
    
    function dontChangePassword()
    {
        echo "L'utilisateur n'existe pas.";
    }
    
    function goBackToLogin()
    {
        header('Location: GUILogin.php');
        exit();
    }
    
    function getCourrielResultset()
    {
        $user = $_POST["user"];
        $sql = "SELECT courriel FROM Benevole WHERE username = $user;";
        return getResultSet($sql);
    }
    
    function tryToChangePassword()
    {
        $rs = getCourrielResultset();
        
        if (mysql_num_rows($rs) == 0){
            dontChangePassword()();
        }else{ changePassword($rs); }
    }
    
    
    
    if(isFieldsEnteredCorrectly()){
        tryToChangePassword();
    }else {
        goBackToLogin();
    }
?>