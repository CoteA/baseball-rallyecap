<?php
/**********************************************************
Fichier : TableUtil.php
Auteur  : Anthony Cote
Date    : 2019-04-13
Fonction: Cree des tables html a partir d'objets
===========================================================
Vérification :
Date        Nom
2019-04-29  Christophe Leclerc  Approuvé
2019-05-05  Maïka Forestal      Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-17  Anthony Cote        Ajout de la generation de tableau a partir d'un resultset
2019-04-29  Christophe Leclerc  Ajout de la condition pour le root dir
 **********************************************************/


if (isset($_POST['rootDir'])) {
    $root = $_POST['rootDir'];
    require_once $root . "PHP/CLASS/TypeBenevole.php";
    require_once $root . "PHP/CLASS/TypeEpreuve.php";
} else {
    require_once ROOT_DIR . "PHP/CLASS/TypeBenevole.php";
    require_once ROOT_DIR . "PHP/CLASS/TypeEpreuve.php";
}

/** Utilitaire de creation de table */
class TableUtil
{


    /** affiche les element d'une map dans une table
     * @param  mixed $map
     *
     * @return void
     */
    public static function show_map($map)
    {
        echo "<table border='1' style='width:200px'>";

        //* Header
        echo "<thead><tr bgcolor='#CCC'>";
        echo "<th><b>Key</b></th>";
        echo "<th><b>Value</b></th>";
        echo "</tr></thead>";

        //* Body
        echo "<tbody>";
        array_walk_recursive($map, function ($item, $key) {
            echo "<tr><td> $key </td><td> $item </td></tr>";
        });

        echo "</tbody>";
    }

    // RS Table *******************************************


    /**  Cree une table a partir d'un resultset
     * @param  mixed $rs
     * @param  mixed $header
     * @return void
     */
    public static function toRsTable($rs, $header = NULL)
    {
        if (NULL == $header) $header = toRsHeader($rs);

        $str = "";
        if (isset($rs) ) {
            $str .= $header;
            $str .= self::toRsTableBody($rs);
            $str .= self::toTableFooter();
        }
        return $str;
    }

    /** Cree une entete de table
     * @param  mixed $rs
     * @return void
     */
    public static function toRsHeader($rs)
    {
        $header  = "<table border='1'><thead><tr>";

        for ($i = 0; $i < mysqli_num_fields($rs); $i++) {

            $field_info = mysqli_fetch_field($rs);
            $header .= "<td><b>{$field_info->name}</b></td>";
        }

        $header .= "</tr></thead>";
        return $header;
    }


    /** Cree un corps de table a partir d'un array d'objet
     * @param  mixed $rs
     * @return void
     */
    public static function toRsTableBody($rs)
    {
        $str = "";
        foreach ($rs as $row) {
            $str .= self::toRsTableRow($row);
        }
        return $str;
    }

    // 
    /** Cree une ligne de table a partir d'un objet
     * @param  mixed $row
     * @return void
     */
    public static function toRsTableRow($row)
    {
        $str = "<tr onclick='selectLigne(this.id)'>";
        // Chaque propriete
        foreach ($row as $key => $value) {
            $str .= "<td>$value</td>";
        }

        return $str . "</tr>";
    }

    // ==========================================

    /** Cree une table a partir d'un array d'objet
     * toTable
     *
     * @param  mixed $array
     *
     * @return void
     */
    public static function toTable($array)
    {
        $str = "";
        if (isset($array) && count($array) > 0) {
            $str .= self::toTableHeader($array[0]);
            $str .= self::toTableBody($array);
            $str .= self::toTableFooter();
        }
        return $str;
    }


    /** Cree une entete de table a partir d'es noms de proprietes d'un objet
     * toTableHeader
     *
     * @param  mixed $obj
     *
     * @return void
     */
    public static function toTableHeader($obj)
    {
        // Class Name
        $name = get_class($obj);
        $str  = "<h3>$name</h3>";
        $str .= "<table border='1'  style='width:100%'>";

        // Properties
        $ref  = new ReflectionClass($obj);
        foreach ($ref->getProperties() as $prop) {
                $prop->setAccessible(true);
                $str .= "<td bgcolor='#CCC'><b>" . $prop->getName() . "</b></td>";
            }
        return $str;
    }

    
    /** Cree un corps de table a partir d'un array d'objet
     * toTableBody
     *
     * @param  mixed $array
     *
     * @return void
     */
    public static function toTableBody($array)
    {
        $str = "";
        foreach ($array as $elem) {
            $str .= self::toTableRow($elem);
        }
        return $str;
    }

    /** Cree une ligne de table a partir d'un objet
     * @param  mixed $obj
     *
     * @return void
     */
    public static function toTableRow($obj)
    {
        $str = "<tr>";
        // Chaque propriete
        $reflexion = new ReflectionObject($obj);
        foreach ($reflexion->getProperties() as $prop) {
                $prop->setAccessible(true);
                $str .= "<td>" . $prop->getValue($obj) . "</td>";
            }
        return $str . "</tr>";
    }

    /** Ferme une table
     * @return void
     */
    public static function toTableFooter()
    {
        return "</table>";
    }

    /** Cree un radio button
     * @param  mixed $group
     * @param  mixed $value
     * @param  mixed $text
     *
     * @return void
     */
    public static function toRadioBtn($group, $value, $text)
    {
        return "<input type=\"radio\" name=" . $group . " value=" . $value . "> $text";
    }

}
