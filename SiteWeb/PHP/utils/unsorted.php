<?php
/**********************************************************
Fichier : unsorted.php
Auteur  : Anthony Cote
Date    : 2019-04-13
Fonction: Code qui n'a pas ete trier dans une classe specifique
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok 
===========================================================
Historique de modifications :
Date        Nom             Description

**********************************************************/


/** Cree un array des noms de fichier dans le dossier de classe
 * @return array
 */
function arrayOfClassName()
{
    //Get a list of file paths using the glob function.
    $fileList = glob(ROOT_DIR. "PHP/CLASS/*.php");
    $arr = array();
    //Loop through the array that glob returned.
    foreach($fileList as $filename){
        $arr[] = basename($filename, ".php"); 
    }
    return $arr;
}

/** Cree une infoBulle
 *
 * @param  mixed $msg
 * @param  mixed $url
 */
function infoBulle($url = "")
{
    $imgPath = "images/tooltip.png";
    
    $start = "<span id='tooltip'>";
    
    if ("" == $url)     $lien = "<a>";
    else                $lien = "<a href='$url'>";
    
    $format = "<img src='$imgPath' alt='tooltip'>";
    $infobulle = "<span id=\"msgAide\"></span></a></span>";
    
    echo $start.$lien.$format.$infobulle;
}

?>