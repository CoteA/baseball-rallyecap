<?php
/**********************************************************
Fichier : DatabaseManager.php
Auteur  : Anthony Cote
Date    : 2019-04-13
Fonction: Gestion de la base de donnees
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description
2019-04-16   AnthonyCote    Utilise un fichier ini pour loader les proprietes des la db
2019-05-03   AnthonyCote    Ajout de fonction de backup
**********************************************************/

const DEFAULT_DB_PROPERTY_FILE =  "baseball_db.ini";

/** Gestionnaire de base de donnees */
class DatabaseManager
{
    // TODO : Changer le path par defaut du backup
    const BACKUP_LOCATION = "";
    const DEFAULT_BACKUP_FILENAME = "backup_bbdb.xml";
    
    private static $initialized = false;

    private static $host;
    private static $username;
    private static $password;
    private static $dbname;

    /** Initialise les donnees de connection a la DB
     * @return void
     */
    private static function initialize()
    {
        if (self::$initialized) return;

        self::loadProps();
        self::$initialized = true;
    }

    /** Load les proprietes de connection a la DB
     * @return void
     */
    private static function loadProps(){
        $props = parse_ini_file( DEFAULT_DB_PROPERTY_FILE, true);
        self::$host = $props["host"];
        self::$username = $props["username"];
        self::$password = $props["password"];
        self::$dbname = $props["dbname"];
    }

    /** Cree une nouvelle connaction a la DB
     * @return void
     */
    public static function newConnection()
    {
        self::initialize();
        $conn = new mysqli(self::$host, self::$username, self::$password, self::$dbname);
        if ($conn->connect_error) { exit("Connection failed: " . $conn->connect_error); }
        return $conn;
    }

    /** Execute le statement et ferme la connection
     * @param  mixed $stmt
     * @param  mixed $conn
     *
     * @return void
     */
    public static function persist($stmt, $conn)
    {
        $stmt->execute();
        $stmt->close();
        $conn->close();
    }

    /** Retourne un resultset a partir d'une query 
     *  TODO : Faire attention au injection SQL
     *  Utiliser uniquement avec des valeurs secures 
     * @param  mixed $sql
     *
     * @return void
     */
    public static function getResultSet($sql)
    {
        $conn = self::newConnection();
        $result = $conn->query($sql);
        $conn->close();
        return $result;
    }
    
    /** Retourne le dernier id d'une table
     * @param  mixed $table
     * @param  mixed $column
     *
     * @return void
     */
    public static function nextID($table, $column)
    {
        $sql = "SELECT MAX($column) AS 'dernierID' FROM $table";
        $rs = self::getResultSet($sql);
        $row = mysqli_fetch_assoc($rs);
        $id = $row['dernierID'] + 1;
        return $id;
    }
    
    /** Retourne l'id d'une row a l'endroit ou une condition est respectee
     *  TODO : On assume que la comparaison  retourne une seule valeur
     *  se qui fait que le design est mal fait au niveau des PK
     * @param  mixed $table
     * @param  mixed $colId
     * @param  mixed $colValue
     * @param  mixed $value
     *
     * @return void
     */
    public static function getIdWhere($table, $colId, $colValue, $value){
        
        // TODO : Communiquer le besoin de safe input
        $sql = "SELECT $colId FROM $table WHERE $colValue='$value';";
        $rs = DatabaseManager::getResultSet($sql);
        $row = mysqli_fetch_assoc($rs);
        return $row[$colId];
    }
    
    /** Sauvegarde un fichier XML qui contient un mysql dump de la DB
     * @param  mixed $path
     * @param  mixed $filename
     *
     * @return void
     */
    public static function doBackup($path = null, $filename = null){ 

        self::initialize();
        
        $file = self::normalizedBackupFile($path, $filename);
        
        $user = self::$username;
        $pass = self::$password;
        $command = "mysqldump --xml -u $user --password='$pass' baseball_db > $file";
        //echo $command;
        exec($command);
        
    }

    /** Sauvegarde un fichier XML qui contient un mysql dump de la DB
     * @param  mixed $path
     * @param  mixed $filename
     *
     * @return void
     */
    public static function restoreBackup($path = null, $filename = null){ 

        self::initialize();
        
        $file = self::normalizedBackupFile($path, $filename);
        
        $user = self::$username;
        $pass = self::$password;
        $command = "mysql --xml -u $user --password='$pass' baseball_db < $file";
        //echo $command;
        exec($command);
    }
    
    /** Retourne le nom normalized du fichier de backup
     * @param  mixed $path
     * @param  mixed $filename
     *
     * @return void
     */
    public static function normalizedBackupFile($path = null, $filename = null){
        
        if ($path = null)     $path = BACKUP_LOCATION;
        if ($filename = null) $filename = DEFAULT_BACKUP_FILENAME;
        
        //echo $path . $filename;
        return $path . $filename;
    }
}



?>
