﻿<?php session_start(); ?>
<!DOCTYPE html>
<!--****************************************
Fichier : GUILogin.php
Auteur  : Maika Forestal
Fonction: Page de login
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-01  Anthony Cote    Ok
2019-05-05  Maïka Forestal  Ok
=========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-13  Christophe Leclerc  Ajout d'animation
****************************************/-->
<html lang="fr-ca">

<head>
    <meta charset="utf-8" />
    <?php
    require_once("config.php");
    include "PHP/gestionnaire/GestionErreurs.php"
    ?>
    <title>Rallycap</title>
    <link rel="stylesheet" href="CSS/styleAuthentification.css" />
    <link rel="stylesheet" href="CSS/stylesLogin.css" />
</head>

<body>
    <header></header>

    <main id="contenu">
        <div-- class="rouge">
            <form id="login" action="LogIn.php" method='POST'>
                <h1>Baseball Sherbrooke</h1>

                <!-- Nom -->
                <label for="utilisateur">Nom d'utilisateur :</label>
                <input type="text" name="utilisateur" id="utilisateur"/>

                <!-- Mot de passe -->
                <label for="mdp">Mot de passe :</label>
                <input type="password" name="mdp" id="mdp" onkeyup="detecterMaj()" />
                <?php motDePasseInvalide()?>

                <!-- Mot de passe oublie -->
                <!-- <a id="recuperer" href="GUIRecupererMotPasse.php">Mot de passe oublié?</a> -->
                
                <!-- Bouton de connection -->
                <button type="submit">Se connecter</button>
                
            </form>

            <!-- Icone -->
            <img id="baseballIcone" src="images/baseball_logojpg.jpg" alt="Image d'une batte et d'une balle de baseball" />
            <img id="rallycap" src="images/rallyCap Canada.jpg" alt="Image du rallyecap" />
    </main>

    </div>
    <footer class="rouge"> </footer>
</body>
<script>
function detecterMaj() {
    var divErreur = document.getElementById('motDePasseInvalide');
    divErreur.innerText = "Attention ! La touche Verr. maj. est activé!";

    if (event.getModifierState("CapsLock")) {
        divErreur.hidden = false;
    } else {
        divErreur.hidden = true;
    }
}
</script>
</html>