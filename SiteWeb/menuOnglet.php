﻿<!--****************************************
Fichier : menuOnglet.php
Auteur  : 
Fonction: Sous-menu de gestion de donnes
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-05  Maïka Forestal  Ok    
=========================================================
Historique de modifications :
Date        Nom             Description
2019-05-01  Anthony Cote    Ajout de commentaires  
****************************************/-->
<div class="row col-12 col-xl-10 padding-0 spacer-top centrer"">
    <?php
        $tabMenuLien   = array("GUIBenevoles.php", "GUIJoueurs.php", "GUICasquettes.php", "GUIEpreuves.php");
        $tabMenuTexte  = array("Bénévoles", "Joueurs", "Casquettes", "Épreuves");
        
        $menuOnglet = "\n <div padding-0' id='menuOnglet'>\n <ul id='onglets'>\n";
        
        // Pour tous les liens
        foreach ($tabMenuLien as $cle => $lien) {
            // Affiche les liens
            $menuOnglet .= "<li";
            // Verifie si on est sur la page actuelle
            if (pathinfo($_SERVER['PHP_SELF'])['basename'] == $lien) {
                $menuOnglet .= " class='active'";
            }
            $menuOnglet .= "> <a href='$lien'> $tabMenuTexte [$cle]  </a> </li> \n";
        }
        $menuOnglet .= "</ul>\n</div>";
        echo $menuOnglet;
    ?>
</div>