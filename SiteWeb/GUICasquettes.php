<?php session_start(); ?>
<!DOCTYPE html>
<!--/****************************************
Fichier : GUICasquettes.php
Auteur  : Christophe Leclerc
Fonction: Affiche une interface à l'utilisateur afin de gérer les casquettes
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-05  Anthony Cote    TODO : div est dans un main ???
2019-05-05  Maïka Forestal  Ok
=========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-18  Anthony Cote        Refactoring et extraction de parties globales
2019-05-05  Christophe Leclerc  Formatage et style pour la remise finale.   
****************************************/-->
<html lang="fr-ca">

<head>
    <?php
    require_once 'config.php';
    require_once ROOT_DIR . 'CtrlLogin.php';
    //checkLogin();
    require_once ROOT_DIR . 'HTML/global_head.html';
    require_once ROOT_DIR . 'PHP/controller/CtrlCasquettes.php';
    ?>
    <!-- Style pour la page uniquement -->
    <link href="CSS/stylesCasquettes.css" type="text/css" rel="stylesheet" />
    <!-- Javascript pour la page uniquement -->
    <script src="JS/jsCasquettes.js"></script>
    <!--script src="JS/SwalUtil.js"></script-->
    <title>RallyCap</title>
</head>

<body onload="changerMenu()" onresize="changerMenu()">
    <div class="container-fluid contenuPage">
        <?php
        include ROOT_DIR . 'HTML/header.php';
        include ROOT_DIR . 'HTML/nav.php';
        ?>
        <!-- Menu Onglets -->
        <div class="row padding-0 spacer-top">
            <div class="col-0 col-xl-1"></div>
            <?php include ROOT_DIR . 'menuOnglet.php'; ?>
            <div class="col-0 col-xl-1"></div>
        </div>
        <!-- Main -->
        <main class="padding-0">
            <div class="table-responsive padding-0">
                <div class="row padding-0">
                    <div class="col-0 col-xl-1"></div>
                    <div class="col-12 col-xl-10 form">
                        <h2>Informations casquettes</h2>
                        <!-- Choix de casquette -->
                        <div class="row padding-0">
                            <div class="col-12">
                                <label for="casquettesList">Choisir casquette: </label>
                                <select name="casquettes" id="casquettesList">
                                    <option disabled selected value> -- Sélectionner -- </option>
                                    <?php toCasquetteComboBox(); ?>
                                </select>
                                <!-- Boutons permetant de faire des actions -->
                                <input type="button" id="btnAjouter" name="btnPlus" value=" " class="inputButton fix plus" data-placement="top" data-toggle="tooltip" title="Cliquer ici pour ajouter une casquette">
                                <input id="enterAjout" name="enterAjout" type="hidden" data-placement="top" data-toggle="tooltip" title="Entrer la casquette">
                                <input id="ajoutBD" name="ajoutBD" class="inputButton ajustInput" type="hidden" value="Ajouter casquettes" data-placement="top" data-toggle="tooltip" title="Cliquez ici pour terminer l'ajout">
                                <input type="button" id="modifier" name="modifier" value="Modifier" class="inputButton" disabled data-placement="top" data-toggle="tooltip" title="Modifier le nom de la casquette">
                                <input type="button" id="supprimer" name="supprimer" value="Supprimer" class="inputButton" disabled data-placement="top" data-toggle="tooltip" title="Supprimer la casquette sélectionnée">
                                <!-- Div centenant les tableaux d'épreuves -->
                                <div class="row padding-0 spacer-top">
                                    <!-- Table d'epreuve pour la casquette -->
                                    <div class="col-12 col-md-5 table-responsive table-wrapper-scroll-y">
                                        <table id="epreuveCasquettes" class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Épreuves pour cette casquette</th>
                                                </tr>
                                                <tr>
                                                    <th>Nom</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody id="epreuveCasquette"></tbody>
                                        </table>
                                    </div>
                                    <!-- Boutons transfert d'épreuves -->
                                    <div class="col-12 col-md-2 row cn">
                                        <div class="col-12 verticaly-align cn">
                                            <div class="verticaly-align max-width">
                                                <button type="button" id="addEpreuve" class="arrowLeft fix" disabled></button>
                                                <button type="button" id="removeEpreuve" class="arrowRight fix spacer-top" disabled></button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Table d'epreuves disponibles -->
                                    <div class="col-12 col-md-5 table-responsive table-wrapper-scroll-y spacer-top-mobile">
                                        <table id="epreuveDispos" class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Épreuves disponibles</th>
                                                </tr>
                                                <tr>
                                                    <th>Nom</th>
                                                    <th>Description</th>
                                                </tr>
                                            </thead>
                                            <tbody id="epreuveDispo"></tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- Debug de path -->
                                <input id="rootDir" name="rootDir" type="hidden" value="<?php echo ROOT_DIR; ?>">
                            </div>
                            <div class="col-0 col-xl-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include ROOT_DIR . 'HTML/footer.html'; ?>
    </div>
    <script type="text/javascript">
        setOnChangeAction();
    </script>
</body>

</html>