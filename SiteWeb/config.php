<?php
/**********************************************************
Fichier : config.php
Auteur : Anthony Cote
Date : 2019-04-13
Fonctionnalité : CODE / NOM DE LA FONCTIONNALITÉ
===========================================================
Vérification :
Date        Nom
2019-04-24  Christophe Leclerc APPROUVÉ
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom             Description
2019-04-19  Anthony Cote    Ajout de ROOT_DIR
**********************************************************/
    ini_set('display_errors',1);
    error_reporting(E_ALL);
    
    define('ROOT_DIR', __DIR__."/");
    define("BR", "<br>");
    
    function pingRootDir() { echo "<br>pong<br>"; }
?>