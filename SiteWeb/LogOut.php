<!--****************************************
Fichier : LogOut.php
Auteur  : Anthony Cote
Fonction: Authentification
            Redirige vers la page d'accueil
Date    : 2019-05-01
=========================================================
Vérification :
2019-05-05  Maïka Forestal  Ok    
=========================================================
Historique de modifications :
Date        Nom             Description
2019-04-18  
****************************************/-->
<?php
    session_start(); 
    
    // Reset les variables de session
    $_SESSION['utilisateur'] = "";
    $_SESSION['mdp'] = "";
    
    // Retourne au Login
    header("Location: GUILogin.php");
?>