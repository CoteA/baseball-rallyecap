<?php session_start(); ?>
<!DOCTYPE html>
<!--****************************************
Fichier : GUIEpreuves.php
Auteur  : Anthony Cote
Fonction: Gestion des epreuves
Date    : 2019-04-15
=========================================================
Vérification :
Date        Nom
2019-04-24  Christophe Leclerc  APROUVÉ
2019-05-01  Anthony Cote        Ok
2019-05-05  Maïka Forestal      Enlever ce qui n'est pas utilisé en commentaire
=========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-24  Christophe Leclerc  Modification du responsive.
2019-05-03  Christophe Leclerc  J'ai enlevé les id en double.
2019-05-05  Christophe Leclerc  Formatage et style pour la remise final (responsive)
****************************************/-->

<html lang="fr-ca">

<head>
    <?php
    require_once("config.php");
    require_once ROOT_DIR . 'CtrlLogin.php';
    //checkLogin();
    require_once ROOT_DIR . 'HTML/global_head.html';
    require_once ROOT_DIR . 'PHP/controller/CtrlEpreuve.php';
    require_once 'PHP/gestionnaire/GestionErreurs.php';
    ?>

    <link type="text/css" rel="stylesheet" href="CSS/styleEpreuve.css" />

    <!-- Javascript pour la page uniquement -->
    <script src="JS/jsEpreuve.js"></script>
    <script src="JS/regex.js"></script>
    <title>RallyCap</title>
</head>

<body onload="changerMenu()" onresize="changerMenu()">
    <div class="container-fluid contenuPage">
        <?php
        //CtrlEpreuve::handlePost();
        include ROOT_DIR . 'HTML/header.php';
        include ROOT_DIR . 'HTML/nav.php';
        ?>

        <!-- Menu Onglet -->
        <div class="row padding-0 spacer-top">
            <div class="col-0 col-xl-1"></div>
            <?php include ROOT_DIR . 'menuOnglet.php';  ?>
            <div class="col-0 col-xl-1"></div>
        </div>
        <!-- Main -->
        <main class="row padding-0">
            <div class="table-responsive padding-0">
                <div class="row padding-0">
                    <div class="col-0 col-xl-1"></div>
                    <div class="col-12 col-xl-10 form" id="login">
                        <h2>Informations Evaluation :</h2>
                        <!-- TypeEpreuve -->
                        <div class="row padding-0">
                            <div class="col-12 padding-0">
                                <label for="typeEpreuve">Choisir type :</label>
                                <?php
                                echo CtrlEpreuve::toTypeEpreuveComboBox();
                                ?>
                                <input type="submit" name="action" class="inputButton fix plus" value=" " id="btnPlus" data-toggle="tooltip" title="Cliquer ici pour ajouter un nouveau type d'épreuve">
                                <!-- TODO : Comprendre a quoi ca sert -->
                                <input type="hidden" name="ajoutTypeEpreuve" id="ajoutTypeEpreuve" data-toggle="tooltip" title="Entrer le nouveau type d'épreuve">
                                <input type="hidden" name="action" class="inputButton ajustInput" value="Ajouter Type d'épreuve" id="btnAdd" data-toggle="tooltip" title="Cliquer ici pour ajouter le type d'épreuve">
                            </div>
                        </div>
                        <!-- Nom et description -->
                        <!-- Nom -->
                        <div class="form-group row padding-0 spacer-top">
                            <div class="col-12 padding-0">
                                <label for="nom">Nom:</label>
                                <input type="text" name="nom" id="nom" onblur="validerNom(this.id)" data-toggle="tooltip" title="Entrer un nom pour l'épreuve">
                            </div>
                        </div>
                        <!-- Description -->
                        <div class="form-gourp row padding-0 spacer-top">
                            <label class="control-label" for="description">Description:</label>
                            <textarea class="form-control" type="text" name="description" id="description" data-toggle="tooltip" title="Entrer une description pour l'épreuve"></textarea>
                        </div>

                        <!-- Nombres -->
                        <!-- Nbr a reussir -->
                        <div class="form-group row spacer-top">
                            <div class="col-12 col-md-6">
                                <label class="" for="nbrEssaiMin">Nombre à&nbsp;reussir:</label>
                                <input class="" type="number" name="nbrEssaiMin" id="nbrEssaiMin" data-toggle="tooltip" title="Entre le nombre de fois que le test doit être réussi">
                            </div>
                            <div class="col-12 col-md-6">
                                <label class="" for="nbrEssaiTotal">Nombre d'essais:</label>
                                <input class="" type="number" name="nbrEssaiTotal" id="nbrEssaiTotal" data-toggle="tooltip" title="Entrer le nombre de fois que le test peut être effectuer">
                            </div>
                        </div>


                        <!-- Boutons CRUD -->
                        <div class="col-12 col-md-12 spacer-top">
                            <!-- <button type="button" name="action" value="rechEpreuve" id="rechEpreuve">Rechercher</button> -->
                            <button type="button" name="action" value="ajoutEpreuve" id="ajoutEpreuve" data-toggle="tooltip" title="Cliquer ici pour ajouter l'épreuve">Ajouter</button>
                            <button type="button" name="action" value="modifEpreuve" id="modifEpreuve" disabled data-toggle="tooltip" title="Cliquer ici pour modifier l'épreuve sélectionnée">Modifier</button>
                            <button type="button" name="action" value="suppEpreuve" id="suppEpreuve" disabled data-toggle="tooltip" title="Cliquer ici pour supprimer l'épreuve sélectionnée">Supprimer</button>
                            <input id="rootDir" name="rootDir" type="hidden" value="<?php echo ROOT_DIR; ?>">
                        </div>

                        <!-- Table des epreuves -->
                        <div class="row padding-0">
                            <div class="col-12 table-responsive table-wrapper-scroll-y spacer-top">
                                <table id="epreuve" class="table">
                                    <thead>
                                        <th>Nom de l'épreuve</th>
                                        <th>Description</th>
                                        <th>Nombre à réussir</th>
                                        <th>Nombre d'essai</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $ctrl = new CtrlEpreuve();
                                        $ctrl->loadTableEpreuve();
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-1 col-md-1"></div>
                </div>
            </div>
        </main>

        <?php
        include ROOT_DIR . 'HTML/footer.html';
        ?>
    </div>
    <script type="text/javascript">
        setOnChangeAction();
    </script>
</body>

</html>