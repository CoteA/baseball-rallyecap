<?php session_start(); ?>
<!--/**********************************************************
Fichier :           GUIBenevole.php
Auteur  :           Maïka Forestal
Date    :           12 avril 2019
Fonctionnalité :    FW-01 / Gestion des bénévoles
===========================================================
Vérification :
Date        Nom             Approuvé
2019-05-05  Anthony Cote    Ok
2019-05-05  Maïka Forestal  Ok
===========================================================
Historique de modifications :
Date        Nom                 Description
2019-05-05  Christophe Leclerc  Formatage final
**********************************************************/-->
<!DOCTYPE html>
<html lang="fr">

<head>
    <?php
    require_once 'config.php';
    require_once ROOT_DIR . 'CtrlLogin.php';
    //checkLogin();
    require_once ROOT_DIR . 'HTML/global_head.html';
    ?>
    <script src="JS/jsBenevole.js"></script>
    <link href="CSS/styleBenevole.css" rel="stylesheet" />
    <title>RallyCap</title>
</head>

<body onload="changerMenu()" onresize="changerMenu()">
    <div class="container-fluid contenuPage">
        <?php
        require_once 'config.php';
        require_once 'PHP/utils/DatabaseManager.php';
        require_once 'PHP/CLASS/Benevole.php';
        require_once 'PHP/gestionnaire/GestionBenevole.php';
        require_once 'PHP/gestionnaire/GestionErreurs.php';

        include 'HTML/header.php';
        include 'HTML/nav.php';
        ?>
        <div class="row padding-0 spacer-top">
            <div class="col-0 col-xl-1"></div>
            <?php include ROOT_DIR . 'menuOnglet.php'; ?>
            <div class="col-0 col-xl-1"></div>
        </div>
        <main>
            <div id="Evaluateurs" class="table-responsive ">
                <div class="row padding-0">
                    <div class="col-0 col-xl-1"></div>
                    <!-- Formulaire -->
                    <form class="col-12 col-xl-10" id="formBenevole" action="PHP/controller/CtrlBenevole.php" method="post">
                        <div id="infoEvaluateur" class="col-sm-12 col-md-6 col-xl-4">
                            <h2>Information évaluateur</h2>
                            <!-- Nom -->
                            <label class="col-xl-4" for="nomEvaluateur">Nom :</label>
                            <input class="col-xl-6" autocomplete="off" type="text" name="nomEvaluateur" id="nomEvaluateur" required onblur="validerNom(this.id)">
                            <?php nomInvalide() ?>
                            <!-- Prenom -->
                            <label class="col-xl-4" for="prenomEvaluateur">Prénom :</label>
                            <input class="col-xl-6" autocomplete="off" type="select" name="prenomEvaluateur" id="prenomEvaluateur" required onblur="validerPrenom(this.id)">
                            <?php prenomInvalide() ?>
                            <!-- TypeBenevole -->
                            <label class="col-xl-4" for="typeBenevole">Type :</label>
                            <select onchange="activeBenevole()" class="form-control col-xl-5" name="typeBenevole" id="typeBenevole" required>
                                <?php include "PHP/Structures/loadTypeBenevole.php" ?>
                            </select>
                            <!-- Telephone -->
                            <label class="col-xl-4" for="telEvaluateur">Téléphone :</label>
                            <input class="col-xl-6" autocomplete="off" placeholder="819-123-456" type="text" name="telEvaluateur" id="telEvaluateur" onblur="validerTelephone(this.id)">
                            <?php telInvalide() ?>
                            <!-- Courriel -->
                            <label class="col-xl-4" for="courrielEvaluateur">Courriel :</label>
                            <input class="col-xl-6" autocomplete="off" placeholder="exemple@rallycap.ca" type="text" name="courrielEvaluateur" id="courrielEvaluateur" required onblur="validerCourriel(this.id)">
                            <?php courrielInvalide() ?>
                        </div>
                        <div id="infoCompte" class="col-sm-12 col-md-6 col-xl-4">
                            <h2>Informations compte</h2>
                            <!-- Nom -->
                            <label class="col-xl-5" for="utilisateur">Nom d'utilisateur :</label>
                            <input class="col-xl-6" autocomplete="off" type="text" name="utilisateur" id="utilisateur" onblur="validerUtilisateur(this.id)">
                            <?php utilisateurInvalide() ?>
                            <!-- Mot de passe -->
                            <label class="col-xl-5 champMdp" for="mdp">Mot de passe:</label>
                            <input class="col-xl-6 champMdp" type="password" name="mdp" id="mdp" onkeyup="detecterMaj()" />
                            <?php motDePasseInvalide()?>

                            <!-- Affichage mot de passe -->
                            <div class="col-xl-1"></div>
                            <div class="row padding-0">
                                <input type="checkbox" name="showMDP" id="showMDP" onclick="afficherMotDePasse()" onblur="validerMotDePasse(this.id)">
                                <p id="affMdpText">Afficher le mot de passe</p>
                            </div>
                            <!-- Actif -->
                            <label class="switch">
                                <input type="checkbox" name="Actif" id="switchActif" checked>
                                <span class="slider">
                                    <div id="estActif" class="col-xl-6">Activé</div>
                                    <div id="nonActif" class="col-xl-6">Désactivé</div>
                                </span>
                            </label>
                        </div>
                        <div id="groupedeEvaluateur" class="col-sm-12 col-md-12 col-xl-4" class="row">
                            <h2>Groupes d'enfants</h2>
                            <!-- Date evaluation -->
                            <label class="col-xl-6 champMdp" id="lblDateEval" for="dateEval">Date de l'évaluation :</label>
                            <?php include 'PHP\Structures\loadEvent.php' ?>
                        </div>
                        <!-- Boutons CRUD -->
                        <div id="formBtn" class=" col-11 col-md-11 col-xl-11">
                            <button id="rechEvaluateur" type="button" onclick="rechercherBenevole()">Rechercher</button>
                            <button id="ajoutEvaluateur" type="button" name="action" value="ajouter" onclick="ajoutBenevole()">Ajouter</button>
                            <button id="modifBenevole" type="button" name="action" value="modifier" onclick="modifierBenevole()" disabled>Modifier</button>
                            <button id="suppBenevole" type="button" name="action" value="supprimer" onclick="supprimerBenevole()" disabled>Supprimer</button>
                        </div>
                        <!-- Recherche -->
                        <div id="zoneRecherche" class="col-12 col-md-12 col-xl-12" hidden>
                            <?php include 'HTML/rechercheBenevole.html' ?>
                        </div>
                        <!-- Table de benevoles -->
                        <div class="row padding-0 table-responsive" id="tabBenevole">
                            <div class="col-12  table-wrapper-scroll-y">
                                <table class="table" id="listBenevole">
                                    <thead>
                                        <th>Nom</th>
                                        <th>Prénom</th>
                                        <th>Type</th>
                                        <th>Téléphone</th>
                                        <th>Courriel</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        include 'PHP/Structures/loadBenevole.php';
                                        loadAllBenevole();
                                        ?>
                                    </tbody>
                                </table>
                                <script>
                                    setRows("listBenevole");
                                    loadInfoBulle();
                                </script>
                            </div>
                        </div>
                    </form>
                    <div class="col-0 col-xl-1"></div>
                </div>
            </div>
        </main>
    </div>
    <?php include 'HTML/footer.html'; ?>
    <script src="JS/SortableTable.js"></script>
</body>
<script>


</script>
</html>