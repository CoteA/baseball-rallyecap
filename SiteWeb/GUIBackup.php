﻿<?php session_start(); ?>
<!DOCTYPE html>
<!--****************************************
Fichier : GUIBackup.php
Auteur  : Anthony Cote
Fonction: Page de backup
Date    : 2019-05-01
=========================================================
Vérification :
2019-05-05  Maïka Forestal  Ok
=========================================================
Historique de modifications :
Date        Nom                 Description
2019-05-01  
****************************************/-->


<html lang="fr-ca">

<head>
    <?php
    require_once 'config.php';
    require_once ROOT_DIR . 'CtrlLogin.php';
    //checkLogin();
    include ROOT_DIR . 'HTML/global_head.html';
    ?>

    <!-- TODO C'est deja dans config.php -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.0/umd/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="CSS/resetCSS.css" rel="stylesheet" />
    <link href="CSS/styles.css" rel="stylesheet" />

    <title>RallyCap</title>
</head>

<body onload="changerMenu()" onresize="changerMenu()">
    <?php

    require_once 'PHP/utils/DatabaseManager.php';
    require_once 'PHP/controller/CtrlBackup.php';
    include 'HTML/header.php';
    include 'HTML/nav.php';
    ?>
    <div class="container-fluid contenuPage">
        <main class="row spacer-top">
            <div class="col-0 col-xl-1"></div>
            <form class="col-12 col-xl-10" id="login" action="GUIRapportSauvegarde.php" method='POST'>

                La location du fichier de backup par defaut est :
                <?php echo DatabaseManager::BACKUP_LOCATION. Databasemanager::DEFAULT_BACKUP_FILENAME; ?>
                <br><br>
                
                <!-- Bouton Backup -->
                <button type="submit" name="action" value="backup" id="backup">Backup</button>

                <!-- Button Restaurer -->
                <button type="submit" name="action" value="restore" id="restore">Restaure</button>

                <br><br>
                <?php handlePost();?>
            </form>
            <div class="col-0 col-xl-1"></div>

        </main>
    </div>
    <?php include 'HTML/footer.html'; ?>
    <script>$('#msgAide').load('../HTML/info/infoBackup.html');</script>
</body>

</html>