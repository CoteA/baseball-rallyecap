<!--****************************************
Fichier : nav.php
Auteur  : Christophe Leclerc
Fonction: Nav à inclure dans toutes les pages du site web.
Date    : 2019-04-15
=========================================================
Vérification :
2019-04-15     Massinisah Talah,
               Maïka Forestal

2019-04-18     Anthony Cote         TODO : Mieux documenter le code
2019-04-03    Massinisa Talah
2019-05-05     Maïka Forestal       Ok
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
=========================================================
Historique de modifications :

Date        Nom                 Description
2019-04-18  Anthony Cote        Utilise une version simplifiee tant que c'est bugguer
2019-04-03  Massinisa Talah     changement de code pour pouvoir insérer des id et des classes au besoin
2019-05-05  Maïka Forestal      Enlever les bloques de code en commentaire non-utilisé
                                Ajout de commentaires plus clair
2019-05-05  Christophe Leclerc  Modification du responsive.
****************************************/-->
<script src="JS/menu.js"></script>
<nav id="menu" class="col-12 col-md-12 col-xl-12 ">
    <div id="menuIcon" hidden onclick="afficherMenu()">
    <div id=icon>
        <div></div>
        <div></div>
        <div></div>
    </div>
    </div>

    <?php
        $tabNavLien   = array("GUIBenevoles.php", "GUIEvenement.php", "GUIEquipe.php", "GUIBackup.php");    // Chemin des pages du site
        $tabNavTexte  = array("Gestion des données", "Programmer les évaluations", "Création des équipes", "Backup");   // Nom des pages

        $NavOnglet = "\n <div padding-0' class='vertical-menu' id='verticalMenu'>\n <ul id=''>\n";

        // Pour tous les pages du sites
        foreach ($tabNavLien as $cle => $lien) {
            $NavOnglet .= "<li class='col-12 col-xl-3'";

            // Verifie si nous somme sur la page actuelle
            if (pathinfo($_SERVER['PHP_SELF'])['basename'] == $lien) {
                $NavOnglet .= " id='active'";
            }

            // Affiche le nom de la page, et attribut le lien
            $NavOnglet .= "> <a href='$lien'> $tabNavTexte [$cle]  </a> </li> \n";
        }
        $NavOnglet .= "</ul>\n</div>";
        echo $NavOnglet;
    ?>
</nav>
