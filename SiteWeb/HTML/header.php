<!--****************************************
Fichier : header.php
Auteur  : Christophe Leclerc
Fonction: header à inclure dans toutes les pages du site web.
Date    : 2019-04-15
=========================================================
Vérification :
2019-04-15  Massinisah Talah    Ok
2019-04-15  Maïka Forestal      Ok
2019-05-05  Anthony Cote        Ok
2019-05-05 Christophe Leclerc Approuvé final (plus de modification)
=========================================================
Historique de modifications :
Date        Nom                 Description

****************************************/-->
<header>
    RallyCap

    <a href="LogOut.php" id="logout">
        <img src="../images/homerun.png" alt="LogOut">
    </a>

    <?php
    include_once ROOT_DIR . 'PHP/utils/unsorted.php';
    infoBulle();
    ?>

</header>
