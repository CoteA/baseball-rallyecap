﻿<!DOCTYPE html>
<!--****************************************
Fichier : GUIRecupererMotPasse.php
Auteur  : Maika Forestal
Fonction: interface de recuperation de mot de passe
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-04  Anthony Cote
2019-05-05  Maïka Forestal  Ok
=========================================================
Historique de modifications :
Date        Nom             Description
2019-04-18  
****************************************/-->
<html lang="fr-ca">
<head>
    <meta charset="utf-8" />
    <title>Rallycap</title>
    <link rel="stylesheet" href="CSS/styleAuthentification.css" />
</head>
<body>
    <header></header>
    <div id="contenu">
        <div class="rouge">
            <form id="newMDP" action="PHP/changePassword.php" method='POST'>
                <h1>Récupération du mot de passe</h1>

                <!-- Utilisateur -->
                <label for="Utilisateur">Utilisateur :</label>
                <input type="text" name="user" id="user" />

                <!-- Mot de passe -->
                <label for="nouvMdp">Nouveau mot de passe :</label>
                <input type="text" name="nouvMdp" id="nouvMdp" />

                <!-- Confimation de mot de passe -->
                <label for="confMdp">Confirmer le mot de passe :</label>
                <input type="text" name="confMdp" id="confMdp" />

                <!-- Bouton de confirmation -->
                <button type="submit">Confirmer</button>

                <!-- Retour a l'authentification -->
                <a id="pagePrecedente" href="GUILogin.html">Retourner à l'authentification</a>
            </form>
            
            <!-- Images -->
            <img id="baseballIcone" src="images/baseball_logojpg.jpg" alt="Image d'une batte et d'une balle de baseball" />
            <img id="rallycap" src="images/rallyCap Canada.jpg" alt="Image du rallyecap" />
        </div>

    </div>
    <footer class="rouge"></footer>
</body>
</html>