﻿<?php session_start(); ?>
<!DOCTYPE html>
<!--****************************************
Fichier : GUIEquipe.php
Auteur  : 
Fonction: Gestion des equipe
Date    : 2019-04-15
=========================================================
Vérification :
Date        Nom
2019-04-24  Anthony Cote        Ok
2019-05-05  Maïka Forestal      Ok
=========================================================
Historique de modifications :
Date        Nom                 Description
2019-04-24  
****************************************/-->

<html lang="fr-ca">

<head>
    <?php
    require_once 'config.php';
    require_once ROOT_DIR . 'CtrlLogin.php';
    //checkLogin();
    include ROOT_DIR . 'HTML/global_head.html';
    ?>


    <link href="CSS/styleEquipe.css" rel="stylesheet" />

    <title>RallyCap</title>
</head>

<body onload="changerMenu()" onresize="changerMenu()">
    <?php
    require_once 'config.php';
    require_once 'PHP/utils/DatabaseManager.php';
    require_once 'PHP/CLASS/Benevole.php';
    require_once 'PHP/gestionnaire/GestionBenevole.php';
    require_once 'PHP/gestionnaire/GestionErreurs.php';

    include 'HTML/header.php';
    include 'HTML/nav.php';
    ?>
    <div class="container-fluid contenuPage">

        <!-- Main -->
        <main>
            <div id="contenuPage">
                <form id="formEquipe" class="col-10 col-md-10 col-xl-10 ml-auto mr-auto">
                    <div id="choixCasquette" class="row col-12 col-md-5 col-xl-5 mr-auto ml-auto">
                        <label for="casquettes" class="col-md-3 float-left">Casquette: </label>  
                        <?php include 'PHP/Structures/loadCasquettesList.php' ?>
                    </div> 
                    <div id="listJoueur" class="col-md-3 col-xl-3 table-responsive">
                        <table class="table">
                            <thead>
                                <th>Joueurs</th>
                            </thead>
                            <tbody>
                                <?php ?>
                            </tbody>
                        </table>
                    </div>

                    <div id="creerEquipe" class="col-md-6 col-xl-6  table-responsive">
                    <table class="table">
                            <thead>
                                <th colspan="2">Joueurs de l'équipe:</th>
                            </thead>
                            <tbody>
                               <tr>
                                   <td></td>
                                   <td></td>
                               </tr>
                               <tr>
                                   <td></td>
                                   <td></td>
                               </tr>
                               <tr>
                                   <td></td>
                                   <td></td>
                               </tr>
                               <tr>
                                   <td></td>
                                   <td></td>
                               </tr>
                               <tr>
                                   <td></td>
                                   <td></td>
                               </tr>
                            </tbody>
                        </table>
                    </div> 

                    <div id="listBenevole" class="col-md-3 col-xl-3 table-responsive ">
                    <table class="table">
                            <thead>
                                <th>Bénévoles</th>
                            </thead>
                            <tbody>
                                <?php
                                    include 'PHP/Structures/loadBenevole.php';
                                    loadEntraineur();
                                ?>
                            </tbody>
                        </table>
                    </div> 
                </form>
            </div>
        </main>
    </div>
    <?php include 'HTML/footer.html'; ?>
    <script>$('#msgAide').load('../HTML/info/infoEquipe.html');</script>
</body>

</html>