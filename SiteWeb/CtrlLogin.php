<!--****************************************
Fichier : CtrlLogin.php
Auteur  : Anthony Cote
Fonction: Authentification
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-05  Maïka Forestal  TODO:A mettre dans Controller     
=========================================================
Historique de modifications :
Date        Nom             Description
2019-04-18  Anthony Cote    Reformatage
****************************************/-->

<?php
    ini_set('display_errors',1);
    error_reporting(E_ALL);
    
    require_once("PHP/utils/DatabaseManager.php");
    
    
    
    /** Verifi si l'utilisateur a de bon credentials
     * @return void     */
    function isLogged()
    {
        $user = $_SESSION['utilisateur'];
        $pass = $_SESSION['mdp'];
        
        // TODO : Prevenir l'injection SQL
        $sql = "SELECT COUNT(*) as count
                FROM Benevole 
                WHERE username = '$user' 
                AND mot_de_passe = '$pass';";
        
        $rs = DatabaseManager::getResultSet($sql);
        
        if ($rs == false) return false;
        
        $row = $rs->fetch_assoc();
        return $row["count"] != 0;
    }
    
    
    /** Verifi si l'utilisateur a de mauvais credentials
     * @return void     */
    function isNotLogged() { return ! isLogged(); }
    
    
    /** Renvoi les user non connecter vers le login
     * @return void     */
    function checkLogin()
    {
        if(isNotLogged()) {
            header('Location: GUILogin.php'); 
            exit();
        }  
    }
    
?>
