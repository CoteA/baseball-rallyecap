<?php session_start(); ?>
<!DOCTYPE html>
<!--/****************************************
Fichier : GUIJoueurs.php
Auteur  : Massinissa Talah
Fonction: Affiche une interface à l'utilisateur afin d'ajouter, modifer et supprimer un joueur
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-01  Anthony Cote    Ok
2019-05-05  Maïka Forestal  Ok
=========================================================
Historique de modifications :
Date        Nom             Description
2019-04-18  Anthony Cote    Refactoring et extraction de parties globales
****************************************/-->
<html lang="fr-ca">

<head>
  <?php

  require_once 'PHP/gestionnaire/GestionErreurs.php';
  include 'config.php';
  require_once ROOT_DIR . 'CtrlLogin.php';
  //checkLogin();
  include ROOT_DIR . 'HTML/global_head.html';
  ?>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed" rel="stylesheet">
  <link href="CSS/styleJoueur.css" type="text/css" rel="stylesheet" />
  <script src="JS/jsJoueur.js"></script>
  <script src="JS/regex.js"></script>
  <script src="JS/popper.js"></script>

  <title>RallyCap</title>
</head>

<body onload="changerMenu()" onresize="changerMenu()">

  <div class="contenuPage">
    <?php
    include ROOT_DIR . 'HTML/header.php';
    include ROOT_DIR . 'HTML/nav.php';
    ?>
    <div class="row padding-0 spacer-top">
      <div class="col-0 col-xl-1"></div>
      <?php include ROOT_DIR . 'menuOnglet.php'; ?>
      <div class="col-0 col-xl-1"></div>
    </div>
    <main>

      <div id="Enfants" class="table-responsive">
        <div class="row padding-0">
          <div class="col-0 col-xl-1"></div>
          <div class="col-12 col-xl-10 form" method="post" action="PHP/controller/CtrlJoueur.php">

            <div id="infoEnfant" class="col-12 col-md-4 col-xl-4">
              <h2>Information enfant</h2>

              <!-- Nom -->
              <div class="form-group">
                <label class="control-label" for="nom">Nom</label>
                <div class="">
                  <input type="text" class="form-control format" id="nom" placeholder="Entrer nom" name="nom" onblur="validerNom(this.id)" data-placement="left" data-toggle="tooltip" title="Exemple format : Trembley">
                  <?php nomInvalide() ?>
                </div>
                
              </div>

              <!-- Prenom -->
              <div class="form-group">
                <label class="control-label" for="prenom">Prénom</label>
                <div class="">
                  <input type="text" class="form-control" id="prenom" placeholder="Entrer prénom" name="prenom" onblur="validerPrenom(this.id)" data-placement="left" data-toggle="tooltip" title="Exemple format : Simon">
                  <?php prenomInvalide() ?>
                </div>
              </div>

              <!-- Date de naissance -->
              <div class="form-group">
                <label class="control-label" for="dateNaissance">Date de naissance</label>
                <div class="">
                  <input type="text" class="form-control" id="dateNaissance" placeholder="2000-01-01" name="dateNaissance" onblur="validerDate(this.id)" data-placement="left" data-toggle="tooltip" title="Le bon format : AAAA-MM-JJ">
                  <?php dateInvalide()?>
                </div>
              </div>
              <!-- je les utilise pour trouver le joueur après la selection-->
              <input type="hidden" name="nomHidden" id="nomHidden" value="" />
              <input type="hidden" name="prenomHidden" id="prenomHidden" value="">
              <input type="hidden" name="dateNaissanceHidden" id="dateNaissanceHidden" value="">

              <!-- Casquette -->
              <div class="form-group">
                <label class="control-label" for="couleurCasquette">Casquette</label>
                <div class="">
                  <?php include "PHP/Structures/loadCasquettesList.php" ?>
                </div>
              </div>
              <input type="radio" name="etat" class="etat" id="actif" value="true"> Activé
              <input type="radio" name="etat" class="etat" id="desactive" value="false"> Désactivé
            </div>
            <!-- Tuteur -->
            <div id="infoTuteur" class="col-12 col-md-4 col-xl-4">
              <h2>Informations tuteur</h2>

              <!-- Nom -->
              <div class="form-group">
                <label class="control-label" for="nomTuteur">Nom</label>
                <div class="">
                  <input type="text" class="form-control format" id="nomTuteur" placeholder="Entrer nom" required name="nomTuteur" onblur="validerNom(this.id)" data-placement="left" data-toggle="tooltip" title="Exemple format : Trembley">
                  <?php nomInvalide() ?>
                </div>
              </div>

              <!-- Prenom -->
              <div class="form-group">
                <label class="control-label" for="prenomTuteur">Prénom</label>
                <div class="">
                  <input type="text" class="form-control" id="prenomTuteur" placeholder="Entrer prénom" required name="prenomTuteur" onblur="validerPrenom(this.id)" data-placement="left" data-toggle="tooltip" title="Exemple format : Simon">
                  <?php prenomInvalide() ?>
                </div>
              </div>

              <!-- Telephone -->
              <div class="form-group">
                <label class="control-label" for="telTuteur">Téléphone</label>
                <div class="">
                  <input type="text" class="form-control" id="telTuteur" placeholder="819-123-4567" name="telTuteur"  onblur="validerTelephone(this.id)" data-placement="left" data-toggle="tooltip" title="le bon format : 999-999-9999">
                  <?php telInvalide()?>
                </div>
              </div>
              <!-- je l'utilise pour trouver le tuteur du joueur selectionné-->
              <input type="hidden" name="telTuteurHidden" id="telTuteurHidden" value="" />

              <!-- Courriel -->
              <div class="form-group">
                <label class="control-label" for="courrielTuteur">Courriel</label>
                <div class="">
                  <input type="text" class="form-control" id="courrielTuteur" placeholder="exemple@hotmail.com" required name="courrielTuteur" onblur="validerCourriel(this.id)" data-placement="left" data-toggle="tooltip" title="Exemple format : exemple@hotmail.com">
                  <?php courrielInvalide()?>
                </div>
              </div>
            </div>

            <!-- Evaluateur -->
            <div id="evaluateur" class="col-12 col-md-4 col-xl-4">
              <h2>Évaluateur</h2>

              <div class="form-group">
                <label class="control-label" for="selectionDate">Sélectionner la date</label>
                <div class="">
                  <div class="dropdown">
                    <button class="btn btn-info dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Couleur
                      <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Blanc</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Gris</a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Noir</a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <button id="consulterGroupe">Consulter les groupes</button>
            </div>

            <!-- Bouton CRUD -->
             <div id="lesBtn" class="col-11 col-md-11 col-xl-11">
               <button id="rechEnfant">Rechercher</button>
               <button class="inputButton" type="submit" id="ajoutJoueur" name="ajoutJoueur" value="Ajouter" onclick="ajouter()" data-toggle="tooltip" title="Cliquer pour ajouter un joueur">Ajouter</button>

               <button id="modifJoueur" name="modifJoueur" disabled data-toggle="tooltip" title="Selectionner une ligne avant de modifier" onclick="modifierJoueur()">Modifier</button>
               <button id="suppJoueur" name="suppJoueur" disabled data-toggle="tooltip" title="Selectionner une ligne avant de supprimer" onclick="supprimerJoueur()">Supprimer</button>
               <input id="rootDir" name="rootDir" type="hidden" value="<?php echo ROOT_DIR; ?>">

             </div>
             <div class="row table-responsive" id="tabJoueur">
               <div class="col-12 table-wrapper-scroll-y ml-auto mr-auto table-responsive">

                 <?php
                  require_once ROOT_DIR . 'PHP/controller/CtrlJoueur.php';

                  afficher(); ?>
                  <script> setRows(); </script>
              </div>
            </div>
          </div>
          <div class="col-0 col-xl-1"></div>
        </div>
      </div>
    </div>
  </main>
  <input id="rootDir" name="rootDir" type="hidden" value="<?php echo ROOT_DIR; ?>">

  <?php include ROOT_DIR . 'HTML/footer.html'; ?>

  <script src="JS/SortableTable.js"></script>
</body>

</html>
