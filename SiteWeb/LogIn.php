<!--****************************************
Fichier : LogIn.php
Auteur  : Anthony Cote
Fonction: Authentification
            Redirige vers la page d'accueil
Date    : 2019-04-29
=========================================================
Vérification :
2019-05-05  Maïka Forestal  Ok    
=========================================================
Historique de modifications :
Date        Nom             Description
2019-04-18  
****************************************/-->
<?php
    session_start(); 
    
    ini_set('display_errors',1);
    error_reporting(E_ALL);
    require_once 'CtrlLogin.php';
    
    
    /** Recupere le user et mot de passe dans le POST
     * @return void
     */
    function getUserAndPassFromPost(){
        
        if(isset($_POST["utilisateur"]) && isset($_POST["mdp"])){
            
            $_SESSION['utilisateur'] = $_POST['utilisateur'];
            $_SESSION['mdp'] = $_POST['mdp'];
        }
    }
    
    /** Redirige vers la bonne page selon l'etat de login
     * @return void
     */
    function redirect()
    {
        if(isLogged()) { 
            header("Location: GUIBenevoles.php"); 
        } else {
            header("Location: GUILogin.php");}
    }
    
    
    getUserAndPassFromPost();
    redirect();
?>