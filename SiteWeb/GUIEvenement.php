<?php session_start(); ?>
<!DOCTYPE html>
<!--/****************************************
Fichier : GUIEvenement.php
Auteur  : Massinissa Talah
Fonction: Affiche une interface à l'utilisateur afin d'ajouter un evenement et de générer les groupes d'evaluation
Date    : 2019-04-15
=========================================================
Vérification :
2019-05-05  Anthony Cote    Ok
2019-05-05  Maïka Forestal  Ok
=========================================================
Historique de modifications :
Date        Nom             Description
****************************************/-->
<html lang="fr-ca">

<head>
  <?php
  require_once 'PHP/gestionnaire/GestionErreurs.php';
  include 'config.php';
  require_once ROOT_DIR . 'CtrlLogin.php';
  //checkLogin();
  include ROOT_DIR . 'HTML/global_head.html';
  ?>
  <title>Rallycap</title>
  <link href="CSS/styleEvenement.css" type="text/css" rel="stylesheet" />
  <script src="JS/jsEvenement.js"></script>
  <script src="JS/regex.js"></script>

</head>

<body onresize="changerMenu()">
  <?php
  include 'HTML/header.php';
  include 'HTML/nav.php';
  ?>

  <div class="col-12 col-md-10 col-xl-10 ml-auto mr-auto row">
    <h2>Sur cette fenetre
      <span class="txt-rotate" data-period="2000" data-rotate='[ "on ajoute un événement.", "et on compose les groupes." ]'></span>
    </h2>
  </div>

  <div class="returnMessage col-12 col-md-10 ml-auto mr-auto"></div>


  <div class="row">
    <div class="col-md-1"></div>
    <form class="form-horizontal col-md-10 col-12 formAjoutEvent" action="PHP/controller/CtrlEvenementEvaluation.php" method="post">

      <!-- Adresse -->
      <div class="form-group">
        <label class="control-label col-md-2" for="adresse">Adresse</label>
        <div class="col-md-11">
          <input type="text" class="form-control" id="adresse" name="adresse" placeholder="Entrer adresse" onblur="validerAdresse(this.id)" data-placement="left" data-toggle="tooltip" title="À ne pas laisser vide. Entrer une adresse valide">
          <?php adresseInvalide() ?>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-6">

          <!-- Jour -->
          <div class="form-group">
            <label class="control-label col-md-2" for="jour">Jour</label>
            <div class="col-md-10">
              <select class="form-control" id="jour" name="jour">
                <?php
                // Jour du mois
                for ($j = 1; $j < 32; $j++) {
                  if ($j < 10) {
                    $j = '0' . $j;
                  } // Format avec un zero : 01, 02, 03, 04
                  echo '<option value=\'' . $j . '\'>' . $j . '</option>';
                }
                ?>
              </select>
            </div>
          </div>

          <!-- Mois -->
          <div class="form-group">
            <label class="control-label col-md-2" for="mois">Mois</label>
            <div class="col-md-10">
              <select class="form-control" id="mois" name="mois">
                <option value="01">Janvier</option>
                <option value="02">Février</option>
                <option value="03">Mars</option>
                <option value="04">Avril</option>
                <option value="05">Mai</option>
                <option value="06">Juin</option>
                <option value="07">Juillet</option>
                <option value="08">Août</option>
                <option value="09">Septembre</option>
                <option value="10">Octobre</option>
                <option value="11">Novembre</option>
                <option value="12">Décembre</option>
              </select>
            </div>
          </div>

          <!-- Annee -->
          <div class="form-group">
            <label class="control-label col-md-2" for="annee">Année</label>
            <div class="col-md-10">
              <input type="text" class="form-control" id="annee" name="annee" placeholder="2019" onblur="validerAnnee(this.id)" data-placement="left" data-toggle="tooltip" title="Exemple format : 2019">
              <?php anneeInvalide() ?>
            </div>
          </div>
        </div>

        <!-- Heures:minutes -->
        <div class="col-12 col-md-6">
          <!-- Heure -->
          <div class="form-group">
            <label class="control-label col-md-2" for="heure">Heure</label>
            <div class="col-md-10">
              <select class="form-control" id="heure" name="heure">
                <?php
                // Heure d'ouvertures
                for ($h = 8; $h < 23; $h++) {
                  if ($h < 10) {
                    $h = '0' . $h;
                  }
                  echo '<option value=\'' . $h . '\'>' . $h . '</option>';
                } ?>
              </select>
            </div>
          </div>

          <!-- Minutes -->
          <div class="form-group">
            <label class="control-label col-md-2" for="min">Min.</label>
            <div class="col-md-10">
              <select class="form-control" id="min" name="min">
                <option value="00">00</option>
                <option value="15">15</option>
                <option value="30">30</option>
                <option value="45">45</option>
              </select>
            </div>
          </div>

        </div>
      </div>

      <!-- Bouton Ajouter -->
      <div class="form-group">
        <input type="submit" class="form-control inputButton" name="ajoutEvent" id="ajoutEvent" value="Ajouter" data-toggle="tooltip" title="Cliquer pour ajouter un événement">
      </div>

    </form>
    <div class="col-md-1"></div>
  </div>

  <div class="row">
    <div class="col-md-1"></div>
    <form class="form-horizontal col-md-10 col-12 formComposerGroupe" method="post">

      <!-- Choix de l'evenement -->
      <div class="form-group">
        <label class="control-label col-md-4" for="choixEvenement">Choisir l'événement:</label>
        <div class="col-md-4 ml-auto mr-auto">
          <?php include 'PHP\Structures\loadEvent.php' ?>
        </div>
      </div>

      <!-- Bouton Composer Groupe -->
      <div class="form-group col-md-4 ml-auto mr-auto">
        <input type="submit" class="form-control inputButton" name="composerGroupe" id="composerGroupe" value="Composer" data-toggle="tooltip" title="Cliquer pour générer les groupes d'évaluation">
      </div>
    </form>
    <div class="col-md-1"></div>
  </div>

  <div class="divAffichage col-12 col-md-10 ml-auto mr-auto">  </div>

  <?php include 'HTML/footer.html'; ?>

</body>

</html>
