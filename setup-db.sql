
DROP DATABASE baseball_db;


CREATE DATABASE baseball_db;
USE baseball_db;

/* CREATE TABLES */

CREATE TABLE TypeRole (
    id_type_role INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    description VARCHAR(255)
); 

INSERT INTO TypeRole
(id_type_role, nom, description)
VALUES
(1,"Coach","Donne des conseils"),
(2,"Entraineur","Supervise l'entrainement"),
(3,"Assistant","Assiste les autres benevoles"),
(4,"Gestionnaire","S'occuppe de la paperasse");

CREATE TABLE TypeBenevole (
    id_type_benevole INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    description VARCHAR(255)
); 

INSERT INTO TypeBenevole
(id_type_benevole, nom, description)
VALUES
(1, "Organisateur","Organise des activites"),
(2, "Ã‰valuateur","Ã‰valu le niveau de competence des joueurs"),
(3, "Coach","Entraine les joueurs"),
(4, "Support","Dirige les parent et joueurs vers la prochaine etapes");

CREATE TABLE TypeEpreuve (
    id_type_epreuve INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    description VARCHAR(255)
); 

INSERT INTO TypeEpreuve
(id_type_epreuve, nom, description)
VALUES
(1, "Lancer", "Lance des balles"),
(2, "Frapper", "Frapper des balles avec un baton"),
(3, "Attraper", "Attrapper des balles avec un gant"),
(4, "Course", "Se deplacer autour du losange"),
(5, "Savoir", "Connaissance des regles");

CREATE TABLE EvenementEvaluation (
    id_evenement_evaluation INT AUTO_INCREMENT PRIMARY KEY,
    date VARCHAR(10),
    heure VARCHAR(5),
    adresse VARCHAR(255)
); 

INSERT INTO EvenementEvaluation
(id_evenement_evaluation, date, heure, adresse)
VALUES
(1,"2018-07-12", "08:30", "Centre Julien-Ducharme"),
(2,"2019-04-28", "08:30", "Ecole de la Montee");

CREATE TABLE Tuteur (
	id_tuteur INT AUTO_INCREMENT PRIMARY KEY,
    prenom VARCHAR(255),
    nom VARCHAR(255),
    telephone VARCHAR(14),
    courriel VARCHAR(255)
);

INSERT INTO Tuteur
(id_tuteur, prenom, nom, telephone, courriel)
VALUES
(1,"Alice","Carols", "819-555-1234", "alice@email.com"),
(2,"Bob","Roberts", "819-555-4321", "bob@email.com"),
(3,"Charles","Chaplin", "819-555-6789", "charles@email.com");

CREATE TABLE EvaluationCasquette (
	id_evaluation_casquette INT AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(255),
    date VARCHAR(10),
    resultat_numerique INT,
    a_reussi BOOLEAN
);

INSERT INTO EvaluationCasquette
(id_evaluation_casquette, date, resultat_numerique, a_reussi, description)
VALUES
(1, "2019-04-28", 10, true, "Pas vite vite"),
(2, "2019-04-28", 1, false, "");

CREATE TABLE Casquette (
	id_casquette INT AUTO_INCREMENT PRIMARY KEY,
    nom_couleur VARCHAR(15),
    description VARCHAR(255),
    deleted ENUM('false','true') DEFAULT 'FALSE');


INSERT INTO Casquette
(id_casquette, nom_couleur, description)
VALUES
(1,"Blanc", "Debutant"),
(2,"Gris", "Amateur"),
(3,"Noir", "Intermediaire"),
(4,"Vert", "Avance"),
(5,"Bleu", "Professionnel"),
(6,"Rouge", "Superstar");

CREATE TABLE Equipe (
	id_equipe INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255)
);

INSERT INTO Equipe
(id_equipe, nom)
VALUES
(1, "Expos"),
(2, "Cactus"),
(3, "Big Bill");

CREATE TABLE Partie (
	id_partie INT AUTO_INCREMENT PRIMARY KEY,
    date VARCHAR(10),
    adresse VARCHAR(255)
);

INSERT INTO Partie
(id_partie, date, adresse)
VALUES
(1, "2019-05-05", "Centre Lucien Ducharme"),
(2, "2019-07-07", "Stade Amédée Roy");


CREATE TABLE Benevole (
    id_benevole INT AUTO_INCREMENT PRIMARY KEY,
    prenom VARCHAR(255),
    nom VARCHAR(255),
    telephone VARCHAR(255),
    courriel VARCHAR(255),
    username VARCHAR(255),
    mot_de_passe VARCHAR(255),
    est_actif BOOLEAN,
    id_type_benevole INT,
    deleted ENUM('false','true') DEFAULT 'FALSE',
    FOREIGN KEY(id_type_benevole) REFERENCES TypeBenevole(id_type_benevole)
); 

INSERT INTO Benevole
(id_benevole, prenom, nom, telephone, courriel, username, mot_de_passe, est_actif, id_type_benevole, deleted)
VALUES
(1, 'Domingo', 'Palao', '819-555-1111', 'domingo@email.com', 'dom', 'mod', 1, 4, 'true'),
(2, 'Anthony', 'Cote', '819-555-1111', 'anthony@email.com', 'antho', 'side', 1, 2, 'true'),
(3, 'Jacques', 'Cartier', '', '', 'jc', 'hunter2', 0, 3, 'true'),
(4, 'MaÃ¯ka', 'Forestal', '(819) 823-6150', 'maifor@rallycap.ca', 'maifor', 'abc123', 1, 2, 'false'),
(5, 'Estel', 'Blanchet', '(819) 823-6150', 'estblan@rallycap.ca', 'estblan', 'abc123', 1, 2, 'false'),
(6, 'Elisabeth', 'Verreault', '(819) 824-7139', 'eliver@rallycap.ca', 'eliver', 'abc123', 0, 3, 'false'),
(7, 'Josianne', 'Fortin', '(819) 820-5623', 'josfor@rallycap.ca', 'josfor', 'abc123', 0, 1, 'false'),
(8, 'Julie', 'Cloutier', '(819) 564 3256', 'julclo@rallycap.ca', 'julclo', 'abc123', 0, 3, 'false'),
(9, 'VÃ©ronique', 'Audet', '(819) 823-6150', 'veraud@rallycap.ca', 'veraud', 'abc123', 0, 4, 'false'),
(10, 'JÃ©sus', 'Christ', '(819) 111-2323', 'jesus@rallycap.ca', 'jesus', 'abc123', 0, 4, 'true'),
(11, 'Roxanne', 'CotÃ©', '(819) 564-3257', 'roxcot@rallycap.ca', 'roxcot', 'abc123', 0, 3, 'false'),
(12, 'GeneviÃ¨ve', 'Demers', '(819) 820-2428', 'gendem@rallycap.ca', 'gendem', 'abc123', 1, 1, 'false'),
(13, 'Harry', 'Potter', '(819) 934 3941', 'harpot@rallycap.ca', 'harpot', 'abc123', 1, 2, 'false');

CREATE TABLE GroupeEvaluation (
    id_groupe_evaluation INT AUTO_INCREMENT PRIMARY KEY,
    id_evenement_evaluation INT,
    id_benevole INT,
    FOREIGN KEY(id_evenement_evaluation) REFERENCES EvenementEvaluation(id_evenement_evaluation),
	FOREIGN KEY(id_benevole) REFERENCES Benevole(id_benevole)
); 

INSERT INTO GroupeEvaluation
(id_groupe_evaluation, id_evenement_evaluation, id_benevole )
VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1);

CREATE TABLE Joueur (
	id_joueur INT AUTO_INCREMENT PRIMARY KEY,
	prenom VARCHAR(255),
    nom VARCHAR(255),
    date_naissance VARCHAR(10),
	id_casquette INT,
    est_actif BOOLEAN,
    id_tuteur INT,
    id_equipe INT,
    id_groupe_evaluation INT,
	FOREIGN KEY(id_casquette) REFERENCES Casquette(id_casquette),
	FOREIGN KEY(id_tuteur) REFERENCES Tuteur(id_tuteur),
    FOREIGN KEY(id_equipe) REFERENCES Equipe(id_equipe),
    FOREIGN KEY(id_groupe_evaluation) REFERENCES GroupeEvaluation(id_groupe_evaluation)
);

INSERT INTO Joueur
( id_joueur, prenom, nom, date_naissance, est_actif, id_tuteur, id_equipe, id_groupe_evaluation)
VALUES
(1, "Babe", "Ruth", "1895-12-31", false, 1, 2, 1),
(2, "Yogi", "Berra", "1925-12-31", true, 2, 2, 1),
(3, "Lou", "Gehrig", "1903-12-31", false, 2, 2, 1),
(4, "Roberto", "Clemente", "1934-12-31", true, 1, 2, 1),
(5, "Barry", "Bonds", "1964-12-31", true, 1, 2, 1);

CREATE TABLE StatsPartie (
	id_equipe INT,
    id_partie INT,
    pointage INT,
    PRIMARY KEY(id_equipe, id_partie),
	FOREIGN KEY(id_equipe) REFERENCES Equipe(id_equipe),
	FOREIGN KEY(id_partie) REFERENCES Partie(id_partie)
);

INSERT INTO StatsPartie
(id_partie, id_equipe, pointage)
VALUES
(1, 1, 7),
(1, 2, 30),
(2, 3, 12),
(2, 2, 0);

CREATE TABLE Epreuve (
	id_epreuve INT AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(255),
    description VARCHAR(255),
    nbr_minimum_reussite INT,
    nbr_essai_total INT,
    id_casquette INT,
    id_type_epreuve INT,
	FOREIGN KEY(id_casquette) REFERENCES Casquette(id_casquette),
	FOREIGN KEY(id_type_epreuve) REFERENCES TypeEpreuve(id_type_epreuve)
);

INSERT INTO Epreuve
(id_epreuve, nom, description, nbr_minimum_reussite, nbr_essai_total, id_casquette, id_type_epreuve)
VALUES
(1,"Blanc Lancer","Lancer la balle a 5 pieds",1,1,1,4),
(2,"Blanc Attrapper","Attrapper une balle a 5 pieds",9,15,1,1),
(3,"Blanc Frapper","Frapper une balle d'un tee",1,1,1,3),
(4,"Blanc Course","Savoir dans quelle direction courir apres avoir frapper",1,1,1,2),
(5,"Blanc Savoir","Connais le nom de son equipe",1,1,1,5),
(6,"Grise Lancer","Lancer 7/10 balles a 10pi",7,10,1,4),
(7,"Grise Attrapper","Attrapper une balle a 5 pieds",10,15,1,1),
(8,"Grise Frapper","Frapper 10/15 balles sur un tee",10,15,1,3),
(9,"Grise Course","Connais comment se nomme les buts",1,1,1,2),
(10,"Grise Savoir","Peux nommer toutes les positions sur le terrain",1,1,1,5),
(11,"Noir Lancer","Lancer la balle a 25 pieds",1,1,1,4),
(12,"Noir Attrapper","Attrapper 10/20 ballons a 15 pieds de haut",10,20,1,1),
(13,"Noir Frapper","Frapper 7/15 bales lancees par en-dessous",7,15,1,3),
(14,"Noir Course","Capable de courir autour des buts sans arreter",1,1,1,2),
(15,"Noir Savoir","Connais 3 facons de retirer un joueur",1,1,1,5);


CREATE TABLE Role (
    id_equipe INT,
    id_benevole INT,
    id_type_role INT,
    PRIMARY KEY(id_equipe, id_benevole),
    FOREIGN KEY(id_equipe) REFERENCES Equipe(id_equipe),
	FOREIGN KEY(id_benevole) REFERENCES Benevole(id_benevole),
    FOREIGN KEY(id_type_role) REFERENCES TypeRole(id_type_role)
); 

INSERT INTO Role
(id_equipe, id_benevole, id_type_role)
VALUES
(1, 1, 1),
(1, 2, 2);


CREATE TABLE EvaluationEpreuve (
	id_evaluation_epreuve INT AUTO_INCREMENT PRIMARY KEY,
    resultat_numerique INT,
    a_reussi BOOLEAN,
    commentaire VARCHAR(255),
    id_epreuve INT,
	id_evenement_evaluation INT,
    FOREIGN KEY(id_epreuve) REFERENCES Epreuve(id_epreuve),
    FOREIGN KEY(id_evenement_evaluation) REFERENCES EvenementEvaluation(id_evenement_evaluation)
);

INSERT INTO EvaluationEpreuve
(id_evaluation_epreuve, resultat_numerique, a_reussi, commentaire, id_epreuve, id_evenement_evaluation)
VALUES
(1, 4, true, "Interrompu par la pluie", 1, 1);


CREATE TABLE EvaluationEpreuveCasquette (
	id_joueur INT,
    id_evaluation_epreuve INT,
    id_evaluation_casquette INT,
	PRIMARY KEY(id_joueur, id_evaluation_epreuve, id_evaluation_casquette),
	FOREIGN KEY(id_joueur) REFERENCES Joueur(id_joueur),
	FOREIGN KEY(id_evaluation_epreuve) REFERENCES EvaluationEpreuve(id_evaluation_epreuve),
    FOREIGN KEY(id_evaluation_casquette) REFERENCES EvaluationCasquette(id_evaluation_casquette)
);

INSERT INTO EvaluationEpreuveCasquette
( id_joueur, id_evaluation_epreuve, id_evaluation_casquette)
VALUES
(1,1,1);

/* View */

CREATE VIEW vw_JoueurCasquette AS
SELECT j.id_joueur, j.nom, j.prenom, c.nom_couleur
FROM Joueur AS j, EvaluationEpreuveCasquette AS eec, EvaluationEpreuve AS ee, Epreuve AS e, Casquette AS c
WHERE j.id_joueur = eec.id_joueur
AND eec.id_evaluation_epreuve = ee.id_evaluation_epreuve
AND ee.id_epreuve = e.id_epreuve
AND e.id_casquette = c.id_casquette; 
