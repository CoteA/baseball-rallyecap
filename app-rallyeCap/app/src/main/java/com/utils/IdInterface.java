package com.utils;

/**
 The interface Id interface. */
public interface IdInterface {
    
    /**
     Gets id.
     @return the id
     */
    public int getId();
    /**
     Sets id.
     @param id the id
     */
    public void setId(int id);
}
