package com.properties;

import java.io.FileInputStream;
import java.util.Properties;

/** Gestionnaire de proprietes a heriter */
public abstract class PropertiesManager {

    protected static Properties props;
    
    /** Initialise la classe avec le fichier par defaut */
    public PropertiesManager() { init(getDefaultPropertyFile()); }
    
    /** Initialise la classe */
    public PropertiesManager(String propertiesFile) { init(propertiesFile); }
    
    
    /** Initialise les proprietes a partir du fichier de propriete */
    private void init(String propertiesFile) {
        
        try {
            Properties props = new Properties();
            props.load(new FileInputStream(propertiesFile));
            loadProperties();
            
        } catch (Exception e) { e.printStackTrace(); }
    }
    
    /** Load les proprietes une par une */
    protected abstract void loadProperties();
    
    protected abstract String getDefaultPropertyFile();
}
