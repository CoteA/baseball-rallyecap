package com.sqlite;

import com.properties.PropertiesManager;

import java.util.Properties;


import lombok.Getter;

/**
 Proprietes de connection a la database */
public class DatabaseProperties extends PropertiesManager {
    

    private static String defaultPropertyFile = "src/main/java/sqlite/database.properties";
    
    @Getter
    private static String driver;
    
    @Getter
    private static String username;
    
    @Getter
    private static String password;
    
    @Getter
    private static String url;
    
    @Getter
    private static String database;
    

    @Override
    protected void loadProperties() {
        
        // Get Properties
        driver = props.getProperty("sqlite.driver");
        url = props.getProperty("sqlite.url");
        username = props.getProperty("sqlite.username");
        password = props.getProperty("sqlite.password");
        database = props.getProperty("sqlite.database");
        
        // Default values
        if (username == null) username = "";
        if (password == null) password = "";
        if (database== null) database= "default_db_from_database_properties";
    }
    
    @Override
    protected String getDefaultPropertyFile() { return defaultPropertyFile; }
    
    
    // ************************************************************************
    
    // GETTER
    public static Properties getProps() { return props; }
    
    // SETTER
    public static void setProps(Properties props) { DatabaseProperties.props = props; }
}