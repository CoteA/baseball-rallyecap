package com.database;

import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

/** Gere la base de donnees */
public class PlainSQLDBManager {
    
    // PROPERTIES =============================================================
    private static final String MYSQL_DRIVER    = "com.mysql.cj.jdbc.Driver";
    private static final String PROP_FILE = "src/main/java/examen/database.properties";
    private static       String username;
    private static       String password;
    private static       String url;
    
    protected Connection        connect;
    protected Statement         statement;
    protected PreparedStatement prepStatement;
    protected ResultSet         resultSet;
    
    
    // CONSTRUCTORS ===========================================================
    
    public PlainSQLDBManager() { init(); }
    
    // GETTERS ================================================================
    
    public Connection getConnect()              { return connect; }
    public Statement getStatement()             { return statement; }
    public PreparedStatement getPrepStatement() { return prepStatement; }
    public ResultSet getResultSet()             { return resultSet; }
    
    // SETTERS ================================================================
    
    public void setConnect(Connection connect)                    { this.connect = connect; }
    public void setStatement(Statement statement)                 { this.statement = statement; }
    public void setPrepStatement(PreparedStatement prepStatement) { this.prepStatement = prepStatement; }
    public void setResultSet(ResultSet resultSet)                 { this.resultSet = resultSet; }
    
    // METHODS ================================================================
    
    /** Initialise la classe */
    private void init() {
        
        initProperties();
        initDriver();
        initConnection();
    }
    
    /** Initialise les proprietes a partir du fichier de propriete */
    private void initProperties() {
        
        try {
            Properties props = new Properties();
            props.load(new FileInputStream(PROP_FILE));
            
            // Get Properties
            String driver = props.getProperty("jdbc.driver");
            url = props.getProperty("jdbc.url");
            username = props.getProperty("jdbc.username");
            password = props.getProperty("jdbc.password");
            
            // Default values
            if (username == null) username = "";
            if (password == null) password = "";
            
            
        } catch (Exception e) {
            System.out.println(e.toString() + "--- Exception in PlainSQLDBManager.initProperties()");
        }
    }
    
    /** Initialise le driver */
    private void initDriver() {
        
        try {
            Class.forName(MYSQL_DRIVER); // This will load the MySQL DRIVER, each DB has its own DRIVER
        } catch (Exception e) {
            System.out.println(e.toString() + "--- Exception in PlainSQLDBManager.init()");
        } finally { close(); }
    }
    
    /** Initialize the connection */
    public void initConnection() {
        
        try {
            connect = DriverManager.getConnection(url, username, password);
            statement = connect.createStatement(); // Statements allow to issue SQL queries to the old.database
        } catch (SQLException e) {
            System.out.println(e.toString() + "--- Exception in PlainSQLDBManager.initConnection()");
        }
        
    }
    
    /** Ping the DB */
    public boolean ping() {
        
        try {
            resultSet = statement.executeQuery("SELECT 123;");
            resultSet.first();
            
            boolean isSuccessful = resultSet.getInt(1) == 123;
            return isSuccessful;
            
        } catch (Exception e) {
            
            System.err.println(e.toString());
            return false;
        }
    }
    
    
    /** Verify if resultSet contain data */
    public boolean isValid() {
        
        boolean isValid;
        try {
            isValid = resultSet.isBeforeFirst();
        } catch (Exception e) {
            isValid = false;
            System.out.println("--- ResultSet has no data in it. [PlainSQLDBManager.isValid()]");
        }
        return isValid;
    }
    
    /** Execute une requete et stock l'info dans les variables membres */
    public ResultSet executeGet(String query) {
        
        try {
            resultSet = statement.executeQuery(query);
            
        } catch (Exception e) {
            System.err.println(e.toString() + " : PlainSQLDBManager.executeGet()");
            resultSet = null;
        }
        return resultSet;
    }
    

    /** Execute query & stock the data in member variable */
    public void executeChange(String query) {
        
        try {
            statement.executeUpdate(query);// Result set get the result of the SQL query
            
        } catch (Exception e) {
            System.err.println(e.toString() + " : PlainSQLDBManager.executeGet()");
        }
    }
    
    /** Affiche les resultats*/
    public void printResultSet() {
        
        try {
            ResultSetMetaData rsMetaData    = resultSet.getMetaData();
            int               columnCount = rsMetaData.getColumnCount();
            
            // Pour tous les resultats
            while (resultSet.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    // Print each value
                    if (i > 1) System.out.print(",  ");
                    String columnValue = resultSet.getString(i);
                    System.out.print(columnValue + " " + rsMetaData.getColumnName(i));
                }
                System.out.println();
            }
        } catch (Exception e) {
            System.err.println(e.toString() + "--- Exception in PlainSQLDBManager.printResultSet()");
        }
    }
    
    /** Affiche les resultats sous forme de table */
    public void printResultSetAsTable() {
        
        try {
            ResultSetMetaData rsMetaData    = resultSet.getMetaData();
            int               columnCount = rsMetaData.getColumnCount();
            
            // Pour toutes les colonnes
            for (int i = 1; i <= columnCount; i++) {
                System.out.format("%-15s", rsMetaData.getColumnName(i));
            }
            System.out.println();
            
            // Pour tous les resultats
            while (resultSet.next()) {
                for (int i = 1; i <= columnCount; i++) {
                    // Print each value
                    String columnValue = resultSet.getString(i);
                    System.out.format("%-15s", columnValue);
                }
                System.out.println();
            }
        } catch (Exception e) {
            System.err.println(e.toString() + "--- Exception in PlainSQLDBManager.printResultSet()");
        }
    }
    
    /** Ferme la connection */
    public void close() {
        
        try {
            if (resultSet != null) { resultSet.close(); }
            if (statement != null) { statement.close(); }
            if (connect != null) { connect.close(); }
            
        } catch (Exception e) {
            System.err.println(e.toString() + "--- Exception in PlainSQLDBManager.close()");
        }
    }
    
    public void executePreparedStatement(PreparedStatement ps) throws SQLException{
        setPrepStatement(ps);
        ps.executeUpdate();
    }
    
}
