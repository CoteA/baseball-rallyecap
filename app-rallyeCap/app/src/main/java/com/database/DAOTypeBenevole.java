package com.database;

import com.domain.TypeBenevole;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DAOTypeBenevole extends PlainSQLDBManager {
    
    
    /** Verifi si l'type_benevole existe dans la db */
    boolean typeBenevoleExiste(TypeBenevole e) {
        
        executeGet("SELECT TOP 1 type_benevole_id FROM type_benevole WHERE type_benevole_id = " + e.getId() + ";");
        
        try {
            return resultSet.isBeforeFirst();
        } catch (SQLException ex) {
            return false;
        }
        
    }
    
    /** Trouve le prochain ID a utiliser */
    int nextAvailableId(String tablename) {
        
        String query = "SELECT Auto_increment FROM information_schema.tables WHERE table_name='" + tablename + "'";
        executeGet(query);
        
        try {
            resultSet.first();
            return resultSet.getInt(0);
        } catch (SQLException e) {
            return 1;
        }
    }
    
    
    /** Ajoute un type_benevole a la DB */
    boolean ajouter(TypeBenevole e) {
        
        System.out.println("Ajoute " + e);
        
        if (typeBenevoleExiste(e)) e.setId(nextAvailableId("type_benevole"));
        
        try {
            String            query = "INSERT INTO type_benevole (type_benevole_nom, type_benevole_prenom) VALUES (?, ?);";
            PreparedStatement ps    = connect.prepareStatement(query);
            ps.setString(1, e.getNom());
            ps.setString(2, e.getDescription());
            ps.setInt(3, e.getId());
            
            executePreparedStatement(ps);
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
        
    }
    
    /** Met a jour un type_benevole */
    boolean update(TypeBenevole e) {
        
        if (!typeBenevoleExiste(e)) return false;
        
        System.out.println("Update " + e);
        try {
            String            query = "UPDATE type_benevole SET type_benevole_nom = ?, type_benevole_prenom = ? WHERE type_benevole_id = ?; ";
            PreparedStatement ps    = connect.prepareStatement(query);
            ps.setString(1, e.getNom());
            ps.setString(2, e.getDescription());
            ps.setInt(3, e.getId());
            
            executePreparedStatement(ps);
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    /** Supprime un type_benevole */
    boolean supprime(TypeBenevole e) {
        
        if (!typeBenevoleExiste(e)) return false;
        
        System.out.println("Supprime " + e);
        try {
            String            query = "DELETE FROM type_benevole WHERE type_benevole_id = ?;";
            PreparedStatement ps    = connect.prepareStatement(query);
            ps.setInt(1, e.getId());
            
            executePreparedStatement(ps);
            e = null;
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    /** Affiche la liste des type_benevoles */
    void afficheListeTypeBenevoles() {
        
        executeGet("SELECT * FROM type_benevole;");
        printResultSetAsTable();
        
    }
    
//    /** Affiche les sports jouer par les type_benevole avec leur performance */
//    void afficheListeSportJouerPartype_benevole() {
//        
//        String query = "SELECT type_benevole_nom, type_benevole_prenom, sport_nom, performance FROM type_benevole, joue, sport WHERE type_benevole.type_benevole_id = joue.type_benevole_id AND sport.sport_id = joue.sport_id;";
//        executeGet(query);
//        printResultSetAsTable();
//    }
//    
//    /** inscrit un type_benevole a un sport a partir des id */
//    boolean inscriretype_benevoleASport(int id_type_benevole, int id_sport) {
//        
//        System.out.println(id_type_benevole + " joue a " + id_sport);
//        try {
//            String            query = "INSERT INTO joue (id_type_benevole, id_sport, performance) VALUES (?, ?, ?)";
//            PreparedStatement ps    = connect.prepareStatement(query);
//            ps.setInt(1, id_type_benevole);
//            ps.setInt(2, id_sport);
//            ps.setInt(3, 0);    // Zero performance au debut
//            executePreparedStatement(ps);
//            
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }
    
//    /** Inscrit un type_benevole a un sport */
//    boolean inscriretype_benevoleASport(type_benevole e, Sport s) {
//        
//        System.out.println(e + " joue a " + s);
//        try {
//            String            query = "INSERT INTO joue (id_type_benevole, id_sportm performance) VALUES (?, ?, ?)";
//            PreparedStatement ps    = connect.prepareStatement(query);
//            ps.setInt(1, e.getId());
//            ps.setInt(2, s.getId());
//            ps.setInt(3, 0);    // Zero performance au debut
//            executePreparedStatement(ps);
//            
//        } catch (SQLException ex) {
//            ex.printStackTrace();
//            return false;
//        }
//        return true;
//    }

}
