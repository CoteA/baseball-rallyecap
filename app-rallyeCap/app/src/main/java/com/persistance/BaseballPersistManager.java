package com.persistance;


import com.domain.TypeBenevole;
import com.utils.PersistenceManager;


import javax.persistence.EntityManager;


/**
 Classe statique pour les besoin du client
 Il est possible de juste utiliser PersistenceManager qui est generique
 BaseballPersistManager permet de creer des methodes specifique */
public class BaseballPersistManager extends PersistenceManager {
    
    /** Constructeur  @throws ClassNotFoundException the class not found exception */
    public BaseballPersistManager() throws ClassNotFoundException { init(); }
    
    
    /**
     Ajout un nouvel etudiant dans la database  @param c the c
     @return the boolean
     */
    public static boolean ajouteTypeBenevole(TypeBenevole tb) { return create(tb); }
    
    

    //    /** Ajout un nouvel professeur dans la database */
    //    public static boolean ajouteProfesseur(Professeur p) { return create(p); }
    //
    //    /** Ajout un nouvel cours dans la database */
    //    public static boolean ajouterCours(CompetenceRequired c, boolean isCascading) {
    //
    //        if (isCascading) {
    //            update(c.getCandidat());
    //            update(c.getCompetence());
    //        }
    //        create(c);
    //        return true;
    //    }
    //
    //    /**     Modifi le cours
    //     @param c CompetenceRequired a mettre a jour
    //     @return si ca a reussi
    //     */
    //    public static CompetenceRequired changerCours(CompetenceRequired c) {
    //
    //        // Modifi les sous-elements
    //        c.setCandidat((Curriculum) update(c.getCandidat()));
    //        c.setCompetence((Professeur) update(c.getCompetence()));
    //
    //        return (CompetenceRequired) update(c);
    //    }
    //
    //    /**     Change l'etat de l'inscription pour la mettre a INACTIVE
    //     @param i incription a modifier
    //     @return si ca a reussi
    //     */
    //    public static Inscription annulerInscription(Inscription i) {
    //
    //        i.setEtat(Inscription.INACTIVE);
    //        return (Inscription) update(i);
    //    }
    //
    //    /**     Supprime un professeur de la base de donnees
    //     @param p le professeur a supprimer
    //     @return si ca a reussi
    //     */
    //    public static boolean supprimerProfesseur(Professeur p) {
    //
    //        return tryToDeleteProfesseur(p);
    //    }
    //

    
    public static TypeBenevole loadCertification(int id)     { return emFactory.createEntityManager().find(TypeBenevole.class, id); }

    /** @deprecated Not tested */
    @Deprecated()
    public static boolean typeBenevoleExist(int id) { return emFactory.createEntityManager().find(TypeBenevole.class, id) != null; }
    
    /**
     Essaye de supprimer un objet dans la DB  @param obj the obj
     @return the boolean
     */
    public static boolean delete(TypeBenevole obj) {
        
        EntityManager em = emFactory.createEntityManager();
        em.getTransaction().begin();
        obj = em.find(TypeBenevole.class, ((TypeBenevole) obj).getId());
        em.remove(obj);
        
        return commit(em);
    }
    

}
