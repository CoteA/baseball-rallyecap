package com.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Epreuve {
    
    int id;
    String nom;
    String description;
    int nbEssaiMin;
    int nbEssaiTotal;
    int idCasquette;
    int idTypeEpreuve;
}
