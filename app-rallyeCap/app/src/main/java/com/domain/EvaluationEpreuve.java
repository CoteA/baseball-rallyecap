package com.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluationEpreuve {
    
    int id;
    String description;
    int resultatNumerique;
    String date;
    boolean reussi;
}
