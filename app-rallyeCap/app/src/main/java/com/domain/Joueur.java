package com.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Joueur {
    
    int id;
    String prenom;
    String nom;
    String dateNaissance;
    String telephone;
    String commentaire;
}
