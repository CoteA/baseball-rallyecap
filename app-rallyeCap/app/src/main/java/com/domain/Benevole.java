package com.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Benevole {
    
    int id;
    String prenom;
    String nom;
    String telephone;
    String courriel;
    String username;
    String motDePasse;
    boolean estActif;
    int idTypePersonnel;
}
