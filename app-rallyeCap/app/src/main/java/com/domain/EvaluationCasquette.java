package com.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EvaluationCasquette {
    
    int id;
    String description;
    String date;
    boolean reussi;
    int resultatNumerique;
}
