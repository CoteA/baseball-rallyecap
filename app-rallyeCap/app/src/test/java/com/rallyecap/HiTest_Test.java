package com.rallyecap;

import com.example.rallyecap.HiTest;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 Example local unit test, which will execute on the development machine (host).
 @see <a href="http://d.android.com/tools/testing">Testing documentation</a> */
public class HiTest_Test {
    
    @Test
    public void addition_isCorrect() { assertEquals(4, 2 + 2); }
    
    @Test
    public void getTrue() { assertTrue(new HiTest().getTrue()); }
    
}